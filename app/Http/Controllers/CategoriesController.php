<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|owner')->only('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if ($request->hasFile('picture')) {
        // $path = $request->file('picture')->store('pictures','public');
            $path = public_path('/images');
            $file_name = 'images/' . time() . "_" . Input::file('picture')->getClientOriginalName();
            Input::file('picture')->move($path, $file_name);
        }
        $cat = Category::create([
            'name' => $request['name'],
            'details' => $request['details'],
            'picture' => $file_name,
        ]);
        return redirect()->route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Category::find($id)->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit')->with('category', $category);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $cat = Category::findOrfail($id);
        $path = null;
        if ($request->hasFile('picture')) {
            $path = public_path('/images');
            $file_name = 'images/' . time() . "_" . Input::file('picture')->getClientOriginalName();
            Input::file('picture')->move($path, $file_name);
        }
        $cat = $cat->update([
            'name' => $request['name'],
            'details' => $request['details'],
            'picture' => $file_name ? $file_name : $cat->picture,
        ]);
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::findOrfail($id)->delete();
        return redirect()->route('category.index');
    }
}
