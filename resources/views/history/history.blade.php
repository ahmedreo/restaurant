@extends('layouts.app') 
@section('content')
<script src="{{asset('admin/assets/js/easytime.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">

<div id="page_content_inner">
    <div id="searchForm">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a">Search Date range</h3>
                        <form action="{{url('historyfilter')}}" id="search-form" method="get">
                            <div class="uk-grid" data-uk-grid-margin>

                                <div class="uk-width-large-1-4 uk-width-1-1">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_start">Start Date</label>
                                        <input class="md-input" name="start" type="text" id="uk_dp_start" required>
                                    </div>
                                </div>
                                <div class="uk-width-large-1-4 uk-width-medium-1-1">
                                    <div class="uk-input-group">
                                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                        <label for="uk_dp_end">End Date</label>
                                        <input class="md-input" name="end" type="text" id="uk_dp_end" required>
                                    </div>
                                </div>
                                @role('admin')
                                <div class="uk-width-large-1-4 uk-width-medium-1-1">
                                        <div class="uk-input-group">

                                        <select class="uk-nav uk-nav-dropdown" plcaholder="Restaurant" id="restaurant_id" name="restaurant_id" data-md-select2>
                                                <option value="">All</option>
                                                @forelse (\App\Restaurant::all() as $rest)
                                                <option value="{{$rest->id}}">{{$rest->name}}</option>
                                                @empty @endforelse
                                            </select>
                                        </div>
                                </div>
                                @endrole
                                <div class="uk-width-large-1-3 uk-width-medium-1-1">
                                    <div class="uk-input-group">
                                        <button class="md-btn md-btn-primary" type="submit" data-uk-button>Search</button>
                                        <button class="md-btn " id="report" style="display:none"  data-sum="0">Report</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="padding-top: 2%;">
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                        <div class="dt_colVis_buttons"></div>
                    <table class="uk-table uk-table-nowrap table_check"id="dt_tableExport"  cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="uk-width-2-10 uk-text-center">Customer Name</th>
                                <th class="uk-width-2-10 uk-text-center">Order Number</th>
                                <th class="uk-width-1-10 uk-text-center">Order Date</th>
                                <th class="uk-width-1-10 uk-text-center">Restaurant</th>
                            </tr>
                        </thead>
                        <tbody id="results">
    @include('history.ordersrow')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="modal_overflow" class="uk-modal">
    <div class="uk-modal-dialog" style="padding-top: 0%">
        <button type="button" class="uk-modal-close uk-close"></button>
        <div class="uk-modal-header" style="BACKGROUND-COLOR: RED; padding-top: 7px;padding-bottom: 7px;
            color: white;">
            ORDER DETAILS
        </div>
        <div id="modal-content">

        </div>
    </div>
</div>
@endsection
 
@section('pagejs')
<script>
    $(document).ready(function(){
        $(document).on('click','.order-details',function(){
    id = $(this).data('order');
    console.log(id);
    url="{{url('orderdetails/')}}"+'/'+id+'?history=1';
    console.log('name');
    $.ajax({
        url:url,
        type:"GET",
        success:function(res){
            $('#modal-content').html(res);
        }
    })
});
$('#report').on('click',function(e){
    e.preventDefault();
    data =[];
    data = $('#search-form').serializeArray();
    url="{{url('report')}}";
    data.push({name:'_method',value:"{{csrf_token()}}"})
    console.log(data);

    $.ajax({
            url:url,
            method:"get",
            data:data,
            success:function(res){
                console.log(res);
                $('#report').attr("disabled", "disabled");
                $('#report').css("border-bottom", "2px solid green");
                $('#report').css("background-color", "gray");
                $('#report').text("Report Created");

            }
        })
})
    $('#search-form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        data = $(this).serializeArray();
        console.log(data);
        $.ajax({
            url:url,
            method:"GET",
            data:data,
            success:function(res){
                console.log(res)
                $('#dt_tableExport').DataTable().destroy();
                $('#results').empty();
                $('#results').hide()
                $('#results').html(res.html);
                if(res.sum){
                    $('#report').show("slow")

                    $('#report').removeAttr("disabled");
                    $('#report').text("Make Report");
                    $('#report').css("border-bottom", "0px solid white");
                $('#report').css("background-color", "white");
                }else{
                    $('#report').hide("slow");
                  

                }
//                 $('#dt_tableExport').DataTable()
//    .draw();
   altair_datatables.dt_colVis();
    altair_datatables.dt_tableExport();
                $('#results').fadeIn("swing");
                
                
            }
        });
    });
});
altair_datatables = {
    dt_default: function() {
        var $dt_default = $('#dt_default');
        if($dt_default.length) {
            $dt_default.DataTable();
        }
    },
    dt_scroll: function() {
        var $dt_scroll = $('#dt_scroll');
        if($dt_scroll.length) {
            $dt_scroll.DataTable({
                "scrollY": "200px",
                "scrollCollapse": false,
                "paging": false
            });
        }
    },
   
    dt_colVis: function() {
        var $dt_colVis = $('#dt_colVis'),
            $dt_buttons = $dt_colVis.prev('.dt_colVis_buttons');

        if($dt_colVis.length) {

            // init datatables
            var colVis_table = $dt_colVis.DataTable({
                buttons: [
                    {
                        extend: 'colvis',
                        fade: 0
                    }
                ]
            });

            colVis_table.buttons().container().appendTo( $dt_buttons );

        }
    },
    dt_tableExport: function() {
        var $dt_tableExport = $('#dt_tableExport'),
            $dt_buttons = $dt_tableExport.prev('.dt_colVis_buttons');

        if($dt_tableExport.length) {
            var table_export = $dt_tableExport.DataTable({
                buttons: [
                    {
                        extend:    'copyHtml5',
                        text:      '<i class="uk-icon-files-o"></i> Copy',
                        titleAttr: 'Copy'
                    },
                    {
                        extend:    'print',
                        text:      '<i class="uk-icon-print"></i> Print',
                        titleAttr: 'Print'
                    },
                    {
                        extend:    'excelHtml5',
                        text:      '<i class="uk-icon-file-excel-o"></i> XLSX',
                        titleAttr: ''
                    },
                    {
                        extend:    'csvHtml5',
                        text:      '<i class="uk-icon-file-text-o"></i> CSV',
                        titleAttr: 'CSV'
                    },
                    {
                        extend:    'pdfHtml5',
                        text:      '<i class="uk-icon-file-pdf-o"></i> PDF',
                        titleAttr: 'PDF'
                    }
                ]
            });

            table_export.buttons().container().appendTo( $dt_buttons );

        }
    }
};
</script>
<script src="{{asset('admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('admin/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('admin/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>

<script src="{{asset('admin/assets/js/pages/forms_advanced.min.js')}}"></script>
 <!-- datatables -->
 <script src="{{asset('admin/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
 <!-- datatables buttons-->
 <script src="{{asset('admin/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
 <script src="{{asset('admin/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
 <script src="{{asset('admin/bower_components/jszip/dist/jszip.min.js')}}"></script>
 <script src="{{asset('admin/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
 <script src="{{asset('admin/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
 <script src="{{asset('admin/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
 <script src="{{asset('admin/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
 <script src="{{asset('admin/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>

 <!-- datatables custom integration -->
 <script src="{{asset('admin/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

 <!--  datatables functions -->
 <script src="{{asset('admin/assets/js/pages/plugins_datatables.min.js')}}"></script>
@endsection