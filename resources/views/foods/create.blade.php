@extends('layouts.app') 
@section('content')
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">

<div id="page_content_inner">
    <h1>Create New Food</h1>
    <div class="md-card">
        <form action="{{url('food')}}" method="POST" role="form" class="uk-form-stacked" enctype="multipart/form-data">

            <div class="md-card-content large-padding">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-3">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="name">Name<span class="req">*</span></label>
                            <input type="text" value="{{old('name')}}" name="name" data-parsley-trigger="change" required class="md-input" />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="enough">enough<span class="req">*</span></label>
                            <input type="text" value="{{old('enough')}}" name="enough" required class="md-input" />
                        </div>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="details">details<span class="req">*</span></label>
                            <input type="text" value="{{old('details')}}" name="details" class="md-input" required />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="info">info<span class="req">*</span></label>
                            <input class="md-input" type="text" name="info" value="{{old('info')}}" id="close" required>
                        </div>
                    </div>
                    
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="discount">discount<span class="req">*</span></label>
                            <input type="number" value="{{old('discount')}}" name="discount" required class="md-input" />
                        </div>
                    </div>
                    
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="restaurant_id">Restaurant<span class="req">*</span></label>
                            <select id="restaurant_id" name="restaurant_id" class="uk-width-1-1" required data-md-select2>
                                <option value="">Select Restaurant</option>
                                @forelse (\App\Restaurant::all() as $rest)
                                <option @if(app('request')->input('rest_id') && app('request')->input('rest_id') == $rest->id) selected @endif value="{{$rest->id}}">{{$rest->name}}</option>
                                @empty                                
                                @endforelse
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                            <div class="parsley-row">
                                <label for="category_id">category<span class="req">*</span></label>
                                <select id="category" name="category_id" class="uk-width-1-1" data-md-select2>
                                <option value="">Select category</option>
                                @forelse ($categories as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                @empty                                
                                @endforelse
                            </select>
                            </div>
                        </div>
                </div>
                        </div>
                    <div class="uk-width-medium-1-3">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_a uk-margin-small-bottom">
                                    Picture
                                </h3>
                                <input type="file" name="picture" data-default-file="{{old('picture')}}" id="input-file-a"  class="dropify" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">Create</button>
                    </div>
                </div>
            </div>

    </div>
    </form>


</div>
@endsection
 
@section('pagejs')
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
@endsection