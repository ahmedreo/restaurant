<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectedFoodOption extends Model
{

    protected $fillable = ['food_order_id', 'option', 'price','comment'];
    protected $table="selected_options";
}
