<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use App\Http\Resources\UsersCollection as UsersCollection;
use App\Http\Requests\RegisterUser;
use App\Http\Requests\UpdateUser;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\updatePassword;
use App\Restaurant;
class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|owner')->only('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::get();
        $users = User::Paginate(10);
        $users->load('roles','restaurant');
        return  view('user.index')->with(['users'=>$users,'roles'=>$roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return $roles->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterUser $request)
    {
        dd('das');
        // \Log::info($request->all());
        // // dd('s');
        // $role = $request['role_id'];
        // unset($request['role_id']);
        // // $request['password'] = Hash::make($request['password']);

        // $user = User::create($request->all());
        // $user->assignRole($role);
        // return $user->toArray();;
    }
    public function addToRestaurant(User $user,$restaurantId)
    {
        $user->restaurant_id = $restaurantId;
        $user->save();
        return response('ok',200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::with('roles')->whereId($id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $restaurants = Restaurant::all();
        if(auth()->user()->hasRole('admin'))
            $user = User::with('roles')->whereId($id)->first();
        else{
            $user = auth()->user();
            }
            $user->load('roles','restaurant');
        return   view('user.edit')->with(['user'=>$user,'roles'=>$roles,'restaurants'=>$restaurants]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, $id)
    {
    if(auth()->user()->hasRole('admin'))
        $user = User::find($id);
    else
        $user = auth()->user();

    $user->update($request->all());
        return response('ok',200);
    }
    public  function changeRoles(User $user,$roleId)
    {
        if(auth()->user()->hasRole('admin'))
            $user =$user;
        else
            $user = auth()->user();
        $user->syncRoles($roleId);
        return response('ok',200);
    }
    public function changePassword (updatePassword $request,User $user)
    {
        $admin = false;
        if(auth()->user()->hasRole('admin'))
            $admin=true;
        
        if (!$admin && !Hash::check($request['current_password'], auth()->user()->password) ) {
            info('dosent match');
            return response()->json(403);
        }else {
            unset($request['current_password']);
            info($request->all());
            $user->update($request->all());
            return response()->json(200);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->hasRole('admin'))
            $user= User::find($id)->delete();
        return back();
    }
}
