<div class="uk-modal" id="modal_working_calendar">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Working Calendar <i class="material-icons" data-uk-tooltip="{pos:'top'}"
                    title="headline tooltip">event_note</i></h3>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="{{url('restaurant/'.$rest->id.'/calendar')}}" id="calendar_form" method="POST">
            @csrf
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <table class="uk-table uk-table-striped uk-table-hover">

                        <thead style="text-align: center;">
                            <td>Day</td>
                            <td>Open At</td>
                            <td>Close At</td>
                            <td>Closed</td>
                        </thead>

                        <body style="text-transform: capitalize;">
                            @php
                                $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
                            
                           @endphp

                            @foreach ($days as $day)
                            @php
                                // dd($rest->calendar->$day);
                            @endphp  
                            <tr>
                                <td>{{$day}}</td>
                                <td>
                                    <input class="md-input" type="text" name="{{$day}}[openAt]"
                                        value="{{($rest->calendar)?$rest->calendar->$day["openAt"]:null}}" id="{{$day}}-open"
                                        data-uk-timepicker>
                                </td>
                                <td>
                                    <input class="md-input" type="text" name="{{$day}}[closeAt]"
                                        value="{{($rest->calendar)?$rest->calendar->$day['closeAt']:null}}" id="{{$day}}-close"
                                        data-uk-timepicker>
                                </td>
                                <td>
                                    <div class="uk-float-right">
                                    <input type="checkbox" data-switchery value="1" class="closed" data-day="{{$day}}" @if($rest->calendar->$day['closed'])
                                        checked @endif  name="{{$day}}[closed]" id="closed"  />
                                    <input type="hidden"  value="0" class="closed" data-day="{{$day}}" @if(!$rest->calendar->$day['closed'])
                                        checked @endif  name="{{$day}}[closed]" id="{{$day}}-closed"  />
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </body>
                    </table>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
            </div>
        </form>

    </div>
</div>
<script>
     document.addEventListener("DOMContentLoaded",function(){
    $('#calendar_form').on('submit',function(e){
    e.preventDefault();
    url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
    console.log(fdata);
    $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                var modal = UIkit.modal("#modal_working_calendar");
                    modal.hide()
            },
            error: function( error){
                console.log(error.responseJSON.errors);
                errors = error.responseJSON.errors
                for (var key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        console.log(key + " -> " + errors[key]);
                    $('#danger').data('message',errors[key]).click();
                        
                    }
                }
            }
        });
    
});
    $('.closed').on('change',(e)=>{
        var day = $(e.target).data('day');
        console.log(day);
        $('#'+day+'-close').val('00:00')
        $('#'+day+'-open').val('00:00')
        val = (e.target.checked)?1:0;
        console.log(e.target.value);
        console.log(e.target.checked);
        if(e.target.checked)
            $("#"+day+"-closed").attr('disabled',true)
        else
            $("#"+day+"-closed").removeAttr('disabled')
        

     });
     });
</script>