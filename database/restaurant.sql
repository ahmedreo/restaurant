-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2019 at 06:42 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'imgs/placeholder.jpg',
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `picture`, `details`, `created_at`, `updated_at`) VALUES
(27, 'Pizza', 'images/1548375482_pizza.jpg', 'some pizza', '2018-12-03 10:17:50', '2019-01-24 22:18:02'),
(28, 'chicken', 'images/1548375495_chicken.jpg', 'dede', '2018-12-04 16:11:03', '2019-01-24 22:18:15'),
(31, 'pizza', 'images/1557233438_MV5BNDY5NzBiMTQtYzhmMS00Y2FmLTkzYzYtYzM2ODBjNGZlNGNiXkEyXkFqcGdeQXVyMjk3NTUyOTc@._V1_.jpg', 'pizza', '2019-05-07 12:50:38', '2019-05-07 12:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `descount_type` tinyint(1) NOT NULL DEFAULT '0',
  `descount` double(8,2) NOT NULL,
  `restaurant_id` int(10) UNSIGNED DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `count`, `descount_type`, `descount`, `restaurant_id`, `start`, `end`, `created_at`, `updated_at`) VALUES
(1, 'de', 3, 0, 2.00, NULL, NULL, NULL, '2019-04-26 19:26:16', '2019-04-26 19:26:16'),
(6, 'ded', 2, 1, 3.00, 2, '2019-01-02', '2019-12-04', '2019-05-07 14:14:18', '2019-05-11 15:34:31'),
(7, 'de', 3, 0, 2.00, 2, '2018-12-02', '2018-12-04', '2019-05-07 14:14:58', '2019-05-07 14:14:58'),
(9, 'ccc', 3, 1, 2.00, 2, '2018-12-02', '2019-12-03', '2019-05-07 15:52:36', '2019-09-11 21:41:18'),
(13, 'ddd', 3, 0, 3.00, 2, '2019-05-11', '2019-05-26', '2019-05-11 15:31:34', '2019-05-11 15:31:34'),
(14, 'ewwe', 20, 1, 3.00, 2, '2019-05-26', '2019-05-31', '2019-05-25 03:28:51', '2019-05-25 03:28:51');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `company`, `address`, `email`, `phone`, `created_at`, `updated_at`, `user_id`) VALUES
(14, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 07:50:17', '2018-12-05 07:50:17', NULL),
(15, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 07:53:12', '2018-12-05 07:53:12', NULL),
(16, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 07:54:51', '2018-12-05 07:54:51', NULL),
(17, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 07:55:11', '2018-12-05 07:55:11', NULL),
(18, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 07:57:53', '2018-12-05 07:57:53', NULL),
(19, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 07:59:25', '2018-12-05 07:59:25', NULL),
(20, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:00:38', '2018-12-05 08:00:38', NULL),
(21, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:01:05', '2018-12-05 08:01:05', NULL),
(22, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:02:16', '2018-12-05 08:02:16', NULL),
(23, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:02:41', '2018-12-05 08:02:41', NULL),
(24, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:03:16', '2018-12-05 08:03:16', NULL),
(25, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:03:40', '2018-12-05 08:03:40', NULL),
(26, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:04:54', '2018-12-05 08:04:54', NULL),
(27, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:06:39', '2018-12-05 08:06:39', NULL),
(28, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:07:47', '2018-12-05 08:07:47', NULL),
(29, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:08:08', '2018-12-05 08:08:08', NULL),
(30, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:08:29', '2018-12-05 08:08:29', NULL),
(31, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:08:44', '2018-12-05 08:08:44', NULL),
(32, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:08:54', '2018-12-05 08:08:54', NULL),
(33, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:09:38', '2018-12-05 08:09:38', NULL),
(34, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:10:19', '2018-12-05 08:10:19', NULL),
(35, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:10:33', '2018-12-05 08:10:33', NULL),
(36, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:11:56', '2018-12-05 08:11:56', NULL),
(37, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:12:27', '2018-12-05 08:12:27', NULL),
(38, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:12:54', '2018-12-05 08:12:54', NULL),
(39, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:18:53', '2018-12-05 08:18:53', NULL),
(40, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:23:26', '2018-12-05 08:23:26', NULL),
(41, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:23:39', '2018-12-05 08:23:39', NULL),
(42, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:32:26', '2018-12-05 08:32:26', NULL),
(43, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:33:29', '2018-12-05 08:33:29', NULL),
(44, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:34:28', '2018-12-05 08:34:28', NULL),
(45, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:36:28', '2018-12-05 08:36:28', NULL),
(46, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:37:34', '2018-12-05 08:37:34', NULL),
(47, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:45:32', '2018-12-05 08:45:32', NULL),
(48, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:46:22', '2018-12-05 08:46:22', NULL),
(49, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:49:44', '2018-12-05 08:49:44', NULL),
(50, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:52:33', '2018-12-05 08:52:33', NULL),
(51, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:54:40', '2018-12-05 08:54:40', NULL),
(52, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 08:59:59', '2018-12-05 08:59:59', NULL),
(53, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 09:04:29', '2018-12-05 09:04:29', NULL),
(54, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 09:05:47', '2018-12-05 09:05:47', NULL),
(55, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-05 09:07:18', '2018-12-05 09:07:18', NULL),
(56, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-08 16:47:58', '2018-12-08 16:47:58', NULL),
(57, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-08 16:48:38', '2018-12-08 16:48:38', NULL),
(58, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-10 07:52:00', '2018-12-10 07:52:00', NULL),
(59, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-10 16:35:20', '2018-12-10 16:35:20', NULL),
(60, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-10 19:15:33', '2018-12-10 19:15:33', NULL),
(61, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 18:41:43', '2018-12-12 18:41:43', NULL),
(62, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:27:23', '2018-12-12 19:27:23', NULL),
(63, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:37:50', '2018-12-12 19:37:50', NULL),
(64, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:40:12', '2018-12-12 19:40:12', NULL),
(65, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:44:00', '2018-12-12 19:44:00', NULL),
(66, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:51:36', '2018-12-12 19:51:36', NULL),
(67, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:53:13', '2018-12-12 19:53:13', NULL),
(68, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:54:59', '2018-12-12 19:54:59', NULL),
(69, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:56:53', '2018-12-12 19:56:53', NULL),
(70, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 19:59:59', '2018-12-12 19:59:59', NULL),
(71, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:05:59', '2018-12-12 20:05:59', NULL),
(72, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:09:57', '2018-12-12 20:09:57', NULL),
(73, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:12:31', '2018-12-12 20:12:31', NULL),
(74, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:13:48', '2018-12-12 20:13:48', NULL),
(75, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:15:14', '2018-12-12 20:15:14', NULL),
(76, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:17:37', '2018-12-12 20:17:37', NULL),
(77, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:26:03', '2018-12-12 20:26:03', NULL),
(78, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:35:12', '2018-12-12 20:35:12', NULL),
(79, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:50:16', '2018-12-12 20:50:16', NULL),
(80, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:52:14', '2018-12-12 20:52:14', NULL),
(81, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:57:28', '2018-12-12 20:57:28', NULL),
(82, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 20:59:07', '2018-12-12 20:59:07', NULL),
(83, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:05:57', '2018-12-12 21:05:57', NULL),
(84, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:08:52', '2018-12-12 21:08:52', NULL),
(85, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:19:20', '2018-12-12 21:19:20', NULL),
(86, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:31:34', '2018-12-12 21:31:34', NULL),
(87, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:52:53', '2018-12-12 21:52:53', NULL),
(88, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:55:46', '2018-12-12 21:55:46', NULL),
(89, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-12 21:58:42', '2018-12-12 21:58:42', NULL),
(90, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 13:55:58', '2018-12-16 13:55:58', NULL),
(91, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 13:57:43', '2018-12-16 13:57:43', NULL),
(92, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 13:58:47', '2018-12-16 13:58:47', NULL),
(93, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 13:59:15', '2018-12-16 13:59:15', NULL),
(94, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:08:10', '2018-12-16 14:08:10', NULL),
(95, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:09:17', '2018-12-16 14:09:17', NULL),
(96, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:09:38', '2018-12-16 14:09:38', NULL),
(97, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:09:58', '2018-12-16 14:09:58', NULL),
(98, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:10:50', '2018-12-16 14:10:50', NULL),
(99, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:12:39', '2018-12-16 14:12:39', NULL),
(100, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:13:05', '2018-12-16 14:13:05', NULL),
(101, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:13:50', '2018-12-16 14:13:50', NULL),
(102, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:14:13', '2018-12-16 14:14:13', NULL),
(103, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:14:53', '2018-12-16 14:14:53', NULL),
(104, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:17:23', '2018-12-16 14:17:23', NULL),
(105, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:19:24', '2018-12-16 14:19:24', NULL),
(106, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:19:40', '2018-12-16 14:19:40', NULL),
(107, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:22:12', '2018-12-16 14:22:12', NULL),
(108, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:22:56', '2018-12-16 14:22:56', NULL),
(109, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:24:06', '2018-12-16 14:24:06', NULL),
(110, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:24:47', '2018-12-16 14:24:47', NULL),
(111, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:25:47', '2018-12-16 14:25:47', NULL),
(112, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:26:01', '2018-12-16 14:26:01', NULL),
(113, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:26:18', '2018-12-16 14:26:18', NULL),
(114, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:28:00', '2018-12-16 14:28:00', NULL),
(115, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:30:43', '2018-12-16 14:30:43', NULL),
(116, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:32:38', '2018-12-16 14:32:38', NULL),
(117, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:34:45', '2018-12-16 14:34:45', NULL),
(118, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:37:36', '2018-12-16 14:37:36', NULL),
(119, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:38:04', '2018-12-16 14:38:04', NULL),
(120, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:40:32', '2018-12-16 14:40:32', NULL),
(121, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:41:01', '2018-12-16 14:41:01', NULL),
(122, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:41:18', '2018-12-16 14:41:18', NULL),
(123, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:42:14', '2018-12-16 14:42:14', NULL),
(124, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:43:33', '2018-12-16 14:43:33', NULL),
(125, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:43:49', '2018-12-16 14:43:49', NULL),
(126, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:45:15', '2018-12-16 14:45:15', NULL),
(127, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:49:05', '2018-12-16 14:49:05', NULL),
(128, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:49:45', '2018-12-16 14:49:45', NULL),
(129, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:53:09', '2018-12-16 14:53:09', NULL),
(130, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:53:55', '2018-12-16 14:53:55', NULL),
(131, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:54:26', '2018-12-16 14:54:26', NULL),
(132, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:54:53', '2018-12-16 14:54:53', NULL),
(133, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:55:22', '2018-12-16 14:55:22', NULL),
(134, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:55:46', '2018-12-16 14:55:46', NULL),
(135, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:56:20', '2018-12-16 14:56:20', NULL),
(136, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:56:48', '2018-12-16 14:56:48', NULL),
(137, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:56:57', '2018-12-16 14:56:57', NULL),
(138, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 14:59:25', '2018-12-16 14:59:25', NULL),
(139, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:11:04', '2018-12-16 15:11:04', NULL),
(140, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:12:07', '2018-12-16 15:12:07', NULL),
(141, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:13:37', '2018-12-16 15:13:37', NULL),
(142, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:14:31', '2018-12-16 15:14:31', NULL),
(143, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:14:53', '2018-12-16 15:14:53', NULL),
(144, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:15:17', '2018-12-16 15:15:17', NULL),
(145, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:15:52', '2018-12-16 15:15:52', NULL),
(146, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:16:19', '2018-12-16 15:16:19', NULL),
(147, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:16:49', '2018-12-16 15:16:49', NULL),
(148, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:21:00', '2018-12-16 15:21:00', NULL),
(149, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:23:38', '2018-12-16 15:23:38', NULL),
(150, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:24:04', '2018-12-16 15:24:04', NULL),
(151, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:27:30', '2018-12-16 15:27:30', NULL),
(152, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:32:54', '2018-12-16 15:32:54', NULL),
(153, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:33:44', '2018-12-16 15:33:44', NULL),
(154, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:34:09', '2018-12-16 15:34:09', NULL),
(155, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:34:59', '2018-12-16 15:34:59', NULL),
(156, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:36:07', '2018-12-16 15:36:07', NULL),
(157, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:45:03', '2018-12-16 15:45:03', NULL),
(158, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:46:47', '2018-12-16 15:46:47', NULL),
(159, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:47:53', '2018-12-16 15:47:53', NULL),
(160, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:48:44', '2018-12-16 15:48:44', NULL),
(161, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:49:42', '2018-12-16 15:49:42', NULL),
(162, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:51:14', '2018-12-16 15:51:14', NULL),
(163, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:51:58', '2018-12-16 15:51:58', NULL),
(164, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:54:12', '2018-12-16 15:54:12', NULL),
(165, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:55:29', '2018-12-16 15:55:29', NULL),
(166, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 15:58:53', '2018-12-16 15:58:53', NULL),
(167, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:00:32', '2018-12-16 16:00:32', NULL),
(168, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:00:45', '2018-12-16 16:00:45', NULL),
(169, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:01:21', '2018-12-16 16:01:21', NULL),
(170, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:02:04', '2018-12-16 16:02:04', NULL),
(171, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:02:23', '2018-12-16 16:02:23', NULL),
(172, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:03:34', '2018-12-16 16:03:34', NULL),
(173, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:04:29', '2018-12-16 16:04:29', NULL),
(174, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:14:44', '2018-12-16 16:14:44', NULL),
(175, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:15:28', '2018-12-16 16:15:28', NULL),
(176, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:16:36', '2018-12-16 16:16:36', NULL),
(177, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:19:16', '2018-12-16 16:19:16', NULL),
(178, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:19:59', '2018-12-16 16:19:59', NULL),
(179, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:20:50', '2018-12-16 16:20:50', NULL),
(180, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:21:57', '2018-12-16 16:21:57', NULL),
(181, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 16:22:26', '2018-12-16 16:22:26', NULL),
(182, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 18:08:53', '2018-12-16 18:08:53', NULL),
(183, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 18:11:18', '2018-12-16 18:11:18', NULL),
(184, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 18:17:46', '2018-12-16 18:17:46', NULL),
(185, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:05:00', '2018-12-16 19:05:00', NULL),
(186, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:09:39', '2018-12-16 19:09:39', NULL),
(187, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:10:03', '2018-12-16 19:10:03', NULL),
(188, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:14:12', '2018-12-16 19:14:12', NULL),
(189, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:14:59', '2018-12-16 19:14:59', NULL),
(190, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:15:21', '2018-12-16 19:15:21', NULL),
(191, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:15:34', '2018-12-16 19:15:34', NULL),
(192, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-16 19:48:54', '2018-12-16 19:48:54', NULL),
(193, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:22:19', '2018-12-18 00:22:19', NULL),
(194, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:23:21', '2018-12-18 00:23:21', NULL),
(195, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:24:07', '2018-12-18 00:24:07', NULL),
(196, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:24:42', '2018-12-18 00:24:42', NULL),
(197, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:27:22', '2018-12-18 00:27:22', NULL),
(198, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:36:23', '2018-12-18 00:36:23', NULL),
(199, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 00:48:53', '2018-12-18 00:48:53', NULL),
(200, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:00:47', '2018-12-18 01:00:47', NULL),
(201, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:02:49', '2018-12-18 01:02:49', NULL),
(202, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:03:58', '2018-12-18 01:03:58', NULL),
(203, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:04:51', '2018-12-18 01:04:51', NULL),
(204, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:06:15', '2018-12-18 01:06:15', NULL),
(205, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:07:11', '2018-12-18 01:07:11', NULL),
(206, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:07:26', '2018-12-18 01:07:26', NULL),
(207, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:07:51', '2018-12-18 01:07:51', NULL),
(208, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:08:00', '2018-12-18 01:08:00', NULL),
(209, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:35:03', '2018-12-18 01:35:03', NULL),
(210, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:36:57', '2018-12-18 01:36:57', NULL),
(211, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:37:23', '2018-12-18 01:37:23', NULL),
(212, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:38:26', '2018-12-18 01:38:26', NULL),
(213, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:40:07', '2018-12-18 01:40:07', NULL),
(214, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:40:20', '2018-12-18 01:40:20', NULL),
(215, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:48:00', '2018-12-18 01:48:00', NULL),
(216, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:48:13', '2018-12-18 01:48:13', NULL),
(217, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:50:51', '2018-12-18 01:50:51', NULL),
(218, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:52:32', '2018-12-18 01:52:32', NULL),
(219, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 01:53:07', '2018-12-18 01:53:07', NULL),
(220, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 02:26:24', '2018-12-18 02:26:24', NULL),
(221, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 17:16:10', '2018-12-18 17:16:10', NULL),
(222, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 17:21:21', '2018-12-18 17:21:21', NULL),
(223, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-18 17:22:52', '2018-12-18 17:22:52', NULL),
(224, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-19 23:35:36', '2018-12-19 23:35:36', NULL),
(225, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-20 01:12:12', '2018-12-20 01:12:12', NULL),
(226, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-20 01:12:42', '2018-12-20 01:12:42', NULL),
(227, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-20 01:12:53', '2018-12-20 01:12:53', NULL),
(228, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 16:55:06', '2018-12-25 16:55:06', NULL),
(229, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 16:56:01', '2018-12-25 16:56:01', NULL),
(230, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 16:56:12', '2018-12-25 16:56:12', NULL),
(231, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 16:59:40', '2018-12-25 16:59:40', NULL),
(232, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:27:49', '2018-12-25 17:27:49', NULL),
(233, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:28:05', '2018-12-25 17:28:05', NULL),
(234, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:28:32', '2018-12-25 17:28:32', NULL),
(235, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:28:48', '2018-12-25 17:28:48', NULL),
(236, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:29:32', '2018-12-25 17:29:32', NULL),
(237, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:30:08', '2018-12-25 17:30:08', NULL),
(238, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:30:33', '2018-12-25 17:30:33', NULL),
(239, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:32:53', '2018-12-25 17:32:53', NULL),
(240, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:33:22', '2018-12-25 17:33:22', NULL),
(241, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:35:58', '2018-12-25 17:35:58', NULL),
(242, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:36:37', '2018-12-25 17:36:37', NULL),
(243, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:57:17', '2018-12-25 17:57:17', NULL),
(244, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:58:46', '2018-12-25 17:58:46', NULL),
(245, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:59:11', '2018-12-25 17:59:11', NULL),
(246, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:59:23', '2018-12-25 17:59:23', NULL),
(247, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 17:59:35', '2018-12-25 17:59:35', NULL),
(248, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:00:13', '2018-12-25 18:00:13', NULL),
(249, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:00:32', '2018-12-25 18:00:32', NULL),
(250, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:01:13', '2018-12-25 18:01:13', NULL),
(251, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:01:29', '2018-12-25 18:01:29', NULL),
(252, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:10:09', '2018-12-25 18:10:09', NULL),
(253, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:16:13', '2018-12-25 18:16:13', NULL),
(254, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:16:44', '2018-12-25 18:16:44', NULL),
(255, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:25:05', '2018-12-25 18:25:05', NULL),
(256, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:25:58', '2018-12-25 18:25:58', NULL),
(257, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:26:25', '2018-12-25 18:26:25', NULL),
(258, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:35:33', '2018-12-25 18:35:33', NULL),
(259, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:37:09', '2018-12-25 18:37:09', NULL),
(260, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:43:18', '2018-12-25 18:43:18', NULL),
(261, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:45:59', '2018-12-25 18:45:59', NULL),
(262, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:51:36', '2018-12-25 18:51:36', NULL),
(263, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:51:54', '2018-12-25 18:51:54', NULL),
(264, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:56:51', '2018-12-25 18:56:51', NULL),
(265, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:57:33', '2018-12-25 18:57:33', NULL),
(266, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:57:51', '2018-12-25 18:57:51', NULL),
(267, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:58:49', '2018-12-25 18:58:49', NULL),
(268, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 18:58:57', '2018-12-25 18:58:57', NULL),
(269, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:02:41', '2018-12-25 19:02:41', NULL),
(270, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:06:35', '2018-12-25 19:06:35', NULL),
(271, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:07:05', '2018-12-25 19:07:05', NULL),
(272, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:08:44', '2018-12-25 19:08:44', NULL),
(273, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:12:23', '2018-12-25 19:12:23', NULL),
(274, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:12:44', '2018-12-25 19:12:44', NULL),
(275, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:18:39', '2018-12-25 19:18:39', NULL),
(276, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:18:58', '2018-12-25 19:18:58', NULL),
(277, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:19:10', '2018-12-25 19:19:10', NULL),
(278, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:20:58', '2018-12-25 19:20:58', NULL),
(279, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:21:10', '2018-12-25 19:21:10', NULL),
(280, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:21:46', '2018-12-25 19:21:46', NULL),
(281, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:22:55', '2018-12-25 19:22:55', NULL),
(282, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-25 19:23:04', '2018-12-25 19:23:04', NULL),
(283, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-26 00:32:12', '2018-12-26 00:32:12', NULL),
(284, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2018-12-26 00:48:09', '2018-12-26 00:48:09', NULL),
(285, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-16 19:43:13', '2019-01-16 19:43:13', NULL),
(286, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-16 19:43:28', '2019-01-16 19:43:28', NULL),
(287, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-16 19:44:56', '2019-01-16 19:44:56', NULL),
(288, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-16 19:51:11', '2019-01-16 19:51:11', NULL),
(289, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-20 18:50:59', '2019-01-20 18:50:59', NULL),
(290, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-20 18:51:25', '2019-01-20 18:51:25', NULL),
(291, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-20 18:51:44', '2019-01-20 18:51:44', NULL),
(292, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-20 18:54:14', '2019-01-20 18:54:14', NULL),
(293, 'Joe', NULL, 'dedm st', 'joe@gmail.com', '01392349923', '2019-01-21 16:35:23', '2019-01-21 16:35:23', NULL),
(294, 'dslkj', NULL, 'lljljl  Neuss', 'yy@lk', '987987987', '2019-01-31 02:59:29', '2019-01-31 02:59:29', NULL),
(295, 'dslkj', NULL, 'lljljl 41460 Neuss', 'y3@lk', '987587987', '2019-01-31 03:00:32', '2019-01-31 03:00:32', NULL),
(296, 'dslkj  lkjlkj', NULL, 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:01:00', '2019-01-31 03:01:00', NULL),
(297, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:27:19', '2019-01-31 03:27:19', NULL),
(298, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:28:08', '2019-01-31 03:28:08', NULL),
(299, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:28:44', '2019-01-31 03:28:44', NULL),
(300, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:29:27', '2019-01-31 03:29:27', NULL),
(301, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:31:01', '2019-01-31 03:31:01', NULL),
(302, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:31:50', '2019-01-31 03:31:50', NULL),
(303, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:33:50', '2019-01-31 03:33:50', NULL),
(304, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:35:09', '2019-01-31 03:35:09', NULL),
(305, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:35:31', '2019-01-31 03:35:31', NULL),
(306, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:35:50', '2019-01-31 03:35:50', NULL),
(307, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:35:54', '2019-01-31 03:35:54', NULL),
(308, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:38:07', '2019-01-31 03:38:07', NULL),
(309, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:38:43', '2019-01-31 03:38:43', NULL),
(310, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:40:15', '2019-01-31 03:40:15', NULL),
(311, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:47:25', '2019-01-31 03:47:25', NULL),
(312, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:49:05', '2019-01-31 03:49:05', NULL),
(313, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:50:03', '2019-01-31 03:50:03', NULL),
(314, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 03:52:36', '2019-01-31 03:52:36', NULL),
(315, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:16:11', '2019-01-31 04:16:11', NULL),
(316, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:18:20', '2019-01-31 04:18:20', NULL),
(317, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:20:30', '2019-01-31 04:20:30', NULL),
(318, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:34:37', '2019-01-31 04:34:37', NULL),
(319, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:35:15', '2019-01-31 04:35:15', NULL),
(320, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:41:18', '2019-01-31 04:41:18', NULL),
(321, 'lkjlkj', 'dslkj', 'lljljl 41460 Neuss', 'y3@lk', '987587347', '2019-01-31 04:42:29', '2019-01-31 04:42:29', NULL),
(322, 'fras mustaa', 'bx bx', 'neumarkt 14 41460 Neuss', 'info@frasjaf.net', '015781184458', '2019-01-31 04:51:50', '2019-01-31 04:51:50', NULL),
(323, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 04:59:35', '2019-01-31 04:59:35', NULL),
(324, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:01:45', '2019-01-31 05:01:45', NULL),
(325, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:03:10', '2019-01-31 05:03:10', NULL),
(326, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:04:03', '2019-01-31 05:04:03', NULL),
(327, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:23:18', '2019-01-31 05:23:18', NULL),
(328, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:23:20', '2019-01-31 05:23:20', NULL),
(329, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:25:21', '2019-01-31 05:25:21', NULL),
(330, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:26:56', '2019-01-31 05:26:56', NULL),
(331, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:33:41', '2019-01-31 05:33:41', NULL),
(332, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:38:03', '2019-01-31 05:38:03', NULL),
(333, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:43:00', '2019-01-31 05:43:00', NULL),
(334, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:45:36', '2019-01-31 05:45:36', NULL),
(335, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:46:59', '2019-01-31 05:46:59', NULL),
(336, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:48:11', '2019-01-31 05:48:11', NULL),
(337, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:50:10', '2019-01-31 05:50:10', NULL),
(338, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:51:19', '2019-01-31 05:51:19', NULL),
(339, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:53:26', '2019-01-31 05:53:26', NULL),
(340, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 05:58:18', '2019-01-31 05:58:18', NULL),
(341, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 06:01:47', '2019-01-31 06:01:47', NULL),
(342, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 06:05:37', '2019-01-31 06:05:37', NULL),
(343, 'fras jaf', 'bx bx', 'neumarkt 14 33324 sssssd', 'info@frasjaf.net', '015781184458', '2019-01-31 06:06:28', '2019-01-31 06:06:28', NULL),
(344, 'ikjof', 'djsjj', 'oijoi 33443 Neuss', 'km3m@ko', '39499493993', '2019-01-31 21:54:07', '2019-01-31 21:54:07', NULL),
(345, 'rfrfkjo``', 'frfrf', 'ijroifjrij 41460 Neuss', 'uh@de', '348595345', '2019-01-31 22:02:09', '2019-01-31 22:02:09', NULL),
(346, 'rfrfkm', 'rfrf', 'oifrifj 41460 Neuss', 'ij@IJ', '3445453454', '2019-01-31 22:03:10', '2019-01-31 22:03:10', NULL),
(347, 'rfrfkm', 'rfrf', 'oifrifj 41460 Neuss', 'ij@IJ', '3445453454', '2019-01-31 22:05:04', '2019-01-31 22:05:04', NULL),
(348, 'kiokoiko', 'fr', 'ijoijoi 41460 Neuss', 'uh@UH', '9485748957', '2019-01-31 22:20:18', '2019-01-31 22:20:18', NULL),
(349, 'joijio', 'lkjk', 'oijoi 41460 Neuss', 'oj@k', '989676878', '2019-01-31 22:32:15', '2019-01-31 22:32:15', NULL),
(350, NULL, NULL, ' 41460 Neuss', NULL, NULL, '2019-01-31 22:33:16', '2019-01-31 22:33:16', NULL),
(351, 'loijoi', 'kmlkm', 'joijoi 98778 Neuss', 'hu@ji', '869879777', '2019-01-31 22:35:15', '2019-01-31 22:35:15', NULL),
(352, 'efwefefio', 'cdssdc', 'odieodoe 41460 Neuss', 'de@lo', '242545343223', '2019-02-01 07:06:00', '2019-02-01 07:06:00', NULL),
(353, 'kmk', 'vv', 'mkm 41460 Neuss', 'mj@jm.c', '49389358958', '2019-02-03 04:01:46', '2019-02-03 04:01:46', NULL),
(354, 'oijfr', 'deimi', 'iejdoej 41460 Neuss', 'lkk@lko', '98494857435', '2019-02-03 08:42:00', '2019-02-03 08:42:00', NULL),
(355, 'ijij', 'fv', 'oijoij 41460 Neuss', 'ju@ji.de', '965786575', '2019-02-05 05:29:02', '2019-02-05 05:29:02', NULL),
(356, 'ijeir', 'cdcfc', 'oijefri 41460 Neuss', 'ij@ik.de', '454535453', '2019-02-05 05:44:30', '2019-02-05 05:44:30', NULL),
(357, 'okoko', 'frfk', 'kokoko 41460 Neuss', 'ki@ki.de', '78657865765', '2019-02-06 06:25:00', '2019-02-06 06:25:00', NULL),
(358, 'oko', 'ded', 'ko 41460 Neuss', 'beshoytamry@gmail.com', '3453454534', '2019-02-06 06:48:50', '2019-02-06 06:48:50', NULL),
(359, 'oijo', 'ded', 'iojoij 41460 Neuss', 'beshoytamry@gmail.com', '765465476', '2019-02-06 07:10:09', '2019-02-06 07:10:09', NULL),
(360, 'okpokp', 'dedko', 'pokpok 41460 Neuss', 'beshoytamry@gmail.com', '765875758', '2019-02-06 07:20:55', '2019-02-06 07:20:55', NULL),
(361, 'okpo', 'dsko', '98789 41460 Neuss', 'beshoytamry@gmail.com', '9769876876', '2019-02-07 07:09:10', '2019-02-07 07:09:10', NULL),
(362, 'Fras Jaf', 'Fras', 'Neuss 41460 Neuss', 'info@frasjaf.net', '948374938478', '2019-02-07 07:25:12', '2019-02-07 07:25:12', NULL),
(363, 'ederf', 'deed', 'ijoij 41460 Neuss', 'ju@ki.de', '786575755', '2019-02-09 13:56:03', '2019-02-09 13:56:03', NULL),
(364, 'okefoekfo', 'vveok', 'ijfiorjw  ', 'beshoytamry@gmail.com', '453534535', '2019-03-08 19:53:39', '2019-03-08 19:53:39', NULL),
(365, 'kiekdeik', 'eded', 'okdepokdeokd  ', 'beshoytamry@gmail.com', '934857458734', '2019-04-14 15:45:57', '2019-04-14 15:45:57', NULL),
(366, 'pokfprofkprfk', 'cijijiojoij', 'pokfprfprij  ', 'beshoytamry@gmail.com', '342342384', '2019-04-14 15:50:07', '2019-04-14 15:50:07', NULL),
(367, 'eded', 'dede', 'eded  ', 'mi@ko.de', '304834233', '2019-05-13 15:18:52', '2019-05-13 15:18:52', NULL),
(368, 'okp', 'cdc', 'okpok  ', 'ji@d.de', '2329839489', '2019-05-15 13:55:44', '2019-05-15 13:55:44', NULL),
(369, 'ded', 'ded', 'ded  ', 'iju@de.de', '49487483475', '2019-05-15 13:59:39', '2019-05-15 13:59:39', NULL),
(370, 'oijoijoij', 'dededjii', 'jorjfrofjo  ', 'juu@de.ded', '38459485739', '2019-05-15 15:47:33', '2019-05-15 15:47:33', NULL),
(371, 'ijrifjro', 'ccfvji', 'irjoifjierjf  ', 'iji@de.de', '8749583479', '2019-05-15 15:50:12', '2019-05-15 15:50:12', NULL),
(372, 'ouhuihiuh', 'uhuifhuih', '979897979  ', 'hju@ko.d', '8965768576', '2019-05-15 16:10:38', '2019-05-15 16:10:38', NULL),
(373, 'oijoijoi', 'ijijoij', 'oijoijoi  ', 'ji@ki.xe', '0837498377', '2019-05-25 03:33:51', '2019-05-25 03:33:51', NULL),
(374, 'oijrifj', 'ijiejfrij', 'iojoij  ', 'i@k.de', '53445345541', '2019-05-25 03:46:14', '2019-05-25 03:46:14', NULL),
(375, 'efefji', 'oijijr', 'oijrifj  ', 'fr@kf.fr', '573498573498', '2019-09-11 20:48:46', '2019-09-11 20:48:46', NULL),
(376, 'frifjio', 'joijfrijf', 'iojfrifj 41460 ', 'iji@lfr.rf', '349584095', '2019-09-11 21:13:41', '2019-09-11 21:13:41', NULL),
(377, 'ofirjfroifj', 'oifjrofj', 'oifjerfj 41460 ', 'joi@fr.fr', '345409830495', '2019-09-11 21:18:51', '2019-09-11 21:18:51', NULL),
(378, 'vfrfrf', 'porgkepk', 'p 41460 ', 'ifj@lr.fr', '596564560394', '2019-09-11 21:28:50', '2019-09-11 21:28:50', NULL),
(379, 'frfrfkp', 'orfkrof', 'ifkrf 41460 Neuss', 'frff@kof.cr', '45365645767', '2019-09-11 21:36:08', '2019-09-11 21:36:08', NULL),
(380, 'deudheuh', 'uihfirufh', 'defrf 41460 Neuss', 'frf@fr.fr', '40534803495', '2019-09-11 22:34:23', '2019-09-11 22:34:23', NULL),
(381, 'frokp', 'pokpo', 'okpok 41460 Neuss', 'ko@rf.de', '485734985', '2019-09-11 22:42:24', '2019-09-11 22:42:24', NULL),
(382, 'deokp', 'pkfrifj', 'oijrfji 41460 Neuss', 'fr@dr.fr', '9386730584', '2019-09-11 22:45:43', '2019-09-11 22:45:43', NULL),
(383, 'ijgij', 'oiejgeoi', 'oiejggj 41460 Neuss', 'ed@de.de', '98673586450', '2019-09-11 22:49:00', '2019-09-11 22:49:00', NULL),
(384, 'leifnij', 'ijfrifjri', 'ijerogije 41460 Neuss', 'forkg@fr.fr', '3459045345', '2019-09-11 22:52:57', '2019-09-11 22:52:57', NULL),
(385, ';ogkpok', 'potkgto', 'poktrok 41460 Neuss', 'ko@ed.de', '505698504', '2019-09-11 22:55:31', '2019-09-11 22:55:31', NULL),
(386, 'okpdi', 'fkrfoij', 'iojoijg 41460 Neuss', 'fr@de.de', '3495847598', '2019-09-11 22:57:58', '2019-09-11 22:57:58', NULL),
(387, 'egrgr', 'gerg', 'gerg 41468 Neuss', 'ij@mo.fr', '349584098', '2019-09-11 23:52:12', '2019-09-11 23:52:12', NULL),
(388, 'rgokpo', 'kpokpok', 'okpok 41468 Neuss', 'koko@ko.de', '345345453', '2019-09-11 23:54:05', '2019-09-11 23:54:05', NULL),
(389, 'fsdf', 'ijorij', 'oijergj 41468 Neuss', 'frfr@rf.fr', '353485049', '2019-09-12 00:19:07', '2019-09-12 00:19:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE `foods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enough` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allergies` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgs/placeholder.jpg',
  `discount` int(11) NOT NULL DEFAULT '0',
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `name`, `enough`, `details`, `info`, `allergies`, `picture`, `discount`, `group_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Beef Steak', '3', 'deok', 'okefk', 'dedqd', 'images/1545786771_images.jpg', 23, 34, 27, '2018-12-04 10:06:39', '2019-01-16 19:33:31'),
(2, 'Diner Box', '3', 'ef', 'de', 'ede', 'images/1548377882_diner.jpg', 23, 34, 28, '2018-12-04 16:13:46', '2019-01-24 22:58:02'),
(4, 'ded', 'ded', NULL, 'de', 'dede', 'images/1552100336_download.jpg', 1, 33, 27, '2019-03-09 01:58:56', '2019-03-09 01:58:56'),
(5, 'pasta', '2', 'huhuh', 'huhnj', 'o', 'images/1552420454_download (1).jpg', 10, 33, 27, '2019-03-12 18:54:14', '2019-03-12 18:54:14'),
(7, 'Beef Steak - copy', '3', 'deok', 'okefk', 'dedqd', 'images/1545786771_images.jpg', 23, 34, 27, '2019-03-13 03:52:34', '2019-03-13 03:52:34');

-- --------------------------------------------------------

--
-- Table structure for table `food_options`
--

CREATE TABLE `food_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `food_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min` int(10) UNSIGNED NOT NULL,
  `max` int(10) UNSIGNED NOT NULL,
  `size_id` int(100) UNSIGNED NOT NULL,
  `req` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food_options`
--

INSERT INTO `food_options` (`id`, `food_id`, `name`, `min`, `max`, `size_id`, `req`, `created_at`, `updated_at`) VALUES
(24, 1, 'EXTRA 1', 0, 10, 0, 1, '2018-12-04 17:33:33', '2018-12-04 17:33:33'),
(25, 1, 'EXTRA 2', 0, 1, 0, 0, '2018-12-04 17:33:52', '2018-12-04 17:33:52'),
(26, 1, 'EXTRA FREI', 0, 30, 0, 0, '2018-12-04 17:34:17', '2018-12-04 17:34:17'),
(27, 2, 'EXTRA 1', 0, 3, 0, 1, '2018-12-04 17:37:36', '2018-12-04 17:37:36'),
(28, 2, 'EXTRA 2', 0, 10, 0, 0, '2018-12-04 17:37:59', '2018-12-04 17:37:59'),
(29, 1, 'chees', 0, 2, 0, 1, '2018-12-25 23:47:06', '2018-12-25 23:47:06'),
(30, 1, '2s', 2, 3, 6, 0, '2019-01-15 15:06:34', '2019-01-15 15:06:34'),
(31, 1, 'wu', 1, 2, 7, 0, '2019-01-15 15:08:38', '2019-01-15 15:08:38'),
(32, 1, 'ew', 1, 1, 6, 0, '2019-01-15 15:10:09', '2019-01-15 15:10:09'),
(33, 1, 'wq', 1, 2, 7, 0, '2019-01-15 15:13:22', '2019-01-15 15:13:22'),
(34, 1, 'ddw', 1, 2, 7, 0, '2019-01-15 15:15:52', '2019-01-15 15:15:52'),
(35, 1, 'cheese', 1, 1, 10, 0, '2019-01-15 15:31:08', '2019-01-15 15:31:08'),
(36, 1, 'cheese', 1, 1, 12, 0, '2019-01-15 15:34:37', '2019-01-15 15:34:37'),
(37, 1, 'cheese', 1, 1, 12, 0, '2019-01-15 15:35:16', '2019-01-15 15:35:16'),
(38, 1, 'cheese', 1, 1, 14, 0, '2019-01-15 15:38:18', '2019-01-15 15:38:18'),
(39, 1, 'cheese', 1, 1, 16, 0, '2019-01-15 15:42:11', '2019-01-15 15:42:11'),
(40, 1, 'cheese', 1, 1, 17, 0, '2019-01-15 15:44:04', '2019-01-15 15:44:04'),
(41, 1, 'w3', 22, 1, 18, 0, '2019-01-15 15:46:13', '2019-01-15 15:46:13'),
(42, 1, 'de', 1, 1, 18, 0, '2019-01-15 15:47:21', '2019-01-15 15:47:21'),
(43, 1, 'cheese', 1, 1, 19, 0, '2019-01-15 15:49:57', '2019-01-15 15:49:57'),
(44, 1, 'EXTRA 1', 1, 4, 20, 0, '2019-01-15 15:55:45', '2019-03-12 21:46:54'),
(46, 1, 's', 1, 1, 21, 0, '2019-01-16 15:12:31', '2019-01-16 15:12:31'),
(47, 1, 'w', 1, 1, 22, 0, '2019-01-16 15:17:34', '2019-01-16 15:17:34'),
(48, 1, 'sw', 1, 1, 22, 0, '2019-01-16 15:19:38', '2019-01-16 15:19:38'),
(49, 1, '2', 2, 2, 21, 0, '2019-01-16 15:19:50', '2019-01-16 15:19:50'),
(50, 1, 'ss', 1, 1, 22, 0, '2019-01-16 15:20:02', '2019-01-16 15:20:02'),
(51, 1, 's', 2, 2, 22, 0, '2019-01-16 15:20:27', '2019-01-16 15:20:27'),
(52, 1, 'sw', 1, 2, 21, 0, '2019-01-16 15:21:03', '2019-01-16 15:21:03'),
(53, 1, 'w', 1, 1, 21, 0, '2019-01-16 15:21:18', '2019-01-16 15:21:18'),
(54, 1, 'cv', 1, 1, 21, 0, '2019-01-16 15:21:46', '2019-01-16 15:21:46'),
(55, 1, 'xa', 1, 1, 21, 0, '2019-01-16 15:22:09', '2019-01-16 15:22:09'),
(56, 1, 'daaa', 1, 1, 22, 0, '2019-01-16 15:22:24', '2019-01-16 15:22:24'),
(57, 1, 'de', 1, 2, 35, 0, '2019-01-16 15:43:54', '2019-01-16 15:43:54'),
(58, 1, 'de', 1, 1, 36, 0, '2019-01-16 15:47:10', '2019-01-16 15:47:10'),
(59, 1, 'dw', 1, 1, 37, 0, '2019-01-16 15:48:23', '2019-01-16 15:48:23'),
(60, 1, 'de', 2, 2, 37, 0, '2019-01-16 15:49:26', '2019-01-16 15:49:26'),
(61, 1, 'ss', 1, 1, 37, 0, '2019-01-16 15:49:32', '2019-01-16 15:49:32'),
(62, 1, 'x', 2, 2, 38, 0, '2019-01-16 15:52:23', '2019-01-16 15:52:23'),
(63, 1, 'de', 22, 2, 39, 0, '2019-01-16 15:59:49', '2019-01-16 15:59:49'),
(64, 1, 'dd', 2, 2, 42, 0, '2019-01-16 16:03:59', '2019-01-16 16:03:59'),
(65, 1, 'EXTRA 1', 1, 1, 43, 1, '2019-01-16 18:48:18', '2019-01-16 18:48:18'),
(66, 1, 'EXTRA 1', 1, 1, 44, 0, '2019-01-16 18:51:21', '2019-01-16 18:51:21'),
(67, 2, 'cheese', 1, 3, 46, 0, '2019-01-24 23:23:04', '2019-01-25 02:54:48'),
(68, 2, 'chees 2', 1, 1, 45, 0, '2019-01-24 23:23:18', '2019-01-24 23:23:18'),
(69, 2, 'sauce', 1, 1, 46, 1, '2019-01-24 23:23:33', '2019-01-24 23:23:33'),
(76, 1, 'EXTRA 1', 1, 1, 49, 0, '2019-03-08 20:40:34', '2019-03-08 20:40:34'),
(77, 1, 'EXTRA 1', 1, 1, 50, 0, '2019-03-08 20:59:07', '2019-03-08 20:59:07'),
(78, 2, 'EXTRA 1', 1, 1, 51, 0, '2019-03-08 21:00:31', '2019-03-08 21:00:31'),
(79, 2, 'de', 1, 23, 52, 0, '2019-03-08 21:09:00', '2019-03-08 21:09:00'),
(80, 2, 'de', 1, 23, 53, 0, '2019-03-08 21:09:15', '2019-03-08 21:09:15'),
(81, 2, 'de', 1, 23, 54, 0, '2019-03-08 21:09:59', '2019-03-08 21:09:59'),
(82, 2, 'EXTRA 1', 1, 1, 55, 0, '2019-03-08 22:19:58', '2019-03-08 22:19:58'),
(87, 7, 'EXTRA 1', 1, 4, 60, 0, '2019-03-13 03:52:34', '2019-03-13 03:52:34'),
(88, 7, 'EXTRA 1', 1, 1, 61, 0, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(89, 7, 'EXTRA 1', 1, 1, 62, 0, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(90, 7, 'EXTRA 1', 1, 1, 63, 0, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(91, 1, 'EXTRA 1', 1, 1, 68, 0, '2019-03-13 04:03:22', '2019-03-13 04:03:22'),
(92, 1, 'EXTRA 1', 1, 1, 69, 0, '2019-03-13 04:03:33', '2019-03-13 04:03:33'),
(93, 1, 'EXTRA 1', 1, 1, 70, 0, '2019-03-13 04:03:53', '2019-03-13 04:03:53'),
(94, 1, 'EXTRA 1', 1, 1, 71, 0, '2019-03-13 04:04:03', '2019-03-13 04:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `food_order`
--

CREATE TABLE `food_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `food_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `size` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `discount` decimal(8,2) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food_order`
--

INSERT INTO `food_order` (`id`, `order_id`, `food_id`, `count`, `size`, `price`, `discount`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'size 20cm', '0.00', '23.00', NULL, NULL, NULL),
(2, 1, 2, 1, 'size 2', '0.00', '23.00', NULL, NULL, NULL),
(3, 1, 2, 1, 'size 2', '0.00', '23.00', NULL, NULL, NULL),
(4, 2, 1, 1, 'size 20cm', '0.00', '23.00', NULL, NULL, NULL),
(5, 2, 2, 1, 'size 2', '0.00', '23.00', NULL, NULL, NULL),
(6, 2, 2, 1, 'size 2', '0.00', '23.00', NULL, NULL, NULL),
(7, 3, 1, 1, 'size 20cm', '0.00', '23.00', NULL, NULL, NULL),
(8, 3, 2, 1, 'size 2', '0.00', '23.00', NULL, NULL, NULL),
(9, 3, 2, 1, 'size 2', '0.00', '23.00', NULL, NULL, NULL),
(10, 5, 1, 1, 'size 20cm', '2.00', '2.00', NULL, NULL, NULL),
(11, 5, 2, 1, 'size 2', '40.00', '31.00', NULL, NULL, NULL),
(12, 5, 2, 1, 'size 2', '40.00', '31.00', NULL, NULL, NULL),
(13, 6, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(14, 6, 2, 1, 'size 2', '40.00', '30.80', NULL, NULL, NULL),
(15, 6, 2, 1, 'size 2', '40.00', '30.80', NULL, NULL, NULL),
(16, 7, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(17, 7, 2, 1, 'size 2', '40.00', '30.80', NULL, NULL, NULL),
(18, 7, 2, 1, 'size 2', '40.00', '30.80', NULL, NULL, NULL),
(19, 8, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(20, 8, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(21, 9, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(22, 9, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(23, 9, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(24, 10, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(25, 10, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(26, 10, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(27, 11, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(28, 11, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(29, 11, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(30, 12, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(31, 12, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(32, 12, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(33, 13, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(34, 13, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(35, 13, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(36, 14, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(37, 14, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(38, 14, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(39, 15, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(40, 15, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(41, 15, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(42, 16, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(43, 16, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(44, 16, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(45, 17, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(46, 17, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(47, 17, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(48, 18, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(49, 18, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(50, 18, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(51, 19, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(52, 19, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(53, 19, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(54, 20, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(55, 20, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(56, 20, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(57, 21, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(58, 21, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(59, 21, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(60, 22, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(61, 22, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(62, 22, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(63, 23, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(64, 23, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(65, 23, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(66, 24, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(67, 24, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(68, 24, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(69, 25, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(70, 25, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(71, 25, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(72, 26, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(73, 26, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(74, 26, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(75, 27, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(76, 27, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(77, 27, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(78, 28, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(79, 28, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(80, 28, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(81, 29, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(82, 29, 1, 2, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(83, 29, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(84, 30, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(85, 31, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(86, 32, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(87, 33, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(88, 34, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(89, 35, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(90, 36, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(91, 37, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(92, 37, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(93, 38, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(94, 38, 2, 1, 'size 1', '20.00', '15.40', NULL, NULL, NULL),
(95, 38, 1, 1, 'size 2s0m', '31.00', '23.87', 'dcfv', NULL, NULL),
(96, 38, 2, 1, 'size 2', '40.00', '30.80', 'vvvvvv', NULL, NULL),
(97, 39, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(98, 40, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(99, 40, 2, 1, 'size 1', '20.00', '15.40', NULL, NULL, NULL),
(100, 41, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(101, 42, 1, 1, 'size 20cm', '2.00', '1.54', NULL, NULL, NULL),
(102, 42, 2, 2, 'size 2', '40.00', '30.80', NULL, NULL, NULL),
(103, 43, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(104, 44, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(105, 45, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(106, 46, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(107, 47, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(108, 47, 2, 1, 'size 1', '20.00', '15.40', NULL, NULL, NULL),
(109, 48, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(110, 49, 1, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(111, 50, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(112, 51, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(113, 52, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(114, 53, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(115, 54, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(116, 54, 7, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(117, 55, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(118, 55, 7, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(119, 56, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(120, 56, 7, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(121, 57, 7, 1, 'size 2s0m', '31.00', '23.87', NULL, NULL, NULL),
(122, 58, 1, 2, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(123, 59, 1, 2, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(124, 60, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(125, 61, 1, 2, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(126, 61, 7, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(127, 62, 7, 1, 'size 2s0', '34.00', '26.18', NULL, NULL, NULL),
(128, 63, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(129, 64, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(130, 65, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(131, 66, 1, 2, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(132, 67, 1, 2, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(133, 68, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(134, 69, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(135, 70, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(136, 71, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(137, 72, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL),
(138, 73, 1, 1, 'size 2s0mww', '31.00', '23.87', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `group_id`, `created_at`, `updated_at`) VALUES
(33, 'Lords', NULL, '2019-01-16 18:23:10', '2019-01-16 18:44:19'),
(34, 'lord 2', 33, '2019-01-16 18:36:00', '2019-01-16 18:43:47'),
(35, 'lord 1', 33, '2019-01-16 18:36:14', '2019-01-16 18:43:47'),
(37, 'test', NULL, '2019-01-16 19:20:36', '2019-01-16 19:20:36');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:49;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-08 21:53:48.998513\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552078428, 1552078426),
(2, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:49;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-08 21:54:39.729415\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552078479, 1552078476),
(3, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:17:55.066397\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552112275, 1552112272),
(4, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:49;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:21:06.517348\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552112466, 1552112463),
(5, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:37;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:21:15.105839\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552112475, 1552112472),
(6, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:49;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:38:45.940943\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113525, 1552113522),
(7, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:37;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:44:36.293982\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113876, 1552113873),
(8, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:37;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:44:36.963021\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113876, 1552113873),
(9, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:37;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:44:37.591057\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113877, 1552113874),
(10, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:36;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:44:56.586143\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113896, 1552113893),
(11, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:36;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:45:54.223440\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113954, 1552113951),
(12, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:46:28.877422\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113988, 1552113985),
(13, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:46:34.302732\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552113994, 1552113991),
(14, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-09 07:47:24.431599\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552114044, 1552114041),
(15, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:38;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-13 00:29:51.053320\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552433391, 1552433388),
(16, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:40;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-13 00:31:45.071842\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552433505, 1552433502),
(17, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:40;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-13 00:31:47.124959\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552433507, 1552433504),
(18, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:40;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-13 00:31:48.122016\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552433508, 1552433505),
(19, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:32;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-03-13 00:32:10.947322\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552433530, 1552433527),
(20, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:50;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-04-14 17:46:06.115879\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1555256766, 1555256763),
(21, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-04-14 17:50:12.223955\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1555257012, 1555257009),
(22, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-04-14 17:57:10.295867\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1555257430, 1555257427),
(23, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-04-14 17:57:13.247036\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1555257433, 1555257430),
(24, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-04-14 17:57:14.120086\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1555257434, 1555257431),
(25, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:54;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-15 15:59:44.497075\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1557928784, 1557928781),
(26, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:56;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-15 17:50:17.209444\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1557935417, 1557935414),
(27, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:57;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-15 18:10:42.975554\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1557936642, 1557936639),
(28, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:58;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-25 05:33:55.462843\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1558755235, 1558755232),
(29, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-25 05:46:18.690353\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1558755978, 1558755975),
(30, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-25 05:57:39.132272\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1558756659, 1558756656),
(31, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-25 05:57:40.663360\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1558756660, 1558756657),
(32, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-05-25 05:57:41.670417\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1558756661, 1558756658),
(33, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:60;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-11 23:13:45.371132\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568236425, 1568236422),
(34, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:61;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-11 23:18:55.692881\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568236735, 1568236732),
(35, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:62;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-11 23:28:54.054106\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568237334, 1568237331),
(36, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:63;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-11 23:36:12.785200\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568237772, 1568237769),
(37, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:64;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:34:27.150066\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568241267, 1568241264),
(38, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:65;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:42:28.812615\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568241748, 1568241745),
(39, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:66;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:45:47.532982\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568241947, 1568241944),
(40, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:67;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:49:04.863268\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568242144, 1568242141),
(41, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:68;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:53:02.382854\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568242382, 1568242379),
(42, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:69;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:55:35.735625\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568242535, 1568242532),
(43, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:70;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 00:58:17.559881\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568242697, 1568242694),
(44, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:72;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 01:54:24.858479\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568246064, 1568246061),
(45, 'default', '{\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":8:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:73;s:9:\\\"relations\\\";a:2:{i:0;s:8:\\\"customer\\\";i:1;s:10:\\\"restaurant\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-09-12 02:19:11.652519\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Europe\\/Berlin\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1568247551, 1568247548);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_24_213813_create_categories_table', 1),
(4, '2018_11_24_214517_create_restaurants_table', 1),
(5, '2018_11_24_214550_create_foods_table', 1),
(6, '2018_11_24_214620_create_customers_table', 1),
(7, '2018_11_24_214640_create_orders_table', 1),
(8, '2018_11_24_214711_create_food_order_table', 1),
(9, '2018_11_24_214733_create_food_options_table', 1),
(10, '2018_11_24_214924_create_selected_options_table', 1),
(11, '2018_11_24_214942_create_order_status_table', 1),
(12, '2018_11_24_222913_create_option_items_table', 1),
(13, '2013_11_24_214517_create_restaurants_table', 1),
(14, '2018_11_24_225936_create_permission_tables', 2),
(15, '2018_12_12_205214_create_jobs_table', 3),
(16, '2018_12_16_192839_places', 4),
(17, '2013_01_09_150250_create_group_table', 5),
(19, '2018_11_24_214551_create_sizes_table', 6),
(20, '2019_03_09_090459_create_reports_table', 7),
(21, '2019_04_19_200820_create_coupon_table', 8),
(22, '2019_04_24_153603_add_new_column_to_orders', 8),
(23, '2016_06_01_000001_create_oauth_auth_codes_table', 9),
(24, '2016_06_01_000002_create_oauth_access_tokens_table', 9),
(25, '2016_06_01_000003_create_oauth_refresh_tokens_table', 9),
(26, '2016_06_01_000004_create_oauth_clients_table', 9),
(27, '2016_06_01_000005_create_oauth_personal_access_clients_table', 9),
(28, '2019_06_18_203445_alter_customer_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 8),
(1, 'App\\User', 14),
(2, 'App\\User', 12),
(2, 'App\\User', 15),
(2, 'App\\User', 16),
(2, 'App\\User', 17),
(3, 'App\\User', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Restaurant Personal Access Client', 'aMSKgdMh4qwuroU97n0M0t5uRR3Z1cA9QOh9NRMZ', 'http://localhost', 1, 0, 0, '2019-07-06 19:19:41', '2019-07-06 19:19:41'),
(2, NULL, 'Restaurant Password Grant Client', 'OdBxhg2NrcxUMPDvQplq0BSco52ZIySDMq1KkT3i', 'http://localhost', 0, 1, 0, '2019-07-06 19:19:41', '2019-07-06 19:19:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-06 19:19:41', '2019-07-06 19:19:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `option_items`
--

CREATE TABLE `option_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `food_option_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_items`
--

INSERT INTO `option_items` (`id`, `food_option_id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(15, 24, 'GURKE', 0.00, '2018-12-04 17:34:46', '2018-12-04 17:34:46'),
(17, 25, 'TUHNFISCH', 2.00, '2018-12-04 17:35:13', '2018-12-04 17:35:13'),
(18, 25, 'NUDELN', 4.00, '2018-12-04 17:35:26', '2018-12-04 17:35:26'),
(19, 26, 'TOMATEN SUSE', 1.00, '2018-12-04 17:35:42', '2018-12-04 17:35:42'),
(20, 26, 'GURCKE', 3.00, '2018-12-04 17:35:54', '2018-12-04 17:35:54'),
(21, 27, 'TUHNFISCH', 2.00, '2018-12-04 17:38:15', '2018-12-04 17:38:15'),
(22, 27, 'TOMATEN', 2.00, '2018-12-04 17:38:24', '2018-12-04 17:38:24'),
(23, 28, 'kofta', 3.00, '2018-12-04 17:38:39', '2018-12-04 17:38:39'),
(24, 28, 'GURCKE', 0.00, '2018-12-04 17:38:51', '2018-12-04 17:38:51'),
(25, 27, 'TOMATEN', 2.00, '2018-12-04 18:15:43', '2018-12-04 18:15:43'),
(27, 24, 'q', 0.00, '2018-12-25 23:13:39', '2018-12-25 23:13:39'),
(28, 24, 'w', 2.00, '2018-12-25 23:16:04', '2018-12-25 23:16:04'),
(30, 24, 'e', 1.00, '2018-12-25 23:39:27', '2018-12-25 23:39:27'),
(31, 24, 'de', 3.00, '2018-12-25 23:42:02', '2018-12-25 23:42:02'),
(32, 25, 'de', 3.00, '2018-12-25 23:42:23', '2018-12-25 23:42:23'),
(33, 63, 's', 2.00, '2019-01-16 15:59:57', '2019-01-16 15:59:57'),
(34, 63, 'sw', 32.00, '2019-01-16 16:00:03', '2019-01-16 16:00:03'),
(35, 44, 'tamata', 1.00, '2019-01-16 18:49:46', '2019-01-16 18:49:46'),
(37, 44, 'dd', 0.00, '2019-01-16 18:50:13', '2019-01-16 18:50:13'),
(38, 66, 'Pizza', 2.00, '2019-01-16 18:51:32', '2019-01-16 18:51:32'),
(39, 66, 'cheese', 1.00, '2019-01-16 18:51:38', '2019-01-16 18:51:38'),
(40, 69, 'white', 0.00, '2019-01-24 23:23:51', '2019-01-24 23:23:51'),
(41, 69, 'red', 1.00, '2019-01-24 23:24:00', '2019-01-24 23:24:00'),
(42, 67, 'blue', 2.00, '2019-01-24 23:24:18', '2019-01-24 23:24:18'),
(43, 67, 'parmisan', 3.00, '2019-01-24 23:24:29', '2019-01-24 23:24:29'),
(44, 68, 'blue', 1.00, '2019-01-24 23:24:39', '2019-01-24 23:24:39'),
(45, 68, 'green', 3.00, '2019-01-24 23:24:45', '2019-01-24 23:24:45'),
(54, 76, 'tamata', 1.00, '2019-03-08 20:40:34', '2019-03-08 20:40:34'),
(55, 76, 'dd', 0.00, '2019-03-08 20:40:34', '2019-03-08 20:40:34'),
(56, 77, 'tamata', 1.00, '2019-03-08 20:59:07', '2019-03-08 20:59:07'),
(57, 77, 'dd', 0.00, '2019-03-08 20:59:07', '2019-03-08 20:59:07'),
(58, 78, 'tamata', 1.00, '2019-03-08 21:00:31', '2019-03-08 21:00:31'),
(59, 78, 'dd', 0.00, '2019-03-08 21:00:31', '2019-03-08 21:00:31'),
(60, 80, '1', 1.00, '2019-03-08 21:09:44', '2019-03-08 21:09:44'),
(61, 80, '2', 2.00, '2019-03-08 21:09:51', '2019-03-08 21:09:51'),
(62, 81, '1', 1.00, '2019-03-08 21:09:59', '2019-03-08 21:09:59'),
(63, 81, '2', 2.00, '2019-03-08 21:09:59', '2019-03-08 21:09:59'),
(65, 82, 'dd', 0.00, '2019-03-08 22:19:59', '2019-03-08 22:19:59'),
(66, 82, 'dffd', 3.00, '2019-03-08 22:21:12', '2019-03-08 22:21:12'),
(67, 44, 'edee', 0.00, '2019-03-10 21:40:00', '2019-03-10 21:40:00'),
(77, 87, 'tamata', 1.00, '2019-03-13 03:52:34', '2019-03-13 03:52:34'),
(78, 87, 'dd', 0.00, '2019-03-13 03:52:34', '2019-03-13 03:52:34'),
(79, 87, 'edee', 0.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(80, 88, 'Pizza', 2.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(81, 88, 'cheese', 1.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(82, 89, 'tamata', 1.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(83, 89, 'dd', 0.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(84, 90, 'tamata', 1.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(85, 90, 'dd', 0.00, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(86, 91, 'tamata', 1.00, '2019-03-13 04:03:22', '2019-03-13 04:03:22'),
(87, 91, 'dd', 0.00, '2019-03-13 04:03:22', '2019-03-13 04:03:22'),
(88, 92, 'tamata', 1.00, '2019-03-13 04:03:33', '2019-03-13 04:03:33'),
(89, 92, 'dd', 0.00, '2019-03-13 04:03:33', '2019-03-13 04:03:33'),
(90, 93, 'tamata', 1.00, '2019-03-13 04:03:53', '2019-03-13 04:03:53'),
(91, 93, 'dd', 0.00, '2019-03-13 04:03:53', '2019-03-13 04:03:53'),
(92, 94, 'tamata', 1.00, '2019-03-13 04:04:03', '2019-03-13 04:04:03'),
(93, 94, 'dd', 0.00, '2019-03-13 04:04:03', '2019-03-13 04:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `total` double(8,2) DEFAULT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pre_order` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `request_origin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `code`, `restaurant_id`, `customer_id`, `total`, `details`, `pre_order`, `status`, `created_at`, `updated_at`, `request_origin`, `coupon_id`) VALUES
(1, '21548915371', 2, 315, 79.14, 'lijijio', NULL, 3, '2019-01-31 04:16:11', '2019-03-09 05:17:51', NULL, NULL),
(2, '21548915500', 2, 316, 79.14, 'lijijio', NULL, 3, '2019-01-31 04:18:20', '2019-03-09 05:47:21', NULL, NULL),
(3, '21548915630', 2, 317, 79.14, 'lijijio', NULL, 0, '2019-01-31 04:20:30', '2019-01-31 04:20:31', NULL, NULL),
(4, '21548916477', 2, 318, NULL, 'lijijio', NULL, 0, '2019-01-31 04:34:37', '2019-01-31 04:34:37', NULL, NULL),
(5, '21548916515', 2, 319, 79.14, 'lijijio', NULL, 0, '2019-01-31 04:35:15', '2019-01-31 04:35:15', NULL, NULL),
(6, '21548916878', 2, 320, 79.14, 'lijijio', NULL, 0, '2019-01-31 04:41:18', '2019-01-31 04:41:19', NULL, NULL),
(7, '21548916949', 2, 321, 79.14, 'lijijio', NULL, 0, '2019-01-31 04:42:29', '2019-01-31 04:42:30', NULL, NULL),
(8, '31548917511', 3, 322, 30.41, 'fdfds', NULL, 0, '2019-01-31 04:51:51', '2019-01-31 04:51:51', NULL, NULL),
(9, '31548917975', 3, 323, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 04:59:35', '2019-01-31 04:59:35', NULL, NULL),
(10, '31548918106', 3, 324, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:01:46', '2019-01-31 05:01:46', NULL, NULL),
(11, '31548918190', 3, 325, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:03:10', '2019-01-31 05:03:11', NULL, NULL),
(12, '31548918243', 3, 326, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:04:03', '2019-01-31 05:04:03', NULL, NULL),
(13, '31548919398', 3, 327, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:23:18', '2019-01-31 05:23:18', NULL, NULL),
(14, '31548919400', 3, 328, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:23:20', '2019-01-31 05:23:20', NULL, NULL),
(15, '31548919521', 3, 329, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:25:21', '2019-01-31 05:25:21', NULL, NULL),
(16, '31548919616', 3, 330, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:26:56', '2019-01-31 05:26:57', NULL, NULL),
(17, '31548920021', 3, 331, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:33:41', '2019-01-31 05:33:42', NULL, NULL),
(18, '31548920283', 3, 332, 54.28, 'fdsdsfdsf', NULL, 3, '2019-01-31 05:38:03', '2019-01-31 05:40:43', NULL, NULL),
(19, '31548920580', 3, 333, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:43:00', '2019-01-31 05:43:00', NULL, NULL),
(20, '31548920736', 3, 334, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:45:36', '2019-01-31 05:45:36', NULL, NULL),
(21, '31548920819', 3, 335, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:46:59', '2019-01-31 05:47:00', NULL, NULL),
(22, '31548920891', 3, 336, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:48:11', '2019-01-31 05:48:11', NULL, NULL),
(23, '31548921010', 3, 337, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:50:10', '2019-01-31 05:50:10', NULL, NULL),
(24, '31548921079', 3, 338, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:51:19', '2019-01-31 05:51:19', NULL, NULL),
(25, '31548921206', 3, 339, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:53:26', '2019-01-31 05:53:26', NULL, NULL),
(26, '31548921498', 3, 340, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 05:58:18', '2019-01-31 05:58:18', NULL, NULL),
(27, '31548921707', 3, 341, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 06:01:47', '2019-01-31 06:01:47', NULL, NULL),
(28, '31548921937', 3, 342, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 06:05:37', '2019-01-31 06:05:37', NULL, NULL),
(29, '31548921988', 3, 343, 54.28, 'fdsdsfdsf', NULL, 0, '2019-01-31 06:06:28', '2019-01-31 06:06:28', NULL, NULL),
(30, '21548978850', 2, 344, 33.87, 'iij', NULL, 0, '2019-01-31 21:54:10', '2019-01-31 21:54:14', NULL, NULL),
(31, '21548979329', 2, 345, 33.87, 'fvfv', NULL, 0, '2019-01-31 22:02:09', '2019-01-31 22:02:10', NULL, NULL),
(32, '21548979504', 2, 347, 33.87, 'CFCS', NULL, 4, '2019-01-31 22:05:04', '2019-03-12 22:32:07', NULL, NULL),
(33, '21548980418', 2, 348, 33.87, 'IUHIUH', NULL, 0, '2019-01-31 22:20:18', '2019-01-31 22:20:18', NULL, NULL),
(34, '21548981135', 2, 349, 33.87, 'sss', NULL, 0, '2019-01-31 22:32:15', '2019-01-31 22:32:15', NULL, NULL),
(35, '21548981196', 2, 350, 33.87, NULL, NULL, 0, '2019-01-31 22:33:16', '2019-01-31 22:33:16', NULL, NULL),
(36, '21548981315', 2, 351, 33.87, 'lkjl', NULL, 3, '2019-01-31 22:35:15', '2019-03-09 05:45:51', NULL, NULL),
(37, '21549011960', 2, 352, 57.74, 'wfrfer', NULL, 3, '2019-02-01 07:06:00', '2019-03-09 05:44:33', NULL, NULL),
(38, '21549170106', 2, 353, 88.61, 'cfvfvf', NULL, 4, '2019-02-03 04:01:46', '2019-03-12 22:29:47', NULL, NULL),
(39, '21549186920', 2, 354, 33.87, 'deded', 'preorder Tuesday 6:00', 0, '2019-02-03 08:42:00', '2019-02-03 08:42:00', NULL, NULL),
(40, '21549348142', 2, 355, 49.27, NULL, 'Now', 3, '2019-02-05 05:29:02', '2019-03-12 22:31:45', NULL, NULL),
(41, '21549349071', 2, 356, 33.87, 'dcdcd', 'preorder Tuesday 6:00', 3, '2019-02-05 05:44:31', '2019-02-05 05:44:58', NULL, NULL),
(42, '21549437901', 2, 357, 44.34, 'ijkiojio', 'Now', 0, '2019-02-06 06:25:01', '2019-02-06 06:25:02', NULL, NULL),
(43, '21549439330', 2, 358, 33.87, 'deded', 'Now', 0, '2019-02-06 06:48:50', '2019-02-06 06:48:50', NULL, NULL),
(44, '21549440609', 2, 359, 33.87, NULL, 'Now', 0, '2019-02-06 07:10:09', '2019-02-06 07:10:09', NULL, NULL),
(45, '21549441256', 2, 360, 33.87, 'ffvfv', 'Now', 0, '2019-02-06 07:20:56', '2019-02-06 07:20:56', NULL, NULL),
(46, '21549526950', 2, 361, 33.87, NULL, 'Now', 0, '2019-02-07 07:09:10', '2019-02-07 07:09:10', NULL, NULL),
(47, '21549527912', 2, 362, 53.27, NULL, 'Now', 3, '2019-02-07 07:25:12', '2019-02-07 07:31:02', NULL, NULL),
(48, '21549724163', 2, 363, 33.87, NULL, 'Now', 3, '2019-02-09 13:56:03', '2019-02-09 13:56:20', NULL, NULL),
(49, '21552078420', 2, 364, 33.87, 'frfrrgrg', 'preorder Tuesday 7:00', 3, '2019-03-08 19:53:40', '2019-03-09 05:38:42', NULL, NULL),
(50, '21555256757', 2, 365, 33.87, 'dedededede', 'preorder Tuesday 6:00', 0, '2019-04-14 15:45:57', '2019-04-14 15:45:59', NULL, NULL),
(51, '21555257007', 2, 366, 33.87, 'frf', 'preorder Wednesday 6:30', 3, '2019-04-14 15:50:07', '2019-04-14 15:57:11', NULL, NULL),
(52, '21557760732', 2, 367, NULL, 'dekod', 'Now', 0, '2019-05-13 15:18:52', '2019-05-13 15:18:52', 'http://localhost:4200', NULL),
(53, '21557928544', 2, 368, NULL, 'de', 'Now', 0, '2019-05-15 13:55:44', '2019-05-15 13:55:44', 'http://localhost:4200', NULL),
(54, '21557928779', 2, 369, 57.74, 'de', 'Now', 0, '2019-05-15 13:59:39', '2019-05-15 13:59:40', 'http://localhost:4200', NULL),
(55, '21557935253', 2, 370, NULL, 'errfrf', 'Now', 0, '2019-05-15 15:47:33', '2019-05-15 15:47:33', 'http://localhost:4200', NULL),
(56, '21557935412', 2, 371, 55.74, NULL, 'Now', 0, '2019-05-15 15:50:12', '2019-05-15 15:50:13', 'http://localhost:4200', NULL),
(57, '21557936638', 2, 372, 30.87, NULL, 'Now', 0, '2019-05-15 16:10:38', '2019-05-15 16:10:39', 'http://localhost:4200', 13),
(58, '21558755231', 2, 373, 35.87, NULL, 'preorder Tuesday 5:00', 0, '2019-05-25 03:33:51', '2019-05-25 03:33:51', 'http://localhost:4200', 13),
(59, '21558755974', 2, 374, 56.74, NULL, 'preorder Monday 5:00', 3, '2019-05-25 03:46:14', '2019-05-25 03:57:38', 'http://localhost:4200', 13),
(60, '21568236421', 2, 376, 33.15, 'ijoeirjfrij', 'Now', 0, '2019-09-11 21:13:41', '2019-09-11 21:13:41', 'http://localhost:8000', 6),
(61, '21568236731', 2, 377, 84.61, 'jfriofjer', 'preorder', 0, '2019-09-11 21:18:51', '2019-09-11 21:18:51', 'http://localhost:8000', NULL),
(62, '21568237330', 2, 378, 35.39, 'preirgjeo', 'preorder', 0, '2019-09-11 21:28:50', '2019-09-11 21:28:50', 'http://localhost:8000', 6),
(63, '21568237768', 2, 379, 34.87, 'oijrfoerfj', 'preorder Thursday 4:00', 0, '2019-09-11 21:36:08', '2019-09-11 21:36:09', 'http://localhost:8000', NULL),
(64, '21568241263', 2, 380, 33.87, 'frfe', 'Now', 0, '2019-09-11 22:34:23', '2019-09-11 22:34:23', 'http://localhost:8000', NULL),
(65, '21568241744', 2, 381, 33.87, '43kpok', 'Now', 0, '2019-09-11 22:42:24', '2019-09-11 22:42:25', 'http://localhost:8000', NULL),
(66, '21568241943', 2, 382, 58.75, 'rijeo', 'Now', 0, '2019-09-11 22:45:43', '2019-09-11 22:45:43', 'http://localhost:8000', 9),
(67, '21568242140', 2, 383, 58.75, 'goerigjei', 'Now', 0, '2019-09-11 22:49:00', '2019-09-11 22:49:00', 'http://localhost:8000', 9),
(68, '21568242378', 2, 384, 34.87, 'oigjeigj', 'Now', 0, '2019-09-11 22:52:58', '2019-09-11 22:52:58', 'http://localhost:8000', NULL),
(69, '21568242531', 2, 385, 33.87, 'prokhtok', 'Now', 0, '2019-09-11 22:55:31', '2019-09-11 22:55:31', 'http://localhost:8000', NULL),
(70, '21568242678', 2, 386, 33.87, 'oijeogijt', 'Now', 0, '2019-09-11 22:57:58', '2019-09-11 22:57:58', 'http://localhost:8000', NULL),
(71, '31568245932', 3, 387, 29.87, 'ijioji', 'Now', 0, '2019-09-11 23:52:12', '2019-09-11 23:52:12', 'http://localhost:8000', NULL),
(72, '31568246045', 3, 388, 28.87, 'okfro', 'Now', 0, '2019-09-11 23:54:05', '2019-09-11 23:54:05', 'http://localhost:8000', NULL),
(73, '21568247547', 2, 389, 33.87, 'roijergj', 'preorder Friday 3:30', 0, '2019-09-12 00:19:07', '2019-09-12 00:19:07', 'http://localhost:8000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `order_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'created', '2019-01-31 04:16:12', '2019-01-31 04:16:12'),
(2, 2, NULL, 'created', '2019-01-31 04:18:21', '2019-01-31 04:18:21'),
(3, 3, NULL, 'created', '2019-01-31 04:20:31', '2019-01-31 04:20:31'),
(4, 5, NULL, 'created', '2019-01-31 04:35:15', '2019-01-31 04:35:15'),
(5, 6, NULL, 'created', '2019-01-31 04:41:19', '2019-01-31 04:41:19'),
(6, 7, NULL, 'created', '2019-01-31 04:42:30', '2019-01-31 04:42:30'),
(7, 8, NULL, 'created', '2019-01-31 04:51:51', '2019-01-31 04:51:51'),
(8, 9, NULL, 'created', '2019-01-31 04:59:35', '2019-01-31 04:59:35'),
(9, 10, NULL, 'created', '2019-01-31 05:01:46', '2019-01-31 05:01:46'),
(10, 11, NULL, 'created', '2019-01-31 05:03:12', '2019-01-31 05:03:12'),
(11, 12, NULL, 'created', '2019-01-31 05:04:04', '2019-01-31 05:04:04'),
(12, 13, NULL, 'created', '2019-01-31 05:23:18', '2019-01-31 05:23:18'),
(13, 14, NULL, 'created', '2019-01-31 05:23:20', '2019-01-31 05:23:20'),
(14, 15, NULL, 'created', '2019-01-31 05:25:21', '2019-01-31 05:25:21'),
(15, 16, NULL, 'created', '2019-01-31 05:26:57', '2019-01-31 05:26:57'),
(16, 17, NULL, 'created', '2019-01-31 05:33:42', '2019-01-31 05:33:42'),
(17, 18, NULL, 'created', '2019-01-31 05:38:03', '2019-01-31 05:38:03'),
(18, 18, NULL, 'accepted and will be dilivered in 20 mins', '2019-01-31 05:40:25', '2019-01-31 05:40:25'),
(19, 18, NULL, 'with the delivery', '2019-01-31 05:40:36', '2019-01-31 05:40:36'),
(20, 18, NULL, 'Delivered', '2019-01-31 05:40:43', '2019-01-31 05:40:43'),
(21, 19, NULL, 'created', '2019-01-31 05:43:00', '2019-01-31 05:43:00'),
(22, 20, NULL, 'created', '2019-01-31 05:45:36', '2019-01-31 05:45:36'),
(23, 21, NULL, 'created', '2019-01-31 05:47:00', '2019-01-31 05:47:00'),
(24, 22, NULL, 'created', '2019-01-31 05:48:11', '2019-01-31 05:48:11'),
(25, 23, NULL, 'created', '2019-01-31 05:50:10', '2019-01-31 05:50:10'),
(26, 24, NULL, 'created', '2019-01-31 05:51:19', '2019-01-31 05:51:19'),
(27, 25, NULL, 'created', '2019-01-31 05:53:26', '2019-01-31 05:53:26'),
(28, 26, NULL, 'created', '2019-01-31 05:58:18', '2019-01-31 05:58:18'),
(29, 27, NULL, 'created', '2019-01-31 06:01:47', '2019-01-31 06:01:47'),
(30, 28, NULL, 'created', '2019-01-31 06:05:37', '2019-01-31 06:05:37'),
(31, 29, NULL, 'created', '2019-01-31 06:06:28', '2019-01-31 06:06:28'),
(32, 30, NULL, 'created', '2019-01-31 21:54:15', '2019-01-31 21:54:15'),
(33, 31, NULL, 'created', '2019-01-31 22:02:10', '2019-01-31 22:02:10'),
(34, 32, NULL, 'created', '2019-01-31 22:05:04', '2019-01-31 22:05:04'),
(35, 33, NULL, 'created', '2019-01-31 22:20:19', '2019-01-31 22:20:19'),
(36, 34, NULL, 'created', '2019-01-31 22:32:15', '2019-01-31 22:32:15'),
(37, 35, NULL, 'created', '2019-01-31 22:33:16', '2019-01-31 22:33:16'),
(38, 36, NULL, 'created', '2019-01-31 22:35:15', '2019-01-31 22:35:15'),
(39, 36, NULL, 'accepted and will be dilivered in 20 mins', '2019-01-31 23:00:15', '2019-01-31 23:00:15'),
(40, 37, NULL, 'created', '2019-02-01 07:06:01', '2019-02-01 07:06:01'),
(41, 37, NULL, 'accepted and will be dilivered in 20 mins', '2019-02-01 07:06:25', '2019-02-01 07:06:25'),
(42, 38, NULL, 'created', '2019-02-03 04:01:46', '2019-02-03 04:01:46'),
(43, 39, NULL, 'created', '2019-02-03 08:42:00', '2019-02-03 08:42:00'),
(44, 40, NULL, 'created', '2019-02-05 05:29:03', '2019-02-05 05:29:03'),
(45, 41, NULL, 'created', '2019-02-05 05:44:31', '2019-02-05 05:44:31'),
(46, 41, NULL, 'accepted and will be dilivered in 40 mins', '2019-02-05 05:44:54', '2019-02-05 05:44:54'),
(47, 41, NULL, 'with the delivery', '2019-02-05 05:44:57', '2019-02-05 05:44:57'),
(48, 41, NULL, 'Delivered', '2019-02-05 05:44:58', '2019-02-05 05:44:58'),
(49, 42, NULL, 'created', '2019-02-06 06:25:02', '2019-02-06 06:25:02'),
(50, 43, NULL, 'created', '2019-02-06 06:48:50', '2019-02-06 06:48:50'),
(51, 44, NULL, 'created', '2019-02-06 07:10:09', '2019-02-06 07:10:09'),
(52, 45, NULL, 'created', '2019-02-06 07:20:56', '2019-02-06 07:20:56'),
(53, 46, NULL, 'created', '2019-02-07 07:09:10', '2019-02-07 07:09:10'),
(54, 47, NULL, 'created', '2019-02-07 07:25:13', '2019-02-07 07:25:13'),
(55, 47, NULL, 'accepted and will be dilivered in 20 mins', '2019-02-07 07:30:49', '2019-02-07 07:30:49'),
(56, 47, NULL, 'with the delivery', '2019-02-07 07:30:57', '2019-02-07 07:30:57'),
(57, 47, NULL, 'Delivered', '2019-02-07 07:31:02', '2019-02-07 07:31:02'),
(58, 1, NULL, 'accepted and will be dilivered in 40 mins', '2019-02-09 13:55:50', '2019-02-09 13:55:50'),
(59, 1, NULL, 'with the delivery', '2019-02-09 13:55:57', '2019-02-09 13:55:57'),
(60, 48, NULL, 'created', '2019-02-09 13:56:03', '2019-02-09 13:56:03'),
(61, 48, NULL, 'accepted and will be dilivered in 20 mins', '2019-02-09 13:56:17', '2019-02-09 13:56:17'),
(62, 48, NULL, 'with the delivery', '2019-02-09 13:56:19', '2019-02-09 13:56:19'),
(63, 48, NULL, 'Delivered', '2019-02-09 13:56:20', '2019-02-09 13:56:20'),
(64, 49, NULL, 'created', '2019-03-08 19:53:42', '2019-03-08 19:53:42'),
(65, 49, NULL, 'accepted and will be dilivered in 20 mins', '2019-03-08 19:54:36', '2019-03-08 19:54:36'),
(66, 1, NULL, 'Delivered', '2019-03-09 05:17:51', '2019-03-09 05:17:51'),
(67, 49, NULL, 'with the delivery', '2019-03-09 05:21:03', '2019-03-09 05:21:03'),
(68, 37, NULL, 'with the delivery', '2019-03-09 05:21:11', '2019-03-09 05:21:11'),
(69, 49, NULL, 'Delivered', '2019-03-09 05:38:42', '2019-03-09 05:38:42'),
(70, 37, NULL, 'Delivered', '2019-03-09 05:44:33', '2019-03-09 05:44:33'),
(71, 37, NULL, 'Delivered', '2019-03-09 05:44:33', '2019-03-09 05:44:33'),
(72, 37, NULL, 'Delivered', '2019-03-09 05:44:34', '2019-03-09 05:44:34'),
(73, 36, NULL, 'with the delivery', '2019-03-09 05:44:53', '2019-03-09 05:44:53'),
(74, 36, NULL, 'Delivered', '2019-03-09 05:45:51', '2019-03-09 05:45:51'),
(75, 2, NULL, 'accepted and will be dilivered in 20 mins', '2019-03-09 05:46:25', '2019-03-09 05:46:25'),
(76, 2, NULL, 'with the delivery', '2019-03-09 05:46:31', '2019-03-09 05:46:31'),
(77, 2, NULL, 'Delivered', '2019-03-09 05:47:21', '2019-03-09 05:47:21'),
(78, 38, NULL, 'Rejected', '2019-03-12 22:29:47', '2019-03-12 22:29:47'),
(79, 40, NULL, 'accepted and will be dilivered in 20 mins', '2019-03-12 22:31:41', '2019-03-12 22:31:41'),
(80, 40, NULL, 'with the delivery', '2019-03-12 22:31:43', '2019-03-12 22:31:43'),
(81, 40, NULL, 'Delivered', '2019-03-12 22:31:45', '2019-03-12 22:31:45'),
(82, 32, NULL, 'Rejected', '2019-03-12 22:32:07', '2019-03-12 22:32:07'),
(83, 50, NULL, 'created', '2019-04-14 15:45:59', '2019-04-14 15:45:59'),
(84, 51, NULL, 'created', '2019-04-14 15:50:07', '2019-04-14 15:50:07'),
(85, 51, NULL, 'accepted and will be dilivered in 40 mins', '2019-04-14 15:57:07', '2019-04-14 15:57:07'),
(86, 51, NULL, 'with the delivery', '2019-04-14 15:57:10', '2019-04-14 15:57:10'),
(87, 51, NULL, 'Delivered', '2019-04-14 15:57:11', '2019-04-14 15:57:11'),
(88, 54, NULL, 'created', '2019-05-15 13:59:40', '2019-05-15 13:59:40'),
(89, 56, NULL, 'created', '2019-05-15 15:50:13', '2019-05-15 15:50:13'),
(90, 57, NULL, 'created', '2019-05-15 16:10:39', '2019-05-15 16:10:39'),
(91, 58, NULL, 'created', '2019-05-25 03:33:51', '2019-05-25 03:33:51'),
(92, 59, NULL, 'created', '2019-05-25 03:46:14', '2019-05-25 03:46:14'),
(93, 59, NULL, 'accepted and will be dilivered in 20 mins', '2019-05-25 03:57:36', '2019-05-25 03:57:36'),
(94, 59, NULL, 'with the delivery', '2019-05-25 03:57:37', '2019-05-25 03:57:37'),
(95, 59, NULL, 'Delivered', '2019-05-25 03:57:38', '2019-05-25 03:57:38'),
(96, 60, NULL, 'created', '2019-09-11 21:13:41', '2019-09-11 21:13:41'),
(97, 61, NULL, 'created', '2019-09-11 21:18:51', '2019-09-11 21:18:51'),
(98, 62, NULL, 'created', '2019-09-11 21:28:50', '2019-09-11 21:28:50'),
(99, 63, NULL, 'created', '2019-09-11 21:36:09', '2019-09-11 21:36:09'),
(100, 64, NULL, 'created', '2019-09-11 22:34:23', '2019-09-11 22:34:23'),
(101, 65, NULL, 'created', '2019-09-11 22:42:25', '2019-09-11 22:42:25'),
(102, 66, NULL, 'created', '2019-09-11 22:45:43', '2019-09-11 22:45:43'),
(103, 67, NULL, 'created', '2019-09-11 22:49:00', '2019-09-11 22:49:00'),
(104, 68, NULL, 'created', '2019-09-11 22:52:58', '2019-09-11 22:52:58'),
(105, 69, NULL, 'created', '2019-09-11 22:55:32', '2019-09-11 22:55:32'),
(106, 70, NULL, 'created', '2019-09-11 22:57:58', '2019-09-11 22:57:58'),
(107, 71, NULL, 'created', '2019-09-11 23:52:12', '2019-09-11 23:52:12'),
(108, 72, NULL, 'created', '2019-09-11 23:54:05', '2019-09-11 23:54:05'),
(109, 73, NULL, 'created', '2019-09-12 00:19:07', '2019-09-12 00:19:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` int(11) NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `restaurant_id`, `created_at`, `updated_at`) VALUES
(42, 41468, 9, '2019-01-22 22:36:41', '2019-01-22 22:36:41'),
(43, 41460, 9, '2019-01-22 22:36:41', '2019-01-22 22:36:41'),
(46, 33098, 13, '2019-01-22 22:38:49', '2019-01-22 22:38:49'),
(47, 41460, 13, '2019-01-22 22:38:49', '2019-01-22 22:38:49'),
(124, 41460, 3, '2019-03-09 01:54:47', '2019-03-09 01:54:47'),
(125, 41462, 3, '2019-03-09 01:54:47', '2019-03-09 01:54:47'),
(132, 41460, 2, '2019-05-13 13:18:13', '2019-05-13 13:18:13'),
(133, 41462, 2, '2019-05-13 13:18:13', '2019-05-13 13:18:13'),
(134, 41468, 2, '2019-05-13 13:18:14', '2019-05-13 13:18:14');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `sum` double(8,2) NOT NULL,
  `count` int(11) NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `name`, `from`, `to`, `sum`, `count`, `restaurant_id`, `created_at`, `updated_at`) VALUES
(6, '21552131726', '2019-03-01', '2019-03-31', 33.87, 1, 2, '2019-03-09 10:42:06', '2019-03-09 10:42:06'),
(8, '21552131821', '2019-03-01', '2019-03-30', 33.87, 1, 2, '2019-03-09 10:43:41', '2019-03-09 10:43:41'),
(9, '21552430936', '2019-02-01', '2019-02-28', 733.54, 18, 2, '2019-03-12 21:48:56', '2019-03-12 21:48:56'),
(10, '21552433011', '2019-02-01', '2019-03-31', 246.49, 19, 2, '2019-03-12 22:23:31', '2019-03-12 22:23:31'),
(11, '21552433281', '2019-02-01', '2019-03-31', 246.49, 6, 2, '2019-03-12 22:28:01', '2019-03-12 22:28:01'),
(12, '21552433397', '2019-02-01', '2019-03-31', 246.49, 7, 2, '2019-03-12 22:29:57', '2019-03-12 22:29:57'),
(13, '21552433538', '2019-02-01', '2019-03-31', 295.76, 7, 2, '2019-03-12 22:32:18', '2019-03-12 22:39:17'),
(14, '21558756576', '2019-05-01', '2019-05-31', 0.00, 0, 2, '2019-05-25 03:56:16', '2019-05-25 03:56:16'),
(15, '21558756664', '2019-05-01', '2019-05-31', 56.74, 1, 2, '2019-05-25 03:57:44', '2019-05-25 03:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'imgs/placeholder.jpg',
  `addressCord` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addressName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `open` time NOT NULL,
  `close` time NOT NULL,
  `offDays` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minOrder` double NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryPrice` double NOT NULL,
  `deliveryTime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `logo`, `addressCord`, `addressName`, `open`, `close`, `offDays`, `minOrder`, `phone`, `deliveryPrice`, `deliveryTime`, `group_id`, `created_at`, `updated_at`) VALUES
(2, 'pizza lord neuss', 'images/1552124733_download (3).png', '51.2033359,6.6827174', 'Theodor-Heuss-Platz 7, 41460 Neuss, Germany', '03:00:00', '23:00:00', 'a:2:{i:0;s:8:\"Saturday\";i:1;s:6:\"Sunday\";}', 10, '342342343432', 10, '40 - 60', 34, '2018-12-04 06:21:10', '2019-05-13 13:18:13'),
(3, 'pizza freedy', 'images/1545764293_download (3).png', '51.2033359,6.6827174', 'neumarkt 1 neuss', '01:00:00', '05:30:00', 'a:1:{i:0;s:8:\"Saturday\";}', 20, '534534545345', 5, '40 - 60', 34, '2018-12-04 06:21:59', '2019-03-09 01:54:47'),
(9, 'Lord', 'pictures/rSGklZ8rpRTtpSNtjgC41Ui0ygGqsDgETbfcSWcT.jpeg', '2454534', 'street', '10:00:00', '00:00:00', 'a:2:{i:0;s:8:\"Saturday\";i:1;s:6:\"Sunday\";}', 20, '34656456', 20, NULL, 35, '2018-12-04 16:29:18', '2019-01-22 22:37:57'),
(13, 'Lapoir', 'pictures/Nd1mjwWTHYkRdLW3JDxPyeBASsd3Z5w9FdPYCSEA.jpeg', '34234234', 'dsdfsdf', '02:00:00', '02:30:00', 'a:1:{i:0;s:8:\"Saturday\";}', 2, '342545345435', 34, NULL, 35, '2018-12-16 18:01:09', '2019-01-22 22:37:57');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_user`
--

CREATE TABLE `restaurant_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `restaurant_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restaurant_user`
--

INSERT INTO `restaurant_user` (`id`, `restaurant_id`, `user_id`, `created_at`, `updated_at`) VALUES
(12, 3, 11, NULL, NULL),
(13, 3, 12, NULL, NULL),
(14, 13, 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2018-12-01 22:00:00', '2018-12-01 22:00:00'),
(2, 'owner', 'web', '2018-12-02 15:00:00', '2018-12-01 22:00:00'),
(3, 'driver', 'web', '2018-12-01 22:00:00', '2018-12-01 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `selected_options`
--

CREATE TABLE `selected_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `food_order_id` int(10) UNSIGNED NOT NULL,
  `option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `selected_options`
--

INSERT INTO `selected_options` (`id`, `food_order_id`, `option`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 'cheese', 1.00, NULL, NULL),
(2, 2, 'red', 1.00, NULL, NULL),
(3, 3, 'parmisan', 3.00, NULL, NULL),
(4, 3, 'red', 1.00, NULL, NULL),
(5, 4, 'cheese', 1.00, NULL, NULL),
(6, 5, 'red', 1.00, NULL, NULL),
(7, 6, 'parmisan', 3.00, NULL, NULL),
(8, 6, 'red', 1.00, NULL, NULL),
(9, 7, 'cheese', 1.00, NULL, NULL),
(10, 8, 'red', 1.00, NULL, NULL),
(11, 9, 'parmisan', 3.00, NULL, NULL),
(12, 9, 'red', 1.00, NULL, NULL),
(13, 10, 'cheese', 1.00, NULL, NULL),
(14, 11, 'red', 1.00, NULL, NULL),
(15, 12, 'parmisan', 3.00, NULL, NULL),
(16, 12, 'red', 1.00, NULL, NULL),
(17, 13, 'cheese', 1.00, NULL, NULL),
(18, 14, 'red', 1.00, NULL, NULL),
(19, 15, 'parmisan', 3.00, NULL, NULL),
(20, 15, 'red', 1.00, NULL, NULL),
(21, 16, 'cheese', 1.00, NULL, NULL),
(22, 17, 'red', 1.00, NULL, NULL),
(23, 18, 'parmisan', 3.00, NULL, NULL),
(24, 18, 'red', 1.00, NULL, NULL),
(25, 23, 'dd', 0.00, NULL, NULL),
(26, 26, 'dd', 0.00, NULL, NULL),
(27, 29, 'dd', 0.00, NULL, NULL),
(28, 32, 'dd', 0.00, NULL, NULL),
(29, 35, 'dd', 0.00, NULL, NULL),
(30, 38, 'dd', 0.00, NULL, NULL),
(31, 41, 'dd', 0.00, NULL, NULL),
(32, 44, 'dd', 0.00, NULL, NULL),
(33, 47, 'dd', 0.00, NULL, NULL),
(34, 50, 'dd', 0.00, NULL, NULL),
(35, 53, 'dd', 0.00, NULL, NULL),
(36, 56, 'dd', 0.00, NULL, NULL),
(37, 59, 'dd', 0.00, NULL, NULL),
(38, 62, 'dd', 0.00, NULL, NULL),
(39, 65, 'dd', 0.00, NULL, NULL),
(40, 68, 'dd', 0.00, NULL, NULL),
(41, 71, 'dd', 0.00, NULL, NULL),
(42, 74, 'dd', 0.00, NULL, NULL),
(43, 77, 'dd', 0.00, NULL, NULL),
(44, 80, 'dd', 0.00, NULL, NULL),
(45, 83, 'dd', 0.00, NULL, NULL),
(46, 84, 'dd', 0.00, NULL, NULL),
(47, 85, 'dd', 0.00, NULL, NULL),
(48, 86, 'dd', 0.00, NULL, NULL),
(49, 87, 'dd', 0.00, NULL, NULL),
(50, 88, 'dd', 0.00, NULL, NULL),
(51, 89, 'dd', 0.00, NULL, NULL),
(52, 90, 'dd', 0.00, NULL, NULL),
(53, 94, 'green', 3.00, NULL, NULL),
(54, 95, 'dd', 0.00, NULL, NULL),
(55, 96, 'parmisan', 3.00, NULL, NULL),
(56, 96, 'red', 1.00, NULL, NULL),
(57, 98, 'dd', 0.00, NULL, NULL),
(58, 100, 'dd', 0.00, NULL, NULL),
(59, 101, 'cheese', 1.00, NULL, NULL),
(60, 102, 'red', 1.00, NULL, NULL),
(61, 103, 'dd', 0.00, NULL, NULL),
(62, 104, 'dd', 0.00, NULL, NULL),
(63, 105, 'dd', 0.00, NULL, NULL),
(64, 107, 'tamata', 1.00, NULL, NULL),
(65, 108, 'green', 3.00, NULL, NULL),
(66, 111, 'dd', 0.00, NULL, NULL),
(67, 112, 'dd', 0.00, NULL, NULL),
(68, 113, 'dd', 0.00, NULL, NULL),
(69, 114, 'dd', 0.00, NULL, NULL),
(70, 115, 'dd', 0.00, NULL, NULL),
(71, 117, 'tamata', 1.00, NULL, NULL),
(72, 119, 'tamata', 1.00, NULL, NULL),
(73, 122, 'tamata', 1.00, NULL, NULL),
(74, 123, 'tamata', 1.00, NULL, NULL),
(75, 125, 'tamata', 1.00, NULL, NULL),
(76, 126, 'tamata', 1.00, NULL, NULL),
(77, 128, 'tamata', 1.00, NULL, NULL),
(78, 131, 'tamata', 1.00, NULL, NULL),
(79, 132, 'tamata', 1.00, NULL, NULL),
(80, 133, 'tamata', 1.00, NULL, NULL),
(81, 136, 'tamata', 1.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `food_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `price`, `food_id`, `created_at`, `updated_at`) VALUES
(45, 'size 1', 20.00, 2, '2019-01-24 23:22:26', '2019-01-24 23:22:26'),
(46, 'size 2', 40.00, 2, '2019-01-24 23:22:32', '2019-01-24 23:22:32'),
(49, 'size 2s0mww', 31.00, 1, '2019-03-08 20:40:34', '2019-03-08 20:41:08'),
(51, 'size 2s0m', 31.00, 2, '2019-03-08 21:00:30', '2019-03-08 21:00:30'),
(52, 'size 44', 10.00, 2, '2019-03-08 21:08:39', '2019-03-08 21:08:39'),
(53, 'size 45', 10.00, 2, '2019-03-08 21:09:14', '2019-03-08 21:09:38'),
(54, 'size 45', 10.00, 2, '2019-03-08 21:09:58', '2019-03-08 21:09:58'),
(55, 'size 2s0m', 31.00, 2, '2019-03-08 22:19:57', '2019-03-08 22:19:57'),
(60, 'size 2s0', 34.00, 7, '2019-03-13 03:52:34', '2019-03-13 03:52:34'),
(61, 'size 20cm', 2.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(62, 'size 2s0mww', 31.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(63, 'size 2s0m', 31.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(64, 'size 2s0', 34.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(65, 'size 20cm', 2.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(66, 'size 2s0mww', 31.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35'),
(67, 'size 2s0m', 31.00, 7, '2019-03-13 03:52:35', '2019-03-13 03:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurant_id` int(11) UNSIGNED DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `restaurant_id`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(8, 'Admin', 'admin@test.com', NULL, NULL, '$2y$10$.blVIEaNfkZpkIH6qwr0h.4ZR9GXKwyDvFwPYp3KC4M5ug4NgItVS', 'GUGBy6mG5e7bia7ZhQNzfij5TaleTSNoo8s0LXyLHzuhYYwPWos4KSeMDsfw', '2018-12-03 07:48:38', '2019-05-07 12:45:29'),
(11, 'njn', 'wj@ls.e', 3, NULL, '$2y$10$XwDMoBjiBQy05tibV.xGrOlcVUr3tp9FLmJFVSppZ6oqVtZoqebRy', NULL, '2018-12-10 14:04:50', '2019-02-03 00:00:00'),
(12, 'Owner', 'owner@test.com', 2, NULL, '$2y$10$5Jn7EjGUA4JYZNFB0g.HH.W9kypESVjVuGLolSHcJ3cOgr71T.yCm', '6J6k34PlddnhbwQD6OKbcygMiOsw7OJMLb1YjwEekmAIIyj3vg8t68ibBTgR', '2018-12-20 00:06:43', '2019-04-26 18:40:14'),
(14, 'fr', 'de@de.de', NULL, NULL, '$2y$10$5.OjV44lxv/dnuWi27sqyeTrCUW1p9YygyO3vA7jC9UbIq26BJYda', NULL, '2019-01-21 14:41:01', '2019-01-21 14:41:01'),
(15, 'de', 'dw@ws.ed', NULL, NULL, '$2y$10$W/9gvHEJG0BExYN/f8jpvu2d/012RDX2Hqf3cpgoqJvPehix78906', NULL, '2019-01-21 14:44:50', '2019-01-21 14:44:50'),
(16, 'ffs', 'djki@k.e', NULL, NULL, '$2y$10$a12hD/gCIwZ26.jsLt0X2e2xiuQYi292q89JzHYs.W/WNOMi73YdC', NULL, '2019-01-21 14:59:06', '2019-01-21 14:59:06'),
(17, 'fr', 'fr@cfr.n', NULL, NULL, '$2y$10$Ht.UOZtRy4p9maNH4wtYLuzHJ957IyGv7T5smpI1DkFFmlQ0CmQQa', NULL, '2019-01-21 16:22:27', '2019-01-21 16:22:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupons_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_user_id_foreign` (`user_id`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foods_category_id_foreign` (`category_id`),
  ADD KEY `foods_restaurant_id_foreign` (`group_id`);

--
-- Indexes for table `food_options`
--
ALTER TABLE `food_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `food_options_food_id_foreign` (`food_id`);

--
-- Indexes for table `food_order`
--
ALTER TABLE `food_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `food_order_order_id_foreign` (`order_id`),
  ADD KEY `food_order_food_id_foreign` (`food_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `option_items`
--
ALTER TABLE `option_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_lists_food_option_id_foreign` (`food_option_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_restaurant_id_foreign` (`restaurant_id`),
  ADD KEY `orders_customer_id_foreign` (`customer_id`),
  ADD KEY `orders_coupon_id_foreign` (`coupon_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_status_order_id_foreign` (`order_id`),
  ADD KEY `order_status_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant_user`
--
ALTER TABLE `restaurant_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restaurant_user_user_id_foreign` (`user_id`),
  ADD KEY `restaurant_user_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `selected_options`
--
ALTER TABLE `selected_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `selected_options_food_order_id_foreign` (`food_order_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sizes_food_id_foreign` (`food_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=390;

--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `food_options`
--
ALTER TABLE `food_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `food_order`
--
ALTER TABLE `food_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `option_items`
--
ALTER TABLE `option_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `restaurant_user`
--
ALTER TABLE `restaurant_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `selected_options`
--
ALTER TABLE `selected_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `foods`
--
ALTER TABLE `foods`
  ADD CONSTRAINT `foods_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `food_options`
--
ALTER TABLE `food_options`
  ADD CONSTRAINT `food_options_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `food_order`
--
ALTER TABLE `food_order`
  ADD CONSTRAINT `food_order_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `food_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `option_items`
--
ALTER TABLE `option_items`
  ADD CONSTRAINT `option_lists_food_option_id_foreign` FOREIGN KEY (`food_option_id`) REFERENCES `food_options` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_status`
--
ALTER TABLE `order_status`
  ADD CONSTRAINT `order_status_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_status_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places`
--
ALTER TABLE `places`
  ADD CONSTRAINT `places_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `restaurant_user`
--
ALTER TABLE `restaurant_user`
  ADD CONSTRAINT `restaurant_user_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `restaurant_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `selected_options`
--
ALTER TABLE `selected_options`
  ADD CONSTRAINT `selected_options_food_order_id_foreign` FOREIGN KEY (`food_order_id`) REFERENCES `food_order` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sizes`
--
ALTER TABLE `sizes`
  ADD CONSTRAINT `sizes_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
