<?php

namespace App\Http\Controllers;

use App\Allergy;
use Illuminate\Http\Request;
use App\Food;
use App\Http\Resources\FoodsCollectoin;
use App\Http\Requests\FoodRequest;
use App\Restaurant;
use App\Http\Requests\FoodOptionRequest;
use App\FoodOption;
use App\OptionItem;
use App\Category;
use App\Group;
use App\Size;
use App\Http\Requests\OptionItemRequest;
use App\Http\Requests\FoodSizeRequest;
use App\Tag;
use Illuminate\Support\Facades\Input;
use Image;

class FoodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role:admin|owner')->onpely('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('admin'))
            $foods = Food::with('options.items', 'category')->paginate(10);
        elseif (auth()->user()->hasRole('owner'))
            $foods = Food::with('options.items', 'category')->whereIn('restaurant_id', auth()->user()->restaurant->id)->paginate(10);
        return view('foods.index')->with('foods', $foods);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasRole('admin'))
        return view('errors.403');
        $categories = Category::all();
        return view('foods.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FoodRequest $request)
    {
        if (!auth()->user()->hasRole('admin'))
        return view('errors.403');
        $requestData = $request->all();
        $path = null;
        $file_name = null;
        if ($request->hasFile('picture')) {
            $path = public_path('/images');
            $time = time();
            $file_name = 'images/' . $time . "_" . Input::file('picture')->getClientOriginalName();
            Input::file('picture')->move($path, $file_name);
            $thumb_name = 'thumb-images\\' . $time . "_" . Input::file('picture')->getClientOriginalName();            
            $img = Image::make(public_path('/'.$file_name))->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            });
            $img->save(public_path($thumb_name));
            // $path = $request->file('picture')->store('pictures', 'public');
        }
        info($requestData);
        $requestData['order']=Food::max('order');
        $requestData['picture'] = $file_name;
        $food = Food::create($requestData);
        return redirect()->route('food.edit',$food->id);
    }
    public function find($key)
    {
        $foods = Food::with('options.ittems', 'category')->where('name', 'like', '%' . $key . '%')
            ->orWhereHas('category', function ($q) use ($key) {
                $q->where('name', 'like', '%' . $key . '%');
            })
            ->orderBy('name', 'desc')
            ->paginate(10);
        return FoodsCollectoin($foods);
    }
    public function getRestauantFoods($id)
    {
        $foods = Restaurant::find($id)->foods;
        $foods->load('options.items', 'restaurant', 'category');
        return response()->json($foods);
    }

    public function restaurantFoods(Restaurant $rest)
    {
        if (!auth()->user()->hasRole('admin') && !auth()->user()->restaurant->id == $rest->id)
            return response()->json(['succes' => false]);
        $foods = Food::with('options.items', 'category')->where('restaurant_id', $rest->restaurant->id)->paginate(10);
        $html = view('foods.table')->with('foods', $foods)->render();
        return response()->json($html);
    }
    public function restFoods(Restaurant $rest)
    {
        if (!auth()->user()->hasRole('admin'))
            return response()->json(['succes' => false]);
        $foods = Food::with('options.items', 'category')->where('restaurant_id', $rest->id)->paginate(10);
        $html = view('foods.table')->with('foods', $foods)->render();
        return response()->json($html);
    }
    public function createFoodSize(FoodSizeRequest $request, Food $food)
    {
        if (!auth()->user()->hasRole('admin'))
            return response('Not Allowed', 403);
        $size = $food->sizes()->create([
            'name' => $request['name'],
            'price' => $request['price'],
        ]);
        return response()->json(['success' => true, 'size' => $size]);
    }
    public function deleteSize(Size $size)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        $res = $size->delete();
        if ($res)
            return response()->json(['success' => true]);
    }

    public function updateSize(Request $request, Size $size)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        $res = $size->update($request->all());
        $size->refresh();
        if ($res)
            return response()->json(['success' => true, 'size' => $size]);
    }
    public function createFoodOption(FoodOptionRequest $request, Food $food)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        $food->options()->create($request->all());
        $option = FoodOption::orderBy('id', 'DESC')->first();
        $html = view('foods.optionCard')->with('option', $option)->render();
        return response()->json($html);
    }
    public function editFoodOption(FoodOptionRequest $request, FoodOption $option)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        info($request->all());
        $option->update($request->all());
        info($option);
        $html = view('foods.optionCard')->with('option', $option)->render();
        return response()->json($html);
    }
    public function deleteFoodOption(FoodOption $option)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        $option->delete();
        return response('ok', 200);
    }

    public function createOptionList(OptionItemRequest $request, FoodOption $option)
    {
        
        $option->items()->create($request->all());
        $item = OptionItem::orderBy('id', 'DESC')->first();
        return $item->toArray();
    }
    public function deleteOptionList(OptionItemRequest $request, OptionItem $item)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        $item->delete();
        return response('ok', 200);
    }
    public function editOptionList(OptionItemRequest $request, OptionItem $item)
    {
        if (!auth()->user()->hasRole('admin'))
        return response('Not Allowed', 403);
        $item->update($request->all());
        return $item->toArray();
    }
    /**
     * Copy size to food
     * @param int $id of the target food
     * @param int $id of the size to copy
     */
    public function copySize(Request $request, Size $size)
    {
        if (!auth()->user()->hasRole('admin'))
            return response('Not Allowed', 403);

        $food = Food::find($request['food_id']);
        $newSize = $size->replicate();
        $newSize->food_id = $food->id;
        $newSize->save();
        foreach ($size->options as $option) {
            $newOption = $option->replicate();
            $newOption->food_id = $food->id;
            $newOption->size_id = $newSize->id;
            $newOption->save();
            foreach ($option->items as $item) {
                $newItem = $item->replicate();
                $newItem->food_option_id = $newOption->id;
                $newItem->save();
            }
        }
        return back();
    }

    public function FoodCopy(Food $food)
    {
        if (!auth()->user()->hasRole('admin') && !auth()->user()->restaurant->id == $food->restaurant_id)
        return view('errors.403');
        $newFood = $food->replicate();
        $newFood->name = $newFood->name . ' - copy';
        $newFood->save();
        foreach ($food->sizes as $size) {
            $newSize = $size->replicate();
            $newSize->food_id = $newFood->id;
            $newSize->save();
            foreach ($size->options as $option) {
                $newOption = $option->replicate();
                $newOption->food_id = $newFood->id;
                $newOption->size_id = $newSize->id;
                $newOption->save();
                foreach ($option->items as $item) {
                    $newItem = $item->replicate();
                    $newItem->food_option_id = $newOption->id;
                    $newItem->save();
                }
            }
        }
        return back();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Food::findOrfail($id)->toArray();
    }
    public function changeOrder(Request $request, Restaurant $rest)
    {
        info($rest);
        if (!auth()->user()->hasRole('admin') && !auth()->user()->restaurant->id == $rest->id)
            return response()->json(['succes' => false]);
        foreach ($request['ids'] as $key => $id) {
            info($id);
            Food::find($id)->update(['order' => $key + 1]);
        }
        info($request->all());
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $food = Food::findOrfail($id);
        // if (!auth()->user()->restaurants->contains($food->restaurant))
        //     abort(403);
        if (!auth()->user()->hasRole('admin') && !auth()->user()->restaurant->id == $food->restaurant_id)
            return view('errors.403');
        $allergies = Allergy::all();
        $tags = Tag::all();
        $food->load('options', 'sizes','allergies','tags');
        // dd($food);

        $categories = Category::all();
        return view('foods.edit')->with(['food' => $food, 'categories' => $categories,
        'tags'=>$tags,'allergies'=>$allergies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FoodRequest $request, $id)
    {
        // dd($request->all());
        $food = Food::findOrfail($id);
        if (!auth()->user()->hasRole('admin') && !auth()->user()->restaurant->id == $food->restaurant_id)
        return view('errors.403');
        $requestData = $request->all();
        $path = null;
        if ($request->hasFile('picture')) {
            $path = public_path('/images');
            $time = time();
            $file_name = 'images/' . $time . "_" . Input::file('picture')->getClientOriginalName();
           

            Input::file('picture')->move($path, $file_name);
            $thumb_name = 'thumb-images\\' . $time . "_" . Input::file('picture')->getClientOriginalName();            
            $img = Image::make(public_path('/'.$file_name))->resize(300,null,function($constraint){
                $constraint->aspectRatio();
            });
            $img->save(public_path($thumb_name));
            // $path = $request->file('picture')->store('pictures', 'public');
        }
        if (!$path)
            $file_name = $food->picture;
        info($requestData);
        if(array_key_exists('allergies',$requestData))
        $food->allergies()->sync($requestData['allergies']);
        else
        $food->allergies()->sync([]);

        unset($requestData['allergies']);
        info($requestData);
        $requestData['picture'] = $file_name;
        $requestData['available'] = $request['available'] ? 1 : 0;
        $food->update($requestData);
        return redirect()->route('food.edit', $food->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Food::findOrfail($id)->delete();
    }
}
