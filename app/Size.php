<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable=['name','price','food_id'];

    public function food()
    {
        return $this->belongsTo(Food::class);
    }
    public function options()
    {
        return $this->hasMany(FoodOption::class);
    }
}
