<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['user_id', 'order_id', 'delivery', 'quality', 'average', 'comment', 'anonymous', 'published'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
