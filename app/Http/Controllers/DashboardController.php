<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Carbon\Carbon;
use App\Restaurant;
use DB;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role:admin|owner')->only(['index','orderDetails']);
        // $this->middleware('role:driver')->only(['index']);
    }

    public function index()
    {

        if (auth()->user()->hasRole('admin')) {
            $newOorders = Order::where('status', 0)->latest()->get();
            $newOorders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');
            $acceptedOrders = Order::where('status', 1)->get();
            $acceptedOrders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');
            $withDelivery = Order::where('status', 2)->get();
            $withDelivery->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');
            $toDayCount = Order::whereDate('created_at', Carbon::today())->count();
            $revenue = Order::where('status', 3)->sum('total');
        } else {
            if(auth()->user()->restaurant_id){
            $rest_id=auth()->user()->restaurant->id;
            $newOorders = Order::where('restaurant_id', $rest_id)->where('status', 0)->get();
            $newOorders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');
            $acceptedOrders = Order::where('restaurant_id', $rest_id)->where('status', 1)->get();
            $acceptedOrders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');
            $withDelivery = Order::where('restaurant_id', $rest_id)->where('status', 2)->get();
            $withDelivery->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');
            $toDayCount = Order::where('restaurant_id', $rest_id)->whereDate('created_at', Carbon::today())->count();
            $revenue = Order::where('restaurant_id', $rest_id)->where('status', 3)->sum('total');
            }else{
                return view('errors.403');
            }

        }
        return view('dashboard.index')->with([
            'newOorders' => $newOorders, 'todayCount' => $toDayCount,
            'acceptedOrders' => $acceptedOrders, 'withDelivery' => $withDelivery, 'revenue' => $revenue
        ]);
    }
    public function dashboard(Request $request)
    {
        $period = ($request['period'])?$request['period']:7;
        if(auth()->user()->hasRole('admin')){
            $restaurant = ($request['restaurant_id'])?$request['restaurant_id']:Restaurant::orderBy('updated_at','DESC')->first()->id;
        }else{
            $restaurant = auth()->user()->restaurant->id;
        }
        $orders = Order::where('restaurant_id',$restaurant)->where('status',3)
        ->whereDate('created_at','<=',Carbon::today())
        ->whereDate('created_at','>=',Carbon::today()->subDays($period))->count();
        $totalorders = Order::where('restaurant_id',$restaurant)
        ->whereDate('created_at','<=',Carbon::today())
        ->whereDate('created_at','>=',Carbon::today()->subDays($period))->count();
        $total = Order::where('restaurant_id',$restaurant)->where('status',3)
        ->whereDate('created_at','<=',Carbon::today())
        ->whereDate('created_at','>=',Carbon::today()->subDays($period))->sum('total');
        $ordersPerDay = Order::select(DB::raw("count(*) as count,DATE_FORMAT(created_at, '%Y-%b-%d') as date"))->where('restaurant_id',$restaurant)->where('status',3)
        ->whereDate('created_at','<=',Carbon::today())
        ->whereDate('created_at','>=',Carbon::today()->subDays($period))->groupBy('date')
        ->get();
        $ordersTotalPerDay = Order::select(DB::raw("sum(total) as totalP,DATE_FORMAT(created_at, '%Y-%b-%d') as date"))->where('restaurant_id',$restaurant)->where('status',3)
        ->whereDate('created_at','<=',Carbon::today())
        ->whereDate('created_at','>=',Carbon::today()->subDays($period))->orderBy('date','ASC')->groupBy('date')
        ->get();
        info($ordersTotalPerDay);


        return view('dashboard.dashboard')->with(['period'=>$period,'ordersTotalPerDay'=>$ordersTotalPerDay,'ordersPerDay'=>$ordersPerDay,'orders'=>$orders,'totalorders'=>$totalorders,'total'=>$total]);
    }
    public function test()
    {
        return response()->json(['suc' => true]);
    }

    public function history()
    {
        if (auth()->user()->hasRole('admin')) {
            $orders = Order::where('status', 0)->get();
            $orders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');

        } else {
            $orders = Order::whereIn('restaurant_id', auth()->user()->restaurants->pluck('id'))->where('status', 0)->get();
            $orders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');

        }
        return view('history.history')->with(['orders' => $orders]);
    }

    public function historyfilter(Request $request)
    {
        $start = date("Y-m-d H:i:s", strtotime(str_replace('.', '-', $request['start'])));
        $end = date("Y-m-d H:i:s", strtotime(str_replace('.', '-', $request['end'])));
        info($end);
        if (auth()->user()->hasRole('admin')) {
            $order = Order::query();
            if($request['restaurant_id']!=""){
                $order->where('restaurant_id',$request['restaurant_id']);
            }
            $orders = $order->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end)->get();
            $orders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');

        } else {
            $orders = Order::whereIn('restaurant_id', auth()->user()->restaurants->pluck('id'))->whereDate('created_at', '>=', $start)->where('created_at', '<=', $end)->get();
            $orders->load('customer', 'restaurant', 'foods', 'selectedOptions', 'statuses');

        }
        if($request['restaurant_id']!=""){
            $sum = $orders->sum('total');
        }else{
            $sum = null;
        }
        $html = view('history.ordersrow')->with('orders', $orders)->render();
        return response()->json(['html'=>$html,'sum'=>$sum]);



    }

    public function orderDetails(Request $request, Order $order)
    {
        if (!auth()->user()->hasRole('admin')&&!auth()->user()->restaurant->id==$order->restaurant_id )
            return abort(404);
        info($request['history']);
        $controls = 1;
        info($controls);
        if ($request['history'] == true) {
            $controls = 0;
            info('b4' . $controls);
        } else {
            $controls = 1;
        }
        info('after' . $controls);
        info('afterssssss' . $controls);
        $order->load('coupon');
        $html = view('dashboard.orderDetails')->with(['order' => $order, 'controls' => $controls])->render();
        return response()->json($html);
    }

    
}
