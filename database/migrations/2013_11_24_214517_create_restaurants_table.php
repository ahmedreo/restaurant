<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('logo')->default('imgs/placeholder.jpg');
            $table->string('addressCord');
            $table->string('addressName');
            $table->time('open')->nullable();
            $table->time('close')->nullable();
            $table->string('offDays')->nullable();
            $table->string('phone');
            $table->double('minOrder');
            $table->double('deliveryPrice')->nullable();
            $table->string('deliveryTime')->nullable();
            $table->unsignedInteger('group_id');
            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restuarants');
    }
}
