<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodOption extends Model
{
    protected $fillable = ['food_id','name','min', 'max','req','size_id'];

    public function food()
    {
        return $this->belongsTo(Food::class);
    }

    public function items()
    {
        return $this->hasMany(OptionItem::class, 'food_option_id');
    }
    public function size()
    {
        return $this->belongsTo(Size::class);
    }
}
