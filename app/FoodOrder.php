<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FoodOrder extends Pivot
{
    public function selectedOptions()
    {
        return $this->hasMany(SelectedFoodOption::class, 'food_order_id');
    }
}
