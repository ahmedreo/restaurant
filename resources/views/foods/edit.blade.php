@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">

<div id="page_content_inner" style="padding-bottom: 6px">
    <h1>Edit {{$food->name}}</h1>
    <form action="{{url('food/'.$food->id)}}" method="POST" role="form" class="uk-form-stacked"
        enctype="multipart/form-data">
        <div class="md-card">
            <div class="md-card-content large-padding">
                @csrf @method('put')
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-2-3">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-5">
                                <label for="available" class="inline-label">Available</label>
                                <input type="checkbox" name="available" data-switchery
                                    value="{{($food->available)?0:1}}" @if($food->available) checked @endif
                                id="available" />
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="name">Name<span class="req">*</span></label>
                                    <input type="text" value="{{$food->name}}" name="name" data-parsley-trigger="change"
                                        required class="md-input" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="enough">enough<span class="req">*</span></label>
                                    <input type="text" value="{{$food->enough}}" name="enough" required
                                        class="md-input" />
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="details">details<span class="req">*</span></label>
                                    <input type="text" value="{{$food->details}}" name="details" class="md-input" />
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="info">info<span class="req">*</span></label>
                                    <input class="md-input" type="text" name="info" value="{{$food->info}}" id="close"
                                        required>
                                </div>
                            </div>

                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="allergies">Allergies<span class="req">*</span></label>
                                    <select id="allergies" name="allergies[]" style="width:100%" multiple data-md-select2>
                                        <option value="">Select Allergy</option>
                                        @forelse ($allergies as $allergy)
                                        <option value="{{$allergy->id}}" >{{$allergy->code}}-{{$allergy->description}}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="discount">discount<span class="req">*</span></label>
                                    <input type="number" value="{{$food->discount}}" name="discount" required
                                        class="md-input" />
                                </div>
                            </div>

                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="restaurant_id">Restaurant<span class="req">*</span></label>
                                    <select id="restaurant_id" name="restaurant_id" class="uk-width-1-1" required
                                        data-md-select2>
                                        <option value="">Select Restaurant</option>
                                        @forelse (\App\Restaurant::all() as $rest)
                                        <option value="{{$rest->id}}">{{$rest->name}}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="parsley-row">
                                    <label for="category_id">category<span class="req">*</span></label>
                                    <select id="category" name="category_id" class="uk-width-1-1" data-md-select2>
                                        <option value="">Select category</option>
                                        @forelse ($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_a uk-margin-small-bottom">
                                    Picture
                                </h3>
                                <input type="file" name="picture" data-default-file="{{asset($food->picture)}}"
                                    id="input-file-a" class="dropify" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <button type="submit" class="md-btn md-btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="page_content_inner" class="uk-grid" style="padding-bottom: 5px">
    <div class="uk-width-1-2">
        <div class="md-card md-card-collapsed">
            <div class="md-card-toolbar">
                <div class="md-card-toolbar-actions">
                    <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
                </div>
                <h3 class="md-card-toolbar-heading-text">
                    Add Size
                </h3>
            </div>
            <div class="md-card-content">
                <form id="add-size-form" action="{{url('addsize/'.$food->id)}}" method="post">
                    @csrf
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-5">
                            <div class="parsley-row">
                                <label for="name">name<span class="req">*</span></label>
                                <input type="text" name="name" data-parsley-trigger="change" required class="md-input">
                            </div>
                        </div>
                        <div class="uk-width-medium-1-5">
                            <div class="parsley-row">
                                <label for="min">Price<span class="req">*</span></label>
                                <input type="number" step=".01" name="price" required class="md-input" />
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid">
                        <div class="uk-width-1-2">
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="uk-width-1-2">
        <div class="md-card md-card-collapsed">
            <div class="md-card-toolbar">
                <div class="md-card-toolbar-actions">
                    <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
                </div>
                <h3 class="md-card-toolbar-heading-text">
                    Add Option
                </h3>
            </div>
            <div class="md-card-content">
                <form id="add-option-form" action="{{url('addoption/'.$food->id)}}" method="post">
                    @csrf
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-5">
                            <div class="parsley-row">
                                <label for="name">name<span class="req">*</span></label>
                                <input type="text" name="name" data-parsley-trigger="change" required class="md-input">
                            </div>
                        </div>
                        <div class="uk-width-medium-1-5">
                            <div class="parsley-row">
                                <label for="min">min<span class="req">*</span></label>
                                <input type="number" name="min" required class="md-input" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-5">
                            <div class="parsley-row">
                                <label for="max">max<span class="req">*</span></label>
                                <input type="number" name="max" required class="md-input" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-5">
                            <div class="parsley-row">
                                <label for="size_id">Size<span class="req">*</span></label>
                                <select id="size_id" name="size_id" class="uk-width-1-1" required data-md-select2>
                                    <option value="">Select Size</option>
                                    @forelse ($food->sizes as $size)
                                    <option value="{{$size->id}}">{{$size->name}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-5">
                            <input type="checkbox" data-switchery value="1" id="required" name="req" />
                            <label for="required" class="inline-label">Required</label>
                        </div>

                    </div>
                    <div class="uk-grid">
                        <div class="uk-width-1-2">
                            <button type="submit" class="md-btn md-btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="page_content_inner" style="padding-bottom: 5px">

    <div class="uk-grid uk-margin-medium-bottom" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
            <div class="md-card">
                <div class="md-card-content">
                    <ul class="uk-tab uk-tab-grid" id="size-head-tabs" data-uk-tab="{connect:'#size-tabs'}">
                        @forelse ($food->sizes as $size)
                        <li class="" id="size-head-tab-{{$size->id}}"><a href="#">{{$size->name}}</a></li>
                        @empty @endforelse
                    </ul>
                    <ul class="uk-switcher uk-margin" id="size-tabs">
                        @forelse ($food->sizes as $size)
                        <li id="size-tab-{{$size->id}}">
                            <div class="sizeControl" id="size-control-{{$size->id}}" style="margin:1.5%">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td id="size-name-{{$size->id}}">{{$size->name}}</td>
                                            <td id="size-price-{{$size->id}}">{{$size->price}}</td>
                                            <td>
                                                <a href="{{url('editsize/'.$size->id)}}" class="editSize"
                                                    data-id="{{$size->id}}" title="Edit"><i
                                                        class="md-icon material-icons">&#xE254;</i></a>
                                            </td>
                                            <td>
                                                <a href="{{url('deletesize/'.$size->id)}}" data-id="{{$size->id}}"
                                                    class="deleteSize" title="Delete"><i
                                                        class="md-icon material-icons md-color-red-500">delete</i></a>
                                            </td>
                                            </td>
                                            <td>
                                                <a href="{{url('copy-size/'.$size->id)}}" class="copySize"
                                                    data-id="{{$size->id}}" title="Copy"><i
                                                        class="md-icon material-icons"
                                                        style="color:green">file_copy</i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                            <div id="size-copy-{{$size->id}}" style="display:none; margin:1%">
                                <form action="{{url('copy-size/'.$size->id)}}" class="copy-size-form"
                                    data-id="{{$size->id}}" method="post">
                                    @csrf
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-5">
                                            <div class="parsley-row">
                                                <label for="food_id">Choose Food<span class="req">*</span></label>
                                                <select id="food_id" name="food_id" class="uk-width-1-1"
                                                    style="width:100%" required data-md-select2>
                                                    <option value="">Select Size</option>
                                                    @forelse (\App\Food::all() as $food)
                                                    <option value="{{$food->id}}">{{$food->name}} -
                                                        {{$food->restaurant->name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="">
                                                <button type="submit" class="md-btn md-btn-primary">Copy</button>
                                                <button type="button" class="md-btn md-btn-warning cancelSizeCopy"
                                                    data-id="{{$size->id}}">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="size-edit-{{$size->id}}" style="display:none; margin:1%">
                                <form action="{{url('editsize/'.$size->id)}}" class="edit-size-form"
                                    data-id="{{$size->id}}" method="post">
                                    @csrf
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-5">
                                            <div class="parsley-row">
                                                <label for="name">name<span class="req">*</span></label>
                                                <input type="text" name="name" value="{{$size->name}}"
                                                    data-parsley-trigger="change" required class="md-input">
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-5">
                                            <div class="parsley-row">
                                                <label for="min">Price<span class="req">*</span></label>
                                                <input type="number" step=".01" value="{{$size->price}}" name="price"
                                                    required class="md-input" />
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="">
                                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                                                <button type="button" class="md-btn md-btn-warning cancelSizeControl"
                                                    data-id="{{$size->id}}">Cancel</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="sizeOptions" id="size-options-{{$size->id}}">
                                <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium hierarchical_show"
                                    data-uk-grid-margin id="hierarchical-show" data-show-delay="280" style="margin:1px">
                                    @forelse ($size->options as $option)
                                    @include('foods.optionCard') @empty @endforelse
                                </div>
                            </div>
                        </li>
                        @empty @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<button class="md-btn" style="display:none;text-align:center;" id="danger" data-message="" data-status="danger"
    data-pos="bottom-center">Danger</button>
<button class="md-btn" style="display:none;text-align:center;" id="success" data-message="" data-status="success"
    data-pos="bottom-center">Success</button>

<div class="uk-modal" id="edit_option">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Headline <i class="material-icons" data-uk-tooltip="{pos:'top'}"
                    title="headline tooltip">&#xE8FD;</i></h3>
        </div>

        <form id="edit_option_form">
            @csrf
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-4">
                    <div class="parsley-row">
                        <label for="name">name<span class="req">*</span></label>
                        <input type="text" id="option-name" name="name" data-parsley-trigger="change" required
                            class="md-input">
                    </div>
                </div>
                <input type="hidden" id="option-id">
                <div class="uk-width-medium-1-4">
                    <div class="parsley-row">
                        <label for="min">min<span class="req">*</span></label>
                        <input type="number" id="option-min" name="min" required class="md-input" />
                    </div>
                </div>
                <div class="uk-width-medium-1-4">
                    <div class="parsley-row">
                        <label for="max">max<span class="req">*</span></label>
                        <input type="number" id="option-max" name="max" required class="md-input" />
                    </div>
                </div>
                <div class="uk-width-medium-1-4">
                    <input type="checkbox" id="option-req" data-switchery value="1" id="required" name="req" />
                    <label for="option-req" class="inline-label">Required</label>
                </div>
            </div>

            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Update</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('pagejs')
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>

<script>
    document.addEventListener("DOMContentLoaded",function(){
allergies =[];
@forelse($food->allergies as $allergy)
allergies.push("{{$allergy->id}}");
@empty
    
@endforelse
$("#allergies").select2()
$('#allergies').val(allergies).trigger('change');
        // open edit option modal
        $(document).on('click','.edit-option',function(){
            $('#edit_option_form')[0].reset()
            data = $(this).data('option');
            console.log(data);
            $('#option-name').val(data['name'])
            $('#option-name').click();
            $('#option-min').val(data['min'])
            $('#option-max').val(data['max'])
            $('#option-id').val(data['id'])
            if(data['req']==1){
                if($('#option-req').val() != 1){
                console.log('true')
            $('#option-req').trigger('click')
            $('#option-req').val(1)
              }
            }else{
                if($('#option-req').val() == 1){
                console.log($('#option-req').val())
            $('#option-req').trigger('click')
            $('#option-req').val(0)
              }
            }
        });

        // delete option
        $(document).on('click','.delete-option',function(){
            id = $(this).attr('id');
            $.ajax({
            url:"{{url('delete/option')}}"+'/'+id,
            type:"get",
            success:function(res){
                console.log(res)
                $("#option-card-"+id).hide(100);
                $("#option-card-"+id).remove();
                $('#danger').data('message',"@lang('messages.OptionDeleted')").click();
            }
        });
    });

        // edit option 
        $('#edit_option_form').on('submit',function(e){
            e.preventDefault()
            url= "{{url('edit/option/')}}"+ '/'+$('#option-id').val();
            method ="get";
            data = $(this).serializeArray();
            console.log(url,data)

            $.ajax({
            url:url,
            type:method,
            data:data,
            success:function(res){
                console.log(res)
                $("#option-card-"+$('#option-id').val()).html(res);
                $('#success').data('message',"@lang('messages.OptionEdited')").click();

            }

        });
        $('#edit_option_form')[0].reset()
        });
        // add item tp option 
        $(document).on('submit','.add-item',function(e){
            e.preventDefault();
            id= $(this).data('id')
            action = $(this).attr('action');
            data = $(this).serializeArray();
            method="GET";
            console.log(action,method,data);
    $.ajax({
            url:action,
            type:method,
            data:data,
            success:function(res){
                console.log(res)
                li = `<li id="item-`+res.id+`"><i class="material-icons delete-item uk-text-danger"style="cursor:pointer" id="`+res.id+
                    `">clear</i>&nbsp;`+res.name+`&nbsp;<span class="uk-badge uk-badge-notification uk-badge-primary">`+res.price+`</span></li>`
               
               td= ` <tr id="item-${res.id}">
                    <td>${res.name}</td>
                    <td>${res.price}</td>
                    <td style="cursor:pointer; text-align: center" id="${res.id}"><i class="material-icons delete-item uk-text-danger" id="${res.id}">&#xE872;</i></td>
                     </tr>`
                $('#list-'+id).prepend(td);
                $('#success').data('message',"@lang('messages.ItemAdded')").click();

            }
        });
        $(this)[0].reset()
        });
        $(document).on('click','.delete-item',function(e){
            e.preventDefault();
            id = $(this).attr('id');
            pid = $(this).parent().parent();
            $.ajax({
                url:"{{url('deleteitem/')}}"+'/'+id,
                type:"GET",
                success:function(res){
                    pid.hide(100);
                    pid.remove();
                $('#danger').data('message',"@lang('messages.ItemDeleted')").click();

                }
            })
        })

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
});
    $('#category').select2();
        $('#restaurant_id').select2();
        $('#category').val("{{$food->category->id}}").trigger('change');
        $('#restaurant_id').val("{{($food->restaurant)?$food->restaurant->id:''}}").trigger('change');

        // add option
$('#add-option-form').on('submit',function(e){
    e.preventDefault();
    action=$(this).attr('action');
    method=$(this).attr('method');
    data = $(this).serializeArray();
    size_id=$('#size_id').val();
    console.log(action,method,data,size_id);
    console.log(data);
    $.ajax({
            url:action,
            type:method,
            data:data,
            success:function(res){
                $('#size-options-'+size_id).children(':first').prepend(res);
                
                $('#success').data('message',"@lang('messages.ItemAdded')").click();
                $('#size-options-'+size_id).children().children().css('visibility','visible')
            }
        });
        $('#add-option-form')[0].reset()
})
$('#add-size-form').on('submit',function(e){
    e.preventDefault();
    action=$(this).attr('action');
    method=$(this).attr('method');
    data = $(this).serializeArray();
    console.log(action,method,data);
    $.ajax({
            url:action,
            type:method,
            data:data,
            success:function(res){
                console.log(res)
                $('#size_id').append('<option id="option-size-'+res.size.id+'" value="'+res.size.id+'" selected="selected">'+res.size.name+'</option>');
                $('#size-head-tabs').append('<li class="" id="size-head-tab-'+res.size.id+'"><a href="#">'+res.size.name+'</a></li>')
                $('#size-tabs').append(`
                <li id="size-tab-${res.size.id}">
                    <div class="sizeControl">
                    <table>
                    <tbody>
                    <tr>
                    <td id="size-name-${res.size.id}">${res.size.name}</td>
                    <td id="size-price-${res.size.id}">${res.size.price}</td>
                    <td>
                    <a href="{{url('editsize')}}/${res.size.id}" class="editSize" data-id="${res.size.id}" data-price="${res.size.price}"data-name="${res.size.name}"><i class="md-icon material-icons">&#xE254;</i></a>
                    </td>
                    <td>
                        <a href="{{url('deletesize')}}/${res.size.id}" data-id="${res.size.id}" class="deleteSize"><i class="md-icon material-icons">delete</i></a>                </td>
                    </td>
                    <td>
                        <a href="{{url('copy-size/')}}/${res.size.id}" class="copySize" data-id="${res.size.id}" title="Copy"><i class="md-icon material-icons" style="color:green">file_copy</i></a>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    <div id="size-copy-${res.size.id}" style="display:none; margin:1%">
                        <form action="{{url('copy-size/')}}/${res.size.id}" class="copy-size-form" data-id="${res.size.id}" method="post">
                            @csrf
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-5">
                                    <div class="parsley-row">
                                        <label for="food_id">Choose Food<span class="req">*</span></label>
                                        <select id="food_id" name="food_id" class="uk-width-1-1" required data-md-select2>
                                            <option value="">Select Size</option>
                                            @forelse (\App\Food::all() as $food)
                                            <option value="{{$food->id}}">{{$food->name}}-{{$food->restaurant->name}}</option>
                                            @empty                                
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="uk-grid">
                                    <div class="">
                                        <button type="submit" class="md-btn md-btn-primary">Copy</button>
                                        <button type="button" class="md-btn md-btn-warning cancelSizeCopy" data-id="${res.size.id}">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="size-edit-${res.size.id}" style="display:none; margin:1%">
                                <form action="{{url('editsize/')}}/${res.size.id}" data-id="${res.size.id}"class="edit-size-form" method="post">
                                    @csrf
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-5">
                                            <div class="parsley-row">
                                                <label for="name">name<span class="req">*</span></label>
                                                <input type="text" name="name" value="${res.size.name}" data-parsley-trigger="change" required class="md-input">
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-1-5">
                                            <div class="parsley-row">
                                                <label for="min">Price<span class="req">*</span></label>
                                                <input type="number" step=".01" value="${res.size.price}" name="price" required class="md-input" />
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="">
                                                <button type="submit" class="md-btn md-btn-primary">Save</button>
                                                <button type="button" class="md-btn md-btn-warning cancelSizeControl" data-id="${res.size.id}">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    <div class="sizeOptions" id="size-options-${res.size.id}">
                    
                    </div>
                </li>
                `);
                $('#size-options-'+res.size.id).append(`
                <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium hierarchical_show" data-uk-grid-margin
                             style="margin:1px;">
                        </div>
                `)
                $('#size-head-tab-'+res.size.id).click();

                $('#success').data('message',"@lang('messages.SizeAdded')").click();
                
            }
        });
        $('#add-size-form')[0].reset()
});

$(document).on('click','.editSize',function(e){
    e.preventDefault();
    console.log('edit clicked');
    
    id = $(this).data('id');
    console.log(id);

    $('#size-control-'+id).hide();
    $('#size-edit-'+id).show();
});
$(document).on('click','.copySize',function(e){
    e.preventDefault();
    console.log('edit clicked');
    
    id = $(this).data('id');
    console.log(id);

    $('#size-control-'+id).hide();
    $('#size-copy-'+id).show();
});
$(document).on('click','.cancelSizeControl',function(e){
    e.preventDefault();
    console.log('cancel edit clicked');

    id = $(this).data('id');
    console.log(id);

    $('#size-edit-'+id).hide();
    $('#size-control-'+id).show();
});
$(document).on('click','.cancelSizeCopy',function(e){
    e.preventDefault();
    console.log('cancel edit clicked');

    id = $(this).data('id');
    console.log(id);

    $('#size-copy-'+id).hide();
    $('#size-control-'+id).show();
});
$(document).on('submit','.edit-size-form',function(e){
    e.preventDefault();
    console.log('form submitted');
    url= $(this).attr('action');
    type= $(this).attr('method');
    data= $(this).serializeArray();
    id = $(this).data('id');
    console.log(data);
    
    $.ajax({
            url:url,
            type:type,
            data:data,
            success:function(res){               
                $('#size-name-'+id).text(data[1]['value']);
                $('#size-price-'+id).html(data[2]['value']);
                $('#size-edit-'+id).hide();
                $('#size-control-'+id).show();
                $('#success').data('message',"@lang('messages.SizeUpdated')").click();
            }
    });
});
function confirm_action(){
   return confirm('Are You Sure You Want To Permenantly Delete This Size and Its Options?');
}
$(document).on('click','.deleteSize',function(e){
    e.preventDefault();
    url = $(this).attr('href');
    id = $(this).data('id');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
    
    if(!confirm_action())
        return;
    $.ajax({
            url:url,
            type:'POST',
            data: {_method: 'delete'},
            success:function(res){
                console.log(res)
                $('#size_id option[value='+id+']').remove();
                $('#size-tab-'+id).remove();
                $('#size-head-tab-'+id).remove();
                $('#success').data('message',"@lang('messages.SizeDeleted')").click();

            }
        });
});

</script>
@endsection