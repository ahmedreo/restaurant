<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable=['name','group_id'];

    public function restaurants()
    {
        return $this->hasMany(Restaurant::class);
    }
    public function foods()
    {
        return $this->hasMany(Food::class);
    }
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
    public function parent()
    {
        return $this->belongsTo(Group::class);
    }
}
