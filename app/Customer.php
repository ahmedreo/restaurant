<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $fillable = ['name', 'address', 'email', 'phone', 'company', 'user_id','postalCode','city','address_components'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
