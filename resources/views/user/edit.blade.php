@extends('layouts.app') 
@section('content')
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">

<div id="page_content_inner">
    <h1>Edit {{$user->name}}</h1>
    <div class="md-card">
        <div class="md-card-toolbar">
            <h3 class="md-card-toolbar-heading-text">User Update</h3>
        </div>
        <form action="{{url('user/'.$user->id)}}" method="POST" role="form" class="uk-form-stacked" enctype="multipart/form-data">
            <div class="md-card-content large-padding">
                @csrf @method('put')
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="name">Name<span class="req">*</span></label>
                            <input type="text" value="{{$user->name}}" name="name" data-parsley-trigger="change" required class="md-input" />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="email">email<span class="req">*</span></label>
                            <input type="text" value="{{$user->email}}" name="email" required class="md-input" />
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <button type="submit" class="md-btn md-btn-primary">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @role('admin')
    <div class="md-card">
        <div class="md-card-content large-padding">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-3">
                    <div class="parsley-row">
                        <label for="role">Role<span class="req">*</span></label>
                        <select id="role" name="role_id" class="uk-width-1-1" data-md-select2>
                                    <option value="">Select Role</option>
                                    @forelse ($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                    @empty
                                        
                                    @endforelse
                                    </select>
                    </div>
                </div>
                <div class="uk-width-medium-1-3">
                    <div class="parsley-row">
                        <label for="role">Restaurant<span class="req">*</span></label>
                        <select id="restaurant" name="restaurant_id" class="uk-width-1-1" data-md-select2>
                            <option value="">Select Restaurant</option>
                                @forelse ($restaurants as $restaurant)
                                    <option value="{{$restaurant->id}}">{{$restaurant->name}}</option>
                                @empty
                                @endforelse
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endrole 
    @if($user->restaurant)
    <div class="md-card md-card-collapsed">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
            </div>
            <h3 class="md-card-toolbar-heading-text">Restaurant Update</h3>
        </div>
        <div class="md-card-content large-padding">
            <form action="{{url('restaurant/'.$user->restaurant->id)}}" id="restUpdate" method="POST" role="form" class="uk-form-stacked"
                enctype="multipart/form-data">
                @method('put')
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                            <label for="open">open</label>
                            <input class="md-input" type="text" name="open" value="{{$user->restaurant->open}}" id="open" data-uk-timepicker>
                        </div>

                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                            <label for="close">close</label>
                            <input class="md-input" type="text" name="close" value="{{$user->restaurant->close}}" id="close" data-uk-timepicker>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="offDays">offDays<span class="req">*</span></label>
                            <select id="offDays" name="offDays[]" class="uk-width-1-1" multiple data-md-select2>
                                <option value="">Select offDays</option>
                                <option value="Saturday" >Saturday</option>
                                <option value="Sunday" >Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="None">None</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <button type="submit" class="md-btn md-btn-primary">Update</button>
                    </div>
                </div>

        </div>
    </div>
    @if(count($user->restaurant->reports)>0)
    <div class="md-card">
        <div class="md-card-toolbar">
            <h3 class="md-card-toolbar-heading-text">Restaurant Reports</h3>
        </div>
        <div class="md-card-content large-padding">
                <div class="md-list-outside-inner">
                        <ul class="md-list md-list-outside invoices_list" id="invoices_list">
            
            @forelse ($user->restaurant->reports as $report)
            <li id="report-{{$report->id}}">
            @role('admin')
            <a href="{{url('report-delete/'.$report->id)}}" data-id="{{$report->id}}" class="material-icons delete-report" style="float: left;
                padding: 33px;
                margin: -13px;
                font-size: x-large;
                color: red;
                background-color: white;">delete</a>
                @endrole
                <a href="{{url('report/'.$report->id)}}"  target="_blank" class="md-list-content " style="width:70%;    padding-left: 40px;">
                        <span class="md-list-heading uk-text-truncate">{{$report->name}} <span class="uk-text-small uk-text-muted">({{$report->created_at->toFormattedDateString()}})</span></span>
                <span class="uk-text-small uk-text-muted">{{$report->from}} - {{$report->to}}</span>
                    </a>
                </li>
            @empty
                
            @endforelse
        </ul>
                </div>
        </div>
    </div>
    @endif
    @endif 
    @if(auth()->user()->id == $user->id || auth()->user()->hasRole('admin'))
    <div class="md-card md-card-collapsed">

        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                Change Password
            </h3>
        </div>
        <div class="md-card-content">
            <form>
                
            </form>
            <form action="{{url('changepassword/'.$user->id)}}" method="POST" id="change-password">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="email">New Password<span class="req">*</span></label>
                            <input type="password" value="" name="password" required class="md-input" />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="email">Confirm Passowrd<span class="req">*</span></label>
                            <input type="password" value="" name="password_confirmation" required class="md-input" />
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <button type="submit" class="md-btn md-btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endif
<button class="md-btn" style="display:none" id="danger" data-message="Password Confirmation didn't match" data-status="danger"
    data-pos="bottom-center">Danger</button>
<button class="md-btn" style="display:none" id="success" data-message="<a href='#' class='notify-action'>Clear</a> Success Message"
    data-status="success" data-pos="bottom-center">Success</button>

</div>
@endsection
 
@section('pagejs')
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>

<script>
    $("#role").select2()
    $("#restaurant").select2()
@php
$restId = $user->restaurant?$user->restaurant->id:null

@endphp
offDays = [];
@if($user->restaurant)
@forelse (unserialize($user->restaurant->offDays) as $item)
      offDays.push("{{$item}}");
@empty
    
@endforelse
$("#offDays").select2()
$('#offDays').val(offDays).trigger('change');
@endif
$('#role').val({{$user->roles->first()->id}}).trigger('change');
@if($restId)
$('#restaurant').val({{$restId}}).trigger('change');
@endif
$(document).ready(function(){
    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
});
$('.delete-report').on('click',function(e){
    e.preventDefault();
    c = confirm('Are You Sure You Want to delete this report?');
    url = $(this).attr('href');
    id = $(this).data('id');
    if(c){

        $.ajax({
           url:url,
           type:"get",
           success:function(res){
               console.log(res)
               $('#report-'+id).hide('slow');
               $('#report-'+id).remove();
            }
        })
    }
})
$('#restUpdate').on('submit',function(e){
    e.preventDefault();
    form = $(this);
    url= $(this).attr('action');
    data = $(this).serializeArray();
    $.ajax({
           url:url,
           type:"POST",
           data:data,
           success:function(res){
               console.log(res)
               if(res == 200){
                    $('#success').data('message',"@lang('messages.RestaurantDataUpdated')").click();

               }else if(res == 403){
                     $('#danger').data('message',"@lang('messages.RestaurantDataUpdateFail')").click();
               }
           }
       });
});
$('#restaurant').change(function(){
    id = $('#restaurant').val();
    url= "{{url('addUserRestaurant/'.$user->id)}}"+'/'+id;
    $.ajax({
           url:url,
           type:"GET",
           success:function(res){
               console.log(res)
               if(res == 'ok'){
                    $('#success').data('message',"@lang('messages.UserRestaurantUpdated')").click();
               }else {
                     $('#danger').data('message',"@lang('messages.UserRestaurantFail')").click();
               }
           }
       });
});
$('#role').change(function(){
    id = $('#role').val();
    url= "{{url('changeRoles/'.$user->id)}}"+'/'+id;
    $.ajax({
           url:url,
           type:"GET",
           success:function(res){
               console.log(res)
               if(res == 'ok'){
                    $('#success').data('message',"@lang('messages.UserRoleUpdated')").click();

               }else {
                     $('#danger').data('message',"@lang('messages.UserRoleFail')").click();
               }
           }
       });
});
$('#change-password').submit(function(e){
   
    e.preventDefault();
    form = $(this);
    url= $(this).attr('action');
    data = $(this).serializeArray();
    console.log(url,data[1].value,data[2].value);
    if(data[1].value != data[2].value){
        console.log('not');
        $('#danger').data('message',"@lang('messages.PasswordNotMatch')").click();
    }else if(data[1].value.length <6){
        $('#danger').data('message',"@lang('messages.PasswordLength')").click();

    }else{
       $.ajax({
           url:url,
           type:"POST",
           data:data,
           success:function(res){
               console.log(res)
               if(res == 200){
                    $('#success').data('message',"@lang('messages.PasswordChangedSuccessufly')").click();

               }else if(res == 403){
                     $('#danger').data('message',"@lang('messages.wrongPassword')").click();
               }
           }
       });
       $('#change-password').closest('form').find("input[type=password]").val("");
    }
});

</script>
@endsection