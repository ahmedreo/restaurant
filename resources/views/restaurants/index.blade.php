@extends('layouts.app')
<style>
.icon-padding{
}
</style>
@section('content')
<div id="page_content_inner">

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}"
            title="Add Restaurant" href="{{url('restaurant/create')}}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
    <br>

    <div class="uk-grid uk-grid-width-large-1-1 uk-grid-width-medium-1-1 uk-grid-medium hierarchical_show"
        data-uk-grid-margin id="hierarchical-show" data-show-delay="280">
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">

                    <ul class="md-list md-list-addon uk-sortable sortable-handler uk-margin-medium-bottom"
                        data-uk-sortable="{group:'rests'}">
                        @forelse ($restaurants as $rest)
                        <li id="rest-{{$rest->id}}" class="restRow" data-order="{{$rest->order}}"
                            data-id="{{$rest->id}}">
                            <div class=" uk-panel-badge">
                                <a href="{{url('restaurant/'.$rest->id.'/edit')}}"><i
                                        class="md-icon material-icons">&#xE254;</i></a>
                                <a href="{{url('restaurant/'.$rest->id)}}" data-id="{{$rest->id}}" class="delete"><i
                                        class="md-icon material-icons">delete</i></a>
                            </div>
                            <div class="md-list-addon-element">
                                <img class="md-user-image md-list-addon-avatar"
                                    style="min-height: -webkit-fill-available;" src="{{asset('thumb-'.$rest->logo)}}"
                                    alt="{{$rest->name}}" />
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{$rest->name}} 
                                    @if ($rest->active)
                                    <i class="uk-icon-lightbulb-o uk-icon-hover uk-text-success" style="font-size: larger;padding-left: 1%"></i>
                                    @endif
                                    @if ($rest->new)
                                    <i class="material-icons icon-padding" title="New Restaurant">
                                        fiber_new
                                    </i>
                                    @endif
                                    @if ($rest->preorder)
                                    <i class="material-icons icon-padding" title="PreOrder">
                                        alarm_add
                                    </i>
                                    @endif
                                    @if ($rest->suggested)
                                    <i class="material-icons icon-padding" data-uk-tooltip="Suggested">
                                        thumb_up
                                    </i>
                                    @endif
                                </span>
                                <i class="uk-text-small uk-text-muted">{{$rest->details}}</i>


                            </div>

                            {{-- <td class="uk-text-center">
                                            @forelse ($rest->places as $place)
                                            <span class="uk-badge">{{$place->name}}</span>
                            @empty
                            @endforelse
                            </td>
                            <td class="uk-text-center">
                                <a href="{{url('restaurant/'.$rest->id.'/edit')}}"><i
                                        class="md-icon material-icons">&#xE254;</i></a>
                                <a href="{{url('restaurant/'.$rest->id)}}" data-id="{{$rest->id}}" class="delete"><i
                                        class="md-icon material-icons">delete</i></a>
                            </td> --}}
                        </li>
                        @empty @endforelse

                </div>

            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper" style="display: none;right:50%;" id="saveOrder">
    <a class="md-fab md-fab-primary md-fab-wave-light" id="saveOrder-btn"
        data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Save ordering">
        <i class="material-icons">save</i>
    </a>
</div>
<button class="md-btn" style="display:none;text-align:center;" id="danger" data-message="" data-status="danger"
    data-pos="bottom-center">Danger</button>
<button class="md-btn" style="display:none;text-align:center;" id="success" data-message="" data-status="success"
    data-pos="bottom-center">Success</button>
@endsection
@section('pagejs')
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/ecommerce_product_edit.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/uikit_fileinput.min.js')}}"></script>
<script>
    let origianlOrder=[];
    let currentOrder =[];
    let orderChanged=false;
    (function ($) {
        $(document).on('ready', function () {
            console.log('ready');
            
            $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
            });
            var sortable = $('[data-uk-sortable]'),
            	  button = $('button');
            button.click(function () {
              saveOrdering(sortable, button);
            });
            sortable.on('stop.uk.sortable', function (e, el, type) {
               
                setOrdering(sortable, el);
            });
            setOrdering(sortable);
        });

        function setOrdering(sortable, activeEl) {
            console.log('setOrdering');

             currentOrder=[];
            currentOrder.length =0;
            sortable.find('>li.restRow').each(function (key,val) {
                var $ele = $(this);
                
                // let ob={id:$ele.data('id'),order:$ele.data('order')}
                if(!orderChanged){                   
                    origianlOrder.push($ele.data('id'));
                    }
                else{
                    currentOrder.push($ele.data('id'));
                $ele.find('div.uk-badge').text(currentOrder[key]);
                }
                console.log(origianlOrder);
                console.log(currentOrder);
                
                
            });
            if (activeEl) {
                activeEl.find('div.uk-badge').addClass('uk-animation-scale-down');
            }
            // origianlOrder = origianlOrder.filter(v=>v!=null);
            if(JSON.stringify(currentOrder) === JSON.stringify(origianlOrder) || !orderChanged){
               $('#saveOrder').hide()

            }else{
               $('#saveOrder').show()
            }
            orderChanged = true
        }

    })(jQuery);
    $('#saveOrder-btn').on('click',()=>{
        url = "{{url('rests/changeorder')}}"
        $.ajax({
           url:url,
           type:"POST",
           data:{ids:currentOrder},
           success:function(res){
               $('#saveOrder').hide()
               origianlOrder = currentOrder
            }
        })
    })
    document.addEventListener("DOMContentLoaded",function(){
        
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
   
    $('#create_form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    html = `<tr>
                                <td class="uk-text-center uk-table-middle small_col"><input type="checkbox" data-md-icheck class="check_row"></td>
                                <td>`+res.name+`</td>
                            <td class="uk-text-center">`+res.details+`</td>
                                <td class="uk-text-center"><img src="{{asset('images')}}"`+res.picture+`
                                <td class="uk-text-center"><span class="uk-badge">
                                    </span></td>
                                <td class="uk-text-center">
                                    <a href="#"><i class="md-icon material-icons">&#xE254;</i></a>
                                    <a href="#"><i class="md-icon material-icons">&#xE88F;</i></a>
                                </td>
                            </tr>`
                    $('#table-body').prepend(html);
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $('.delete').on('click',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                $('#rest-'+id).hide(100);
                $('#danger').data('message',"@lang('messages.RestaurantDeleted')").click();

            }
        }
    })
});

})

</script>

<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>
@endsection