@extends('layouts.app')
@section('content')
<div id="page_content_inner">
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Add Allergy" id="create" data-uk-modal="{target:'#modal_create_Allergy'}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
    <br>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-nowrap table_check">
                    <thead>
                        <tr>
                            <th class="uk-width-2-10 uk-text-center">Code</th>
                            <th class="uk-width-2-10 uk-text-center">Desciprtion</th>
                            <th class="uk-width-2-10 uk-text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        @forelse ($allergies as $allergy)
                        <tr id="allergy-{{$allergy->id}}">
                            <td class="uk-text-center code">{{$allergy->code}}</td>
                            <td class="uk-text-center description">{{$allergy->description}}</td>
                            <td class="uk-text-center">
                            <a href="{{url('allergy/'.$allergy->id)}}" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Edit Allergy" data-uk-modal="{target:'#modal_Edit_Allergy'}" class="edit" data-info="{{$allergy->toJson()}}"><i
                                        class="md-icon material-icons ">&#xE254;</i></a>
                                <a href="{{url('allergy/'.$allergy->id)}}" data-id="{{$allergy->id}}" class="delete"><i
                                        class="md-icon material-icons">delete</i></a>

                            </td>
                        </tr>
                        @empty @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="uk-modal" id="modal_create_Allergy">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Create New Allergy <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{url('allergy')}}" id="create_form" method="POST">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label>Code</label>
                        <input type="text" name="code" id="code" class="md-input" required/>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <label>Description</label>
                        <input type="text" name="description" id="description" class="md-input" required/>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
    <div class="uk-modal" id="modal_Edit_Allergy">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Edit Allergy <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{url('allergy')}}" id="edit_form" method="POST">
                @csrf
                @method('PUT')
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label>Code</label>
                        <input type="text" name="code" id="edit-code" class="md-input" required/>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <label>Description</label>
                        <input type="text" name="description" id="edit-description" class="md-input" required/>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded",function(){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).on('click','a.edit',function(e){
    console.log($(this).attr('href'));
    console.log($(this).data('info'));
    $('#edit_form').attr('action',$(this).attr('href'));
    console.log($('#edit_form'));
    info = $(this).data('info')
    $('#edit_form :input').each(function(){
        var key = $(this).attr('name');
        if(info[key])
        $(this).val(info[key]).text(info[key]).focus();
    });
});
$('#edit_form').on('submit',function (e) {
    e.preventDefault();
    url = $(this).attr('action');
    type = $(this).attr('method');
    data = $(this).serializeArray();

    $.ajax({
            url:url,
            type:type,
            data:data,
            success:function(res){
                console.log(res);
                $.each(res,function(k,v) {
                    if($('#allergy-'+res.id+' td.'+k))
                        $('#allergy-'+res.id+' td.'+k).text(v);
                });
                UIkit.modal("#modal_Edit_Allergy").hide();
               
            }
    })
})
    $('#create_form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    html = `<tr>
                        <td class="uk-text-center code">`+res.code+`</td>
                                <td class="uk-text-center description">`+res.description+`</td>
                                <td class="uk-text-center">
                                    <a href="{{url('allergy/')}}/${res.id}" class="edit" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Edit Allergy" data-uk-modal="{target:'#modal_Edit_Allergy'}" class="edit" data-info="${res}"><i class="md-icon material-icons">&#xE254;</i></a>
                                    <a href="{{url('allergy/')}}/${res.id}" data-id="${res.id}" class="delete"><i
                                        class="md-icon material-icons">delete</i></a>
                                </td>
                            </tr>`
                    $('#table-body').prepend(html);
                    $('#create_form')[0].reset();

                    var modal = UIkit.modal("#modal_create_Allergy");
                    modal.hide()
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $(document).on('click','.delete',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                $('#allergy-'+id).hide(100);
            }
        }
    })
});
})

</script>
@endsection