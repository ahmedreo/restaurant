@extends('layouts.app') 
@section('content')
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/choices/choices.min.css?version=4.1.3')}}">
<script src="{{asset('admin/assets/choices/choices.min.js?version=4.1.3')}}"></script>

<div id="page_content_inner">
    <h1>Create New Restaurant</h1>
    <div class="md-card">
        <form action="{{url('restaurant')}}" method="POST" role="form" class="uk-form-stacked" enctype="multipart/form-data">

            <div class="md-card-content large-padding">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="name">Name<span class="req">*</span></label>
                            <input type="text" value="{{old('name')}}" name="name" data-parsley-trigger="change" required class="md-input" />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="addressCord">addressCord<span class="req">*</span></label>
                            <input type="text" value="{{old('addressCord')}}" name="addressCord" required class="md-input" />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-3">
                        <div class="parsley-row">
                            <label for="addressName">addressName<span class="req">*</span></label>
                            <input type="text" value="{{old('addressName')}}" name="addressName" required class="md-input" />
                        </div>
                    </div>
                </div>
               
                
                <div class="uk-width-medium-1-3">
                    <div class="parsley-row">
                        <label for="phone">phone<span class="req">*</span></label>
                        <input type="text" value="{{old('phone')}}" name="phone" required class="md-input" />
                    </div>
                </div><br>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_a uk-margin-small-bottom">
                                    Logo
                                </h3>
                                <input type="file" name="logo" data-default-file="{{old('logo')}}" id="input-file-a" class="dropify" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">Save</button>
                    </div>
                </div>
            </div>

    </div>
    </form>


</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {

 var textUniqueVals = new Choices('#choices-text-unique-values', {
        paste: false,
        duplicateItemsAllowed: false,
        editItems: true,
      });
    });

</script>
@endsection
 
@section('pagejs')
<script src="{{asset('admin/bower_components/handlebars/handlebars.min.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/handlebars_helpers.min.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/ecommerce_product_edit.js')}}"></script>

<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
@endsection