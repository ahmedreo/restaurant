@extends('layouts.app') 
@section('content')
<!-- weather icons -->
<link rel="stylesheet" href="{{asset('admin/bower_components/weather-icons/css/weather-icons.min.css')}}" media="all">
<!-- metrics graphics (charts) -->
<link rel="stylesheet" href="{{asset('admin/bower_components/metrics-graphics/dist/metricsgraphics.css')}}">
<!-- chartist -->
<link rel="stylesheet" href="{{asset('admin/bower_components/chartist/dist/chartist.min.css')}}">
<style>
    .new {
        transition: background-color 1s ease;
        background-color: greenyellow;
    }

    /* .order-details:hover{
  transition: background-color 1s ease;
  background-color: white;
} */
</style>
<script src="{{asset('admin/assets/js/easytime.min.js')}}"></script>
<script >
    // Enable pusher logging - don't include this in production
    var timer={};

        channel.bind(event, function(data) {
        //   console.log(event);
          order = data.order
          html = `<tr  id="order-row-${order.id}" class="order-details" data-order="${order.id}" data-uk-modal="{target:'#modal_overflow'}">
                    <td class="uk-text-center">${order.code}</td>
                    <td class="uk-text-center">${order.customer['name']}</td>
                    <td class="uk-text-center">${order.customer.address}</td>
                    <td class="uk-text-center">
                        <p id="order${order.id}">Just Now </td>`
                //              order['timer'] = new Timer();
                
                // order['timer'].start({precision: 'seconds', startValues: {seconds: 0}});
                // order['timer'].addEventListener('secondsUpdated', function (e) {
                //     var el = document.getElementById('order'+order.id).innerHTML =order['timer'].getTimeValues().toString();
                //     console.log(order['timer'].getTimeValues().toString())
                //     console.log(order.id.getTimeValues().toString())
                // });
                
                html +=`
                    @role('admin')
                    <td class="uk-text-center">${order.restaurant.name}</td>
                    @endrole
                    <td class="uk-text-center">
                    <span class="uk-badge uk-badge-notification">New</span>
                    </td>
                </tr>`;
        $('#new-orders').prepend(html);
        $('#order-row-'+order.id).click();

        count = $('#todayCount').text();
        console.log(count);
        count++;
        console.log(count);
        $('#todayCount').text(count);
        var audio = new Audio("{{asset('sound/plucky.mp3')}}");

        audio.play();
        $('#new-order-notify').data('message',`<a href='#' class='notify-action order-details' data-uk-modal="{target:'#modal_overflow'}" data-order='${order.id}'>View</a> You have a new order`)
        .click()
        setTimeout(function(){ $('#order-row-'+order.id).addClass('new') }, 1000);
       
        });

</script>
<div id="startValuesAndTargetExample">

</div>

<div id="page_content_inner">
    <button class="md-btn " style="display:none" data-order="" id="new-order-notify" data-message="Top Center">Top Center</button>

    <div class="uk-grid  uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable
        data-uk-grid-margin>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                    <span class="uk-text-muted uk-text-small">Today Orders</span>
                    <h2 class="uk-margin-remove"><span class="countUpMe" id="todayCount">{{$todayCount}}</span></h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right"><i class="material-icons">euro_symbol</i></div>
                    <span class="uk-text-muted uk-text-small">Today Revenue</span>
                    <h2 class="uk-margin-remove"><span class="countUpMe">{{$revenue}}<noscript>12456</noscript></span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-grid uk-grid-medium    uk-grid-small uk-width-small-1-1">
        <div class="uk-grid-width-large-1-1  uk-width-small-1-1">
            <div class="md-card-header" style="color: darkgray;font-weight: 600;padding-left: .5%;padding-top:2%;
    padding-bottom: 1%;text-align: center;background-: border-box;">New Orders</div>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check">
                            <thead>
                                <tr>
                                    <th class="uk-width-2-10 uk-text-center">Order Number</th>
                                    <th class="uk-width-2-10 uk-text-center">Customer Name</th>
                                    <th class="uk-width-2-10 uk-text-center">Customer Address</th>
                                    <th class="uk-width-1-10 uk-text-center">Order Date</th>
                                    @role('admin')
                                    <th class="uk-width-1-10 uk-text-center">Restaurant</th>
                                    @endrole
                                    <th class="uk-width-2-10 uk-text-center">View</th>
                                </tr>
                            </thead>
                            <tbody id="new-orders">
                                @forelse ($newOorders as $order)
    @include('dashboard.ordersrow') @empty @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-small-1-1 uk-grid-width-large-1-1">
            <div class="md-card-header" style="color: darkgray;font-weight: 600;padding-left: .5%;
    padding-bottom: 1%;text-align: center;background-: border-box;">Ongoing Orders</div>

            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check">
                            <thead>
                                <tr>
                                    <th class="uk-width-2-10 uk-text-center">Order Number</th>
                                    <th class="uk-width-2-10 uk-text-center">Customer Name</th>
                                    <th class="uk-width-2-10 uk-text-center">Customer Address</th>
                                    <th class="uk-width-1-10 uk-text-center">Order Date</th>
                                    <th class="uk-width-1-10 uk-text-center">Restaurant</th>
                                    <th class="uk-width-2-10 uk-text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="accepted-orders">
                                @forelse ($acceptedOrders as $order)
    @include('dashboard.ordersrow') @empty @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="md-card-header" style="color: darkgray;font-weight: 600;padding-left: .5%;
    padding-bottom: 1%;text-align: center;background-: border-box;">On Delivery Orders</div>

            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check">
                            <thead>
                                <tr>
                                    <th class="uk-width-2-10 uk-text-center">Order Number</th>
                                    <th class="uk-width-2-10 uk-text-center">Customer Name</th>
                                    <th class="uk-width-2-10 uk-text-center">Customer Address</th>
                                    <th class="uk-width-1-10 uk-text-center">Order Date</th>
                                    <th class="uk-width-1-10 uk-text-center">Restaurant</th>
                                    <th class="uk-width-2-10 uk-text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="with-delivery">
                                @forelse ($withDelivery as $order)
    @include('dashboard.ordersrow') @empty @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_overflow" class="uk-modal">
        <div class="uk-modal-dialog" style="padding-top: 0%">
            <button type="button" class="uk-modal-close uk-close"></button>
            <div class="uk-modal-header" style="BACKGROUND-COLOR: RED; padding-top: 7px;            padding-bottom: 7px;
            color: white;">
                ORDER DETAILS
            </div>
            <div id="modal-content">

            </div>
        </div>
    </div>

</div>

<script>

</script>
@endsection
 
@section('pagejs')
<script>
    function alertN() {
        var audio = new Audio("{{asset('sound/plucky.mp3')}}");

       var newCount = $('#new-orders')[0].childElementCount
       if(newCount >0){
            audio.play();
            if (! ('Notification' in window)) {
              alert('Web Notification is not supported');
              return;
            }

            Notification.requestPermission( permission => {
              let notification = new Notification('New Order alert!', {
                body: 'You Have '+newCount+ ' new Orders ', // content for the alert
                icon: "{{asset('images/alertLogo.png')}}" // optional image url
              });

              // link to page on clicking the notification
              notification.onclick = () => {
                window.open("{{url('ongoingorders/#')}}"+order.id);
              };
        });

       }

        
    }
    setInterval(alertN, 60000);

    $(document).ready(function(){
    var target = document.location.hash.replace("#", "");
    if (target.length) {
        console.log(target);
        $('#order-row-'+target).click();
    }
 
$(document).on('click','.change-status',function(e){
    e.preventDefault();
    id = $(this).data('id');
url = $(this).attr('href');
url = url+'?history=0';
console.log(url)
$.ajax({
    url:url,
    type:'GET',
    success:function(res){
        console.log(res);
        if(res.status == 1){
        $('#modal-content').html(res.html);
           $('#order-row-'+id).prependTo('#accepted-orders');
           $('#order-row-'+id+' span:first-child').addClass('uk-badge-warning').text('ongoing');
        }else if(res.status == 2){
        $('#modal-content').html(res.html);
            $('#order-row-'+id).prependTo('#with-delivery');
           $('#order-row-'+id+' span:first-child').removeClass('uk-badge-warning').addClass('uk-badge-success').text('With Delivery');
        }else if(res.status == 3){
        $('#modal-content').html(res.html);

            $('#order-row-'+id).hide('slow')
            $('#order-row-'+id).remove()
            // var modal =$('#modal_overflow')
            // modal.toggle();
        }else if(res.status == 4){
        $('#modal-content').html(res.html);

            $('#order-row-'+id).hide('slow');
            // var modal =$('#modal_overflow')
            // modal.hide();
        }

    }
})
});
$(document).on('click','.order-details',function(){
    id = $(this).data('order');
    $(this).removeClass('new')
    console.log(id);
    url="{{url('orderdetails/')}}"+'/'+id+'?history=0';
    console.log('name');
    $.ajax({
        url:url,
        type:"GET",
        success:function(res){
            $('#modal-content').html(res);
        }
    })
})
});

</script>
<script src="{{asset('js/jQuery.print.min.js')}}"></script>

<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>

<script src="{{asset('admin/bower_components/d3/d3.min.js')}}"></script>
<!-- metrics graphics (charts) -->
<script src="{{asset('admin/bower_components/metrics-graphics/dist/metricsgraphics.min.js')}}"></script>
<!-- chartist (charts) -->
<script src="{{asset('admin/bower_components/chartist/dist/chartist.min.js')}}"></script>
<!-- peity (small charts) -->
<script src="{{asset('admin/bower_components/peity/jquery.peity.min.js')}}"></script>
<!-- easy-pie-chart (circular statistics) -->
<script src="{{asset('admin/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<!-- countUp -->
<script src="{{asset('admin/bower_components/countUp.js/dist/countUp.min.js')}}"></script>
<!-- handlebars.js -->
<script src="{{asset('admin/bower_components/handlebars/handlebars.min.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/handlebars_helpers.min.js')}}"></script>
<!-- CLNDR -->
<script src="{{asset('admin/bower_components/clndr/clndr.min.js')}}"></script>

@endsection