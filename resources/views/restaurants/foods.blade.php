<div class="uk-width-medium-2-3">
    <a href="{{url('food/create?rest_id='.$rest->id)}}" class="md-btn md-btn-primary">Add Food</a>
    <ul class="md-list md-list-addon uk-sortable sortable-handler uk-margin-medium-bottom"
        data-uk-sortable="{group:'cats'}">

        @foreach ($categories as $key=>$cat)
        <li style="margin-left: 0%">
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-9-10">

                            <h4 class="heading_c uk-margin-bottom">{{$key}}</h4>
                            <ul class="md-list md-list-addon uk-sortable sortable-handler uk-margin-medium-bottom"
                                data-uk-sortable="{group:'{{$key}}'}">
                                @forelse ($cat as $food)
                                <li class="foodItem" style="width:100%" data-order="{{$food->order}}"
                                    data-id="{{$food->id}}">
                                    <div class=" uk-panel-badge">
                                        <a class="md-btn md-btn-primary copy"
                                            href="{{url('/food/copy/'.$food->id)}}">Copy</a>
                                        <a href="{{url('food/'.$food->id.'/edit')}}" class=""><i
                                                class="md-icon material-icons">&#xE254;</i></a>
                                        <a href="{{url('food/'.$food->id)}}" data-id="{{$food->id}}"
                                            class="delete "><i class="md-icon material-icons"
                                                style="color:red;">delete</i></a>
                                    </div>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar"
                                            src="{{asset($food->picture)}}" alt="" />
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">{{$food->name}}</span>
                                        <span class="uk-text-small uk-text-muted">{{$food->info}}</span>
                                    </div>
                                </li>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>

    </ul>
    <div class="md-fab-wrapper" style="display: none;" id="saveOrder">
        <a class="md-fab md-fab-primary md-fab-wave-light" id="saveOrder-btn"
            data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Save ordering">
            <i class="material-icons">save</i>
        </a>
    </div>
</div>
@section('pagejs')
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/ecommerce_product_edit.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/uikit_fileinput.min.js')}}"></script>
<script>
let origianlOrder=[];
    let currentOrder =[];
    let orderChanged=false;
    (function ($) {
        $(document).on('ready', function () {
            $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
            });
            var sortable = $('[data-uk-sortable]'),
            	  button = $('#saveOrder-btn');
            button.click(function () {
              saveOrdering(sortable, button);
            });
            sortable.on('stop.uk.sortable', function (e, el, type) {
               
                setOrdering(sortable, el);
            });
            setOrdering(sortable);
        });

        function setOrdering(sortable, activeEl) {
             currentOrder=[];
            currentOrder.length =0;
            sortable.find('>li.foodItem').each(function (key,val) {
                var $ele = $(this);
                // let ob={id:$ele.data('id'),order:$ele.data('order')}
                if(!orderChanged){                   
                    origianlOrder.push($ele.data('id'));
                    }
                else{
                    currentOrder.push($ele.data('id'));
                $ele.find('div.uk-badge').text(currentOrder[key]);
                }
            });
            if (activeEl) {
                activeEl.find('div.uk-badge').addClass('uk-animation-scale-down');
            }
            // origianlOrder = origianlOrder.filter(v=>v!=null);
            if(JSON.stringify(currentOrder) === JSON.stringify(origianlOrder) || !orderChanged){
               $('#saveOrder').hide()

            }else{
               $('#saveOrder').show()
            }
            orderChanged = true
        }

    })(jQuery);
    $('#saveOrder-btn').on('click',()=>{
        url = "{{url('food/'.$rest->id.'/changeorder')}}"
        $.ajax({
           url:url,
           type:"POST",
           data:{ids:currentOrder},
           success:function(res){
               $('#saveOrder').hide()
               origianlOrder = currentOrder
            }
        })
    })
</script>
@endsection