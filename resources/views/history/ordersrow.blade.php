@forelse ($orders as $order)
<tr  id="order-row-{{$order->id}}" class="order-details" data-order="{{$order->id}}" data-uk-modal="{target:'#modal_overflow'}">
        <td class="uk-text-center">{{$order->customer->name}}</td>
        <td class="uk-text-center">{{$order->code?$order->code:$order->id}}</td>
        <td class="uk-text-center">
           {{$order->created_at->toDateTimeString()}}
        </td>
        <td class="uk-text-center">{{$order->restaurant->name}}</td>
        
    </tr>
 @empty
 <tr>
     <td></td>
     <td>No Orders</td>
     <td ></td>
     <td></td>
 </tr>

 @endforelse