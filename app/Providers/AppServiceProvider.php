<?php

namespace App\Providers;

use App\Allergy;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;
use Illuminate\Foundation\Auth\User;
use App\Category;
use App\Restaurant;
use App\Food;
use App\Order;
use App\Tag;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
            view()->composer('*', function (View $view) {
                if (auth()->check()) {

                $categoryC = Category::count();
                $allergyC = Allergy::count();
                $tagC = Tag::count();
                if (auth()->check() && auth()->user()->hasRole('admin')) {
                    $userC = User::count();
                    $resturantC = Restaurant::count();
                    $foodC = Food::count();
                    $orderC = Order::count();
                } elseif (auth()->check() && auth()->user()->hasAnyRole(['owner', 'driver'])) {
                    $user = auth()->user();
                if(auth()->user()->restaurant_id){
                    $res = $user->restaurant;
                    // dd($res->restaurant_id);
                    $foodC = Food::where('group_id', $res->group->id)->count();
                    $orderC = Order::where('restaurant_id', $res->id)->count();
                    // dd($orderC);
                    $userC = $res->users->count();
                   $resturantC=null;
                }else{
                    return view('errors.403');
                }
                }}else{
                    $userC=null;$foodC=null; $resturantC=null;$categoryC=null;$orderC=null;$allergyC=null;$tagC=null;
                }
                $view->with(['userC' => $userC, 'foodC' => $foodC, 'categoryC' => $categoryC, 'resturantC' => $resturantC, 'orderC' => $orderC,'allergyC'=>$allergyC,'tagC'=>$tagC]);
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
