@extends('layouts.app') 
@section('content')
<link rel="stylesheet" href="{{asset('admin/bower_components/weather-icons/css/weather-icons.min.css')}}" media="all" />
<!-- metrics graphics (charts) -->
<link rel="stylesheet" href="{{asset('admin/bower_components/metrics-graphics/dist/metricsgraphics.css')}}" />
<!-- chartist -->
<link rel="stylesheet" href="{{asset('admin/bower_components/chartist/dist/chartist.min.css')}}" />
<div id="page_content_inner">
    <div class="uk-grid">
        <div class="uk-width-medium-1-3">
            <div class="md-btn-group">
                <a href="{{url()->full().'&'. http_build_query(['period' => 7])}}" class="md-btn md-btn-wave">Last 7 days</a>
                <a href="{{url()->full().'&'. http_build_query(['period' => 30])}}" class="md-btn md-btn-wave">Last 30 days</a>
            </div>
        </div>
        @role('admin')
        <div class="uk-width-medium-1-3 uk-push-1-3">
            <div class="uk-button-dropdown" data-uk-dropdown>
                <button class="md-btn">Select a restaurant <i class="material-icons">&#xE313;</i></button>
                <div class="uk-dropdown">
                    <ul class="uk-nav uk-nav-dropdown">
                        @forelse (\App\Restaurant::all() as $rest)
                        <li><a class="change-restaurant" href="{{url('dashboard/?restaurant_id='.$rest->id)}}">{{$rest->name}}</a></li>
                        @empty @endforelse
                    </ul>
                </div>
            </div>
        </div>
        @endrole
    </div>
    <h2 style="text-align:center;">STATS FOR THE LAST {{$period}} DAYS</h2>
    <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show"
        data-uk-sortable data-uk-grid-margin>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right">
                        <span class="peity_visitors peity_data">{{str_replace(['[',']'],'',$ordersPerDay->pluck('count'))}}</span>
                    </div>
                    <span class="uk-text-muted uk-text-small">Orders</span>
                    <h2 class="uk-margin-remove">
                        <span class="countUpMe">0<noscript>{{$orders}}</noscript></span>
                    </h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right">
                        <span class="peity_sale peity_data">{{str_replace(['[',']'],'',$ordersTotalPerDay->pluck('totalP'))}}</span>
                    </div>
                    <span class="uk-text-muted uk-text-small">Sale</span>
                    <h2 class="uk-margin-remove">
                        €<span class="countUpMe">0<noscript>{{$total}}</noscript></span>
                    </h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right">
                        <span class="peity_orders peity_data">@if($totalorders>0){{$orders/$totalorders*100}}/100 @endif</span>
                    </div>
                    <span class="uk-text-muted uk-text-small">Orders Completed</span>
                    <h2 class="uk-margin-remove">
                        <span class="countUpMe">0<noscript>@if($totalorders>0){{$orders/$totalorders*100}} @endif</noscript></span>%
                    </h2>
                </div>
            </div>
        </div>
        <div>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-float-right uk-margin-top uk-margin-small-right">
                        <span class="peity_live peity_data">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span>
                    </div>
                    <span class="uk-text-muted uk-text-small">Visitors (live)</span>
                    <h2 class="uk-margin-remove" id="peity_live_text">46</h2>
                </div>
            </div>
        </div>
    </div>

    <!-- large chart -->
    <div class="uk-grid">
        <div class="uk-width-1-1">
            <div class="md-card">
                <div class="md-card-toolbar">
                    <div class="md-card-toolbar-actions">
                        <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                        <i class="md-icon material-icons">&#xE5D5;</i>
                        <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                            <i class="md-icon material-icons">&#xE5D4;</i>
                            <div class="uk-dropdown uk-dropdown-small">
                                <!-- <ul class="uk-nav">
                                    <li><a href="#">Action 1</a></li>
                                    <li><a href="#">Action 2</a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <h3 class="md-card-toolbar-heading-text">
                        Chart
                    </h3>
                </div>
                <div class="md-card-content">
                    <div class="mGraph-wrapper">
                        <div id="mGraph_sale" class="mGraph" data-uk-check-display></div>
                    </div>
                    <div class="md-card-fullscreen-content">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-no-border uk-text-nowrap">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Best Seller</th>
                                    <th>Total Sale</th>
                                    <th>Change</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>January 2014</td>
                                    <td>Voluptatem et eum qui.</td>
                                    <td>$3 234 162</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>February 2014</td>
                                    <td>Et eos ducimus rem veritatis.</td>
                                    <td>$3 771 083</td>
                                    <td class="uk-text-success">+2.5%</td>
                                </tr>
                                <tr>
                                    <td>March 2014</td>
                                    <td>Incidunt culpa ea temporibus.</td>
                                    <td>$2 429 352</td>
                                    <td class="uk-text-danger">-4.6%</td>
                                </tr>
                                <tr>
                                    <td>April 2014</td>
                                    <td>Tempore ea rerum possimus.</td>
                                    <td>$4 844 169</td>
                                    <td class="uk-text-success">+7%</td>
                                </tr>
                                <tr>
                                    <td>May 2014</td>
                                    <td>Dignissimos quia reprehenderit repellat odit.</td>
                                    <td>$5 284 318</td>
                                    <td class="uk-text-success">+3.2%</td>
                                </tr>
                                <tr>
                                    <td>June 2014</td>
                                    <td>Est alias aut quis.</td>
                                    <td>$4 688 183</td>
                                    <td class="uk-text-danger">-6%</td>
                                </tr>
                                <tr>
                                    <td>July 2014</td>
                                    <td>Delectus reiciendis ea fugit.</td>
                                    <td>$4 353 427</td>
                                    <td class="uk-text-success">-5.3%</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <p class="uk-margin-large-top uk-margin-small-bottom heading_list uk-text-success">Some Info:</p>
                        <p class="uk-margin-top-remove">Ut facere eius impedit quaerat ut voluptas aliquid nostrum in fugiat et eum dicta ut quo enim est atque quia non officiis quo minus quisquam consequuntur atque velit architecto et tempore provident nihil aut et reiciendis eos iste quasi explicabo recusandae ipsa natus ipsa repellendus illo illum facere earum tenetur repellat nemo aspernatur vel corporis ut vel quia dolorem mollitia autem magni ducimus nisi laborum deleniti laborum facilis dolores natus quae quia necessitatibus laboriosam repellat minus odio atque et expedita in nihil pariatur quae velit error non culpa unde ut repellendus ut consequuntur et earum corrupti atque earum quisquam dolore totam temporibus consequuntur enim iusto possimus minus nostrum hic incidunt distinctio quae labore repudiandae voluptate.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="uk-grid">
        <div class="uk-width-large-1-1">
            <div class="md-card">
                <div class="md-card-content">
                    <h4 class="heading_c uk-margin-bottom">Orders per day</h4>
                    <div id="chartist_distributed_series" class="chartist"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-grid">
        <div class="uk-width-large-1-1">
            <div class="md-card">
                <div class="md-card-content">
                    <h4 class="heading_c uk-margin-bottom">Total revenue per day</h4>
                    <div id="totalPerDay" class="chartist"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('pagejs')
<script src="{{asset('admin/bower_components/d3/d3.min.js')}}"></script>
<!-- metrics graphics (charts) -->
<script src="{{asset('admin/bower_components/metrics-graphics/dist/metricsgraphics.min.js')}}"></script>
<!-- chartist (charts) -->
<script src="{{asset('admin/bower_components/chartist/dist/chartist.min.js')}}"></script>
<!-- peity (small charts) -->
<script src="{{asset('admin/bower_components/peity/jquery.peity.min.js')}}"></script>
<!-- easy-pie-chart (circular statistics) -->
<script src="{{asset('admin/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<!-- countUp -->
<script src="{{asset('admin/bower_components/countUp.js/dist/countUp.min.js')}}"></script>
<!-- handlebars.js -->
<script src="{{asset('admin/bower_components/handlebars/handlebars.min.js')}}"></script>
<script src="{{asset('admin/assets/js/custom/handlebars_helpers.min.js')}}"></script>
<!-- CLNDR -->
<script src="{{asset('admin/bower_components/clndr/clndr.min.js')}}"></script>

<!-- dashbord functions -->
<script src="{{asset('admin/assets/js/pages/dashboard.min.js')}}"></script>
<script>
    $(function() {
    // metrics-graphics

    // chartist
    altair_charts.chartist_charts();
});
altair_charts = {

 chartist_charts: function() {
        

        // distributed series
        let l = {!! $ordersPerDay->pluck('date') !!};
        // console.log(l)
        // l = l.split('&quot;').join('"');
        // console.log(l)
        var ch_distributed_series = new Chartist.Bar('#chartist_distributed_series', {
            labels: {!! $ordersPerDay->pluck('date') !!},
            series: {!!$ordersPerDay->pluck('count')!!}
        }, {
            distributeSeries: true
        });
        $window.on('resize',function() {
            ch_distributed_series.update();
        });
        var totalPerDay = new Chartist.Bar('#totalPerDay', {
            labels: {!! $ordersTotalPerDay->pluck('date') !!},
            series: {!! $ordersTotalPerDay->pluck('totalP')!!}
        }, {
            distributeSeries: true
        });
        $window.on('resize',function() {
            totalPerDay.update();
        });
    }}
</script>
@endsection