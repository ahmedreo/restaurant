<?php

use Illuminate\Http\Request;
use function foo\func;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([

    'middleware' => 'api',
], function ($router) {
    Route::get('getfoodbyrest/{rest}', 'RestaurantController@getRestauantFoods');
    Route::post('dashboardtest', 'DashboardController@test');
    Route::post('order', 'OrdersController@store');
    Route::get('findrestaurant/{key}', 'RestaurantController@findRestaurant');
    Route::get('find/{key}', 'RestaurantController@findOneRestaurant');
    Route::get('getbygroup/{group}/{place}', 'GroupsController@getRestByGroup');
    Route::get('getbypostal/{postal}', 'RestaurantController@findByPostal');
    Route::get('getbypostalformap/{postal}', 'RestaurantController@findByPostalForMap');
    // Route::get('getfoodbyrest/{rest}/', 'GroupsController@getFoodByRes');
    Route::get('orderstatus/{code}', 'OrdersController@getOrderStatus');
    Route::get('verify-coupon/{restaurant}/{code}', 'CouponsController@isValid');


    Route::post('register','Api\AuthController@create');
    Route::post('login','Api\AuthController@login');
    Route::middleware('auth:api')->get('/user',function(Request $request){
        return $request->user()->load('customer');
    });
});
