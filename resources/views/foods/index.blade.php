@extends('layouts.app') 
@section('content')
<div id="page_content_inner">
    <div class="uk-grid" data-uk-grid-margin>
        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Add food" href="{{url('food/create')}}">
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
        <div class="uk-width-medium-1-2">
            <div class="uk-button-dropdown" data-uk-dropdown>
                <button class="md-btn">Select a Restaurant <i class="material-icons">&#xE313;</i></button>
                <div class="uk-dropdown">
                    <ul class="uk-nav uk-nav-dropdown">
                    <li><a href="{{url('food')}}">All</a></li>
                        @forelse (\App\Restaurant::all() as $rest)
                            <li><a class="change-restaurant" href="{{url('restfoods/'.$rest->id)}}">{{$rest->name}}</a></li>                            
                        @empty
                            
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
        <br>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container" id="data-container">
                    
                     @include('foods.table')
                       
                </div>

            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded",function(){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $('.change-restaurant').on('click',function(e){
        e.preventDefault();
        url = $(this).attr('href');
        type ="GET";
        console.log(type,url)
        $.ajax({
            url:url,
            type:type,
            success:function(res){
                if(res.length>0){
                    $('#data-container').html(res);
                }else{
                    $('#data-container').html('');

                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $('.delete').on('click',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                $('#food-'+id).hide(100);
            }
        }
    })
});
});

    </script>
@endsection