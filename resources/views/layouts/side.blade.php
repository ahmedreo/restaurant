<style>
.menu_badge{
    float: right;
}
</style>
<aside id="sidebar_main">

    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="index.html" class="sSidebar_hide sidebar_logo_large">
                    <img class="logo_regular" src="{{asset('admin/assets/img/logo_main.png')}}" alt="" height="15" width="71"/>
                    <img class="logo_light" src="{{asset('admin/assets/img/logo_main_white.png')}}" alt="" height="15" width="71"/>
                </a>
            <a href="index.html" class="sSidebar_show sidebar_logo_small">
                    <img class="logo_regular" src="{{asset('admin/assets/img/logo_main_small.png')}}" alt="" height="32" width="32"/>
                    <img class="logo_light" src="{{asset('admin/assets/img/logo_main_small_light.png')}}" alt="" height="32" width="32"/>
                </a>
        </div>

    </div>

    <div class="menu_section">
        <ul>
            <li title="Dashboard" @if(request()->path() == 'dashboard') class="current_section" @endif >
                @php
                    if(auth()->user()->hasRole('admin'))
                        $res = 4;
                    else
                        $res = auth()->user()->restaurant->id;
                @endphp
                <a href="{{url('dashboard?restaurant_id='.$res)}}" >
                        <span class="menu_icon"><i class="material-icons" style="color:orange">dashboard</i></span>
                        <span class="menu_title">Dashboard</span>
                    </a>

            </li>
            <li title="Ongoing Orders"  @if(request()->path() == 'ongoingorders') class="current_section" @endif >
                <a href="{{url('ongoingorders')}}">
                        <span class="menu_icon"><i class="material-icons" style="color:lightseagreen">shopping_cart</i></span>
                        <span class="menu_title">Ongoing Orders</span>
                    </a>

            </li>
            @role('admin')
            <li title="Users" @if(request()->path() == 'user') class="current_section" @endif >
                <a href="{{url('user')}}" >
                        <span class="menu_icon"><i class="material-icons" style="color:blueviolet">people</i></span>
                        <span class="menu_title">Users</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$userC}}</span>

                    </a>

            </li>
            <li title="Category"  @if(request()->path() == 'category') class="current_section" @endif>
                <a href="{{url('category')}}" >
                        <span class="menu_icon"><i class="material-icons" style="color:chocolate">category</i></span>
                        <span class="menu_title">Categories</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$categoryC}}</span>

                    </a>

            </li>
            <li title="Groups"  @if(request()->path() == 'groups') class="current_section" @endif>
                <a href="{{url('groups')}}" >
                        <span class="menu_icon"><i class="material-icons" style="color:chocolate">category</i></span>
                        <span class="menu_title">Groups</span>
                        {{-- <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$categoryC}}</span> --}}

                    </a>

            </li>
            <li title="Restaurants" @if(request()->path() == 'restaurant') class="current_section" @endif>
                <a href="{{url('restaurant')}}"   >
                        <span class="menu_icon"><i class="material-icons" style="color:red">restaurant</i></span>
                        <span class="menu_title">Restaurants</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$resturantC}}</span>

                    </a>

            </li>
            <li title="Foods" @if(request()->path() == 'food') class="current_section" @endif >
                <a href="{{url('food')}}">
                        <span class="menu_icon"><i class="material-icons" style="color:green">fastfood</i></span>
                        <span class="menu_title">Foods</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$foodC}}</span>
                    </a>

            </li>
            <li title="Allergy" @if(request()->path() == 'allergy') class="current_section" @endif >
                <a href="{{url('allergy')}}">
                        <span class="menu_icon"><i class="material-icons" style="color:green">fastfood</i></span>
                        <span class="menu_title">Allergy</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$allergyC}}</span>
                    </a>

            </li>
            <li title="Tag" @if(request()->path() == 'tag') class="current_section" @endif >
                <a href="{{url('tag')}}">
                        <span class="menu_icon"><i class="material-icons" style="color:green">fastfood</i></span>
                        <span class="menu_title">Tag</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$tagC}}</span>
                    </a>

            </li>
            <li title="history"@if(request()->path() == 'history') class="current_section" @endif>
                <a href="{{url('history')}}">
                        <span class="menu_icon"><i class="material-icons" style="color:blue">history</i></span>
                        <span class="menu_title">History</span>
                        <span class="menu_badge uk-badge uk-badge-notification" style="position: static;">{{$orderC}}</span>

                    </a>

            </li>
            @endrole
            

        </ul>
    </div>
</aside>
<!-- main sidebar end -->