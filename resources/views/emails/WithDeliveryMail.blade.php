@component('mail::message')
# Your Order On its Way

@php
    $status = $order->statuses()->orderBy('created_at','DESC')->first();
@endphp

    {{$status->status}}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
