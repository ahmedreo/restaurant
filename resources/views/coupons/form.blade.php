{{ csrf_field() }}
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-3">
                    <label>CODE</label>
                    <input type="text" id="code" name="code" class="md-input" />
                </div>
                <div class="uk-width-medium-1-3">
                    <label>COUNT</label>
                    <input type="number" id="count" name="count" min="1" class="md-input label-fixed" />
                </div>
                <div class="uk-width-medium-1-3">
                        <div class="uk-input-group">
                    <label>Descount Type</label>
                    <select id="descount_type" name="descount_type" class="md-input" data-uk-tooltip="{pos:'top'}" title="Select descount type">
                        <option value="0">e</option>
                        <option value="1">%</option>
                    </select>
                        </div>
                </div>
            </div>
       
    </div>
<br>
    <div class="uk-width-medium-1-1" style="padding-top: 20px">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-3">
                        <div class="uk-input-group">
                    <label>Amount</label>
                    <input type="number" name="descount" id="descount" min="0" step=".01" class="md-input" />
                        </div>
                </div>
                <div class="uk-width-medium-1-3">
                    <div class="uk-input-group">
                        <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                        <label for="start">Select  Start Date</label>
                        <input class="md-input" name="start" type="text" id="start" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                    </div>
                </div>
                <div class="uk-width-medium-1-3">
                        <div class="uk-input-group">
                    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                    <label for="end">Select  End Date</label>
                    <input class="md-input" name="end" type="text" id="end" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                        </div>
                </div>
            </div>
       
    </div>
   
</div>