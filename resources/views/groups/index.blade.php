@extends('layouts.app') 
@section('content')
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">
<style>
.select2-container{
    z-index:100000;
}
</style>
<div id="page_content_inner">
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Add Group" id="create" data-uk-modal="{target:'#modal_create_Group'}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
    <div class="uk-modal" id="modal_create_Group">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Create New Group <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{url('groups')}}" id="create_form" method="POST">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label>Name</label>
                        <input type="text" name="name" id="name" class="md-input" required/>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
    <div class="uk-modal" id="modal_edit_Group">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">EditGroup <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{url('groups')}}" id="edit_form" method="POST">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-3">
                        <label>Name</label>
                        {{method_field('put')}}
                        <input type="text" name="name" id="edit-name" class="md-input" required/>
                        <input type="hidden" name="id" id="edit-id">
                    </div>
                    <br>
                    <div class="uk-width-medium-1-1" style="margin-top:3%;">
                        <label for="restaurant_id">Restaurants<span class="req">*</span></label>
                        <select id="restaurant_id" name="restaurant_id[]" class="uk-width-1-1" style="width:50%" multiple  data-md-select2>
                            @forelse (App\Restaurant::all() as $rest)
                            <option value="{{$rest->id}}">{{$rest->name}}</option>
                            @empty                                
                            @endforelse
                        </select>
                    </div>
                    <div class="uk-width-medium-1-1" style="margin-top:3%;">
                        <label for="group_id">Sub Groups<span class="req">*</span></label>
                        <select id="group_id" name="group_id[]" class="uk-width-1-1" style="width:50%" multiple  data-md-select2>
                            @forelse (App\Group::all() as $group)
                            <option value="{{$group->id}}">{{$group->name}}</option>
                            @empty                                
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
    <br>
    <div class="uk-grid-width-large-1-1  uk-width-small-1-1" data-uk-grid-margin
        id="hierarchical-show" data-show-delay="280">
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                            <tr>
                                <th class="uk-width-1-10 uk-text-center">ID</th>
                                <th class="uk-width-2-10 uk-text-center">Name</th>
                                <th class="uk-width-3-10 uk-text-center">sub groups</th>
                                <th class="uk-width-3-10 uk-text-center">Restaurants</th>
                                <th class="uk-width-1-10 uk-text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="table-body">
                            @forelse ($groups as $group)
                            <tr id="group-{{$group->id}}">
                                <td class="uk-text-center">{{$group->id}}</td>
                                <td class="uk-text-center">{{$group->name}}</td>
                                <td class="uk-text-center">@forelse ($group->groups as $g)
                                    <span class="uk-badge uk-badge-notification uk-badge-primary">{{$g->name}}</span>
                                @empty
                                    
                                @endforelse</td>
                                <td class="uk-text-center">@forelse ($group->restaurants as $r)
                                    <span class="uk-badge uk-badge-notification uk-badge-primary">{{$r->name}}</span>
                                @empty
                                    
                                @endforelse</td>
                                <td class="uk-text-center actions">
                                <a href="" data-uk-modal="{target:'#modal_edit_Group'}" id="edit-{{$group->id}}"data-groups="{!!$group->groups->pluck('id')!!}" data-rest="{!!$group->restaurants->pluck('id')!!}" class="edit" data-id="{{$group->id}}" data-name="{{$group->name}}"><i class="md-icon material-icons">&#xE254;</i></a>
                                <a href="{{url('groups/'.$group->id)}}" data-id="{{$group->id}}" class="delete"><i class="md-icon material-icons">delete</i></a>
                                </td>
                            </tr>
                            @empty @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<button class="md-btn" style="display:none" id="danger" data-message="" data-status="danger" data-pos="bottom-center">Danger</button>
<button class="md-btn" style="display:none" id="success" data-message="" data-status="success" data-pos="bottom-center">Success</button>

<script>
    document.addEventListener("DOMContentLoaded",function(){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// $('#resturant_id').val(resturant_id).trigger('change');
$(document).on('click','.edit',function(){
    $('#modal_edit_Group').toggle()
    console.log('for edit')
    id = $(this).data('id');
    name = $(this).data('name');
    $('#edit-id').val(id);
    $('#edit-name').val(name).text(name);
    rest = $(this).data('rest');
    groups = $(this).data('groups');
$("#restaurant_id").select2()

$('#restaurant_id').val(rest).trigger('change');
$("#group_id").select2()

$('#group_id').val(groups).trigger('change');

});
$('#edit_form').on('submit',function(e){
    e.preventDefault();
        id = $('#edit-id').val();
        url = "{{url('groups/')}}"+'/'+id;
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        
        name= $('#edit-name').val();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    $('#group-'+id+' td:nth-child(2)').text(name);
                    console.log($('#group-'+id+' td:nth-child(2)'))
                    $('#edit-'+id).data('rest',res.rest)
                    $('#edit-'+id).data('groups',res.groups)
                    $('#edit-'+id).data('name',res.group.name)
                    $('#edit-'+id).data('id',res.group.id)
                   console.log(res.rest);
                   console.log(res.groups);
                   restsName = '';
                   res.resnames.forEach(element => {
                       restsName +=`
                    <span class="uk-badge uk-badge-notification uk-badge-primary">${element}</span>
                       `
                   });
                   groupsName = '';
                   res.groupnames.forEach(element => {
                    groupsName +=`
                    <span class="uk-badge uk-badge-notification uk-badge-primary">${element}</span>
                       `
                   });

                   $('#group-'+id+' td:nth-child(3)').text("");
                   $('#group-'+id+' td:nth-child(4)').text("");
                   $('#group-'+id+' td:nth-child(3)').append(groupsName);
                   $('#group-'+id+' td:nth-child(4)').append(restsName);
                }
                $('#edit_form')[0].reset();
                $('#modal_edit_Group').toggle();
                $('#success').data('message',"@lang('messages.GroupEdited')").click();

            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
});
$('#create').on('click',function(){
    $('#modal_create_Group').toggle()
})
    $('#create_form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    html = `<tr id="group-${res.group.id}">
                                <td class="uk-text-center">`+res.group.id+`</td>
                                <td class="uk-text-center">`+res.group.name+`</td>
                                <td class="uk-text-center"></td>
                                <td class="uk-text-center"></td>
                                <td class="uk-text-center">
                                    <a href="#" class="edit" data-uk-modal="{target:'#modal_edit_Group'}" id="edit-`+res.group.id+`" data-id="`+res.group.id+`" data-name="`+res.group.name+`"><i class="md-icon material-icons">&#xE254;</i></a>
                                    <a href="{{url('groups')}}/${res.group.id}" class="delete" data-id="`+res.group.id+`"><i class="md-icon material-icons">delete</i></a>
                                </td>
                            </tr>`
                    $('#table-body').prepend(html);
                }
                $('#create_form')[0].reset();
                $('#modal_create_Group').toggle()
                $('#success').data('message',"@lang('messages.GroupCreated')").click();
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $(document).on('click','.delete',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    console.log(id)

    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                console.log(id)
                $('#group-'+id).hide(100);
                $('#danger').data('message',"@lang('messages.GroupDeleted')").click();

            }
        }
    })
})
})

</script>
@endsection
 
@section('pagejs')
<script src="{{asset('admin/bower_components/select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>
@endsection