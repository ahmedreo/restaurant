<?php

return [

    'PasswordChangedSuccessufly'=>'Password has ben changed successufly',
    'wrongPassword'=>'Your enterd a wrong password',
    'PasswordLength'=>'Password Length cannot be less than 6',
    'PasswordNotMatch'=>'Password Confirmation didn\'t match',
    'OptionDeleted'=>'Option has been deleted',
    'OptionEdited'=>'Option has been edited',
    'ItemAdded'=>'New item has been added',
    'ItemDeleted'=>'Item has been deleted',
    'ItemAdded'=>'New Item has been added',
    'UsersUpdated'=>'Users list has been updated',
    'UserRestaurantUpdated'=>'User\'s Restaurant Updated',
    'UserRestaurantFail'=>'User\'s Restaurant Fail',
    'UserRoleUpdated'=>'User\'s Role Updated',
    'UserRoleFail'=>'User\'s Role Fail',
    'RestaurantDataUpdated'=>'Restaurant Data Updated',
    'RestaurantDataUpdateFail'=>'Restaurant Data Update Fail',
    'SizeAdded'=>'New size has been added',
    'SizeDeleted'=>'Size has been deleted',
    'SizeUpdated'=>'Size has been updated',
    'GroupDeleted'=>'The group has been deleted',
    'GroupCreated'=>'New group has been created',
    'GroupEdited'=>'The group has been edited',
    'RestaurantDeleted'=>'Restaurant has been deleted',
    'UserAdded'=>'New User has been created',
    'NoCoupons'=>'No Coupons',

];