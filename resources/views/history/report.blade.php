<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$restaurant->name}}</title>
    <style>
        table {
            width: 100%;
            border-spacing: 0;

        }

        tr {
            width: 100%;
        }

        .borderd-td-left {
            border-left: 1px solid black;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: left;
            padding: 10px;

        }

        .borderd-td-right {
            border-right: 1px solid black;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: right;
            padding: 10px;

        }

        .borderd-td-middle {
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: center;
        }
        .remove-top-border > *{
            border-top:none;
        }
        .items > *{
            border-bottom: 1px solid black;
        }
        .page-break {
    page-break-after: always;
}
        @page {
	header: page-header;
	footer: page-footer;
}
    </style>

</head>

<body>
        <htmlpageheader name="page-header">
                Your Header Content
            </htmlpageheader>
            <htmlpagefooter name="page-footer">
                    Your Footer Content
                </htmlpagefooter>
    <div style="margin:10px">
        <table>
            <tr>
                <td width='60%' style="text-align: center">
                    {{-- <img src="{{asset('images/alertLogo.png')}}" width="70px"> --}}
                    <p>Delivery Hero Germany GmbH - Oranienburger Straße 70 - 10117 Berlin
                    </p>
                </td>
                <td style="vertical-align: baseline">
                    <div style="border-left:2px solid black; padding:10px;line-height: 1">
                        <p style="font-weight: bolder">Delivery Hero Germany GmbH</p>
                        <p>Oranienburger Str. 70</p>
                        <p>D-10117 Berlin</p>
                        <p>T +49 30544459111</p>
                        <p>E cc_finance@deliveryhero.com</p>
                        <p>W www.lieferheld.de </p>
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td width="30%">
                    <p>{{$restaurant->name}}</p>
                    <p>{{$restaurant->addressName}}</p>
                </td>
                <td style="text-align: center">
                    {{-- <img width="70px" src="{{asset($restaurant->logo)}}" alt="{{$restaurant->name}}" /> --}}

                </td>

            </tr>
        </table>
        <table>
            <tr>
                <td class="borderd-td-middle" width="33%">
                  <div style="padding-right: 10%;
                  padding-top: 2px;">
                   <span> Rechnungsnr.:</span>  <span style="float:right">  {{$report->name}}</span>
                </div>
                <div style="padding-right: 10%;
                padding-top: 2px;">
                   <span style="padding-right:90px"> Kundennr.:</span> <span  style="float:right">{{$restaurant->id}}</span>
                </div>
                </td>
                <td class="borderd-td-middle" width="33%">
                        (Bitte bei Zahlung unbedingt angeben!)
                </td>
                <td class="borderd-td-middle" width="33%">
                        <div style="padding-right: 10%;
                        padding-top: 2px;">
                          Rechnungsdatum.:  <span style="float:right">  {{$report->created_at->toFormattedDateString()}}</span>
                      </div>
                </td>
            </tr>
        </table>
        <p>Sehr geehrte Damen und Herren,</p>
               <p> für die Portale pizza.de (PDE) und Lieferheld (LIH) stellen wir Ihnen unsere Leistungen für den Zeitraum 
                   {{$report->from}} - {{$report->to}}
                wie folgt in Rechnung:</p>

        <table>
            <tr>
                <th class="borderd-td-left">
                    Beschreibung
                </th>
                <th class="borderd-td-middle">
                    Menge
                </th>
                <th class="borderd-td-middle">
                    Umsatz/Preis
                </th>
                <th class="borderd-td-right">
                    Gesamtbetrag
                </th>
            </tr>
            <tr><td style="padding-top:5px"></td></tr>
            <tr >
                <td class="borderd-td-left">
                    Orders
                </td>
                <td class="borderd-td-middle">
                    {{$report->count}}
                </td >
                <td class="borderd-td-middle">
                    {{$report->sum}}
                </td>
                <td class="borderd-td-right">
                  
                        {{round($report->sum*2.5/100,2)}}€
                </td>
            </tr>
            <tr  class="remove-top-border">
                <td class="borderd-td-left" >
                </td>
                <td class="borderd-td-middle" >
                </td >
                <td class="borderd-td-middle" >
                    Zwischensumme<br>
                    Umsatzsteuer 19 %
                </td>
                <td class="borderd-td-right" >
                  
                    {{round($report->sum*2.5/100,2)}}€<br>
                        {{round(($report->sum*2.5/100)*19/100,2) }}€
                </td>
            </tr>
            <tr class="remove-top-border">
                <td class="borderd-td-left"  >
                </td>
                <td class="borderd-td-middle"  >
                </td >
                <td class="borderd-td-middle"  >
                    (1) Zwischensumme (inkl. USt.)
                </td>
                <td class="borderd-td-right"  >
                  
                        {{round(($report->sum*2.5/100) + (($report->sum*2.5/100)*19/100),2)}}€
                </td>
            </tr>
        </table>
        <div class="page-break"></div>
        <br>
        <table>
                <tr class="items">
                    <td style="border-bottom:1px solid black ; padding-bottom:5px; text-align: left">Pos</td>
                    <td style="border-bottom:1px solid black ; padding-bottom:5px; text-align: center">Bestellwert</td>
                    <td style="border-bottom:1px solid black ; padding-bottom:5px; text-align: center">Datum</td>
                    <td style="border-bottom:1px solid black ; padding-bottom:5px; text-align: center">Status</td>
                    <td style="border-bottom:1px solid black ; padding-bottom:5px; text-align: center">Bestellnr</td>
                    
                </tr>
                @forelse ($orders as $i=>$order)
                    <tr >
                    <td style="text-align: left">{{$i+1}}</td>
                    <td style="text-align: center">{{$order->code}}</td>
                    <td style="text-align: center">{{$order->created_at}}</td>
                    <td style="text-align: center">
                     
                        @if($order->status == 3)
                        Done
                        @elseif($order->status == 4)
                        Rejected
                        @endif
                    </td>
                    <td style="text-align: center">{{$order->total}}</td>
                   
                    </tr>
                @empty
                    No Orders
                @endforelse
        </table>
    </div>
    <script>
        win = window;
    	win.setTimeout( function () {
			
				win.print(); // blocking - so close will not
				// win.close(); // execute until this is done
		}, 1000 );
    </script>
</body>

</html>