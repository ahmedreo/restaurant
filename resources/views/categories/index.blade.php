@extends('layouts.app') 
@section('content')
<div id="page_content_inner">
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Add Category" href="{{url('category/create')}}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
    <br>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-nowrap table_check">
                    <thead>
                        <tr>
                            <th class="uk-width-1-10 uk-text-center small_col"><input type="checkbox" data-md-icheck class="check_all"></th>
                            <th class="uk-width-2-10 uk-text-center">Name</th>
                            <th class="uk-width-2-10 uk-text-center">Details</th>
                            <th class="uk-width-1-10 uk-text-center">Picture</th>
                            <th class="uk-width-2-10 uk-text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        @forelse ($categories as $category)
                    <tr id="cat-{{$category->id}}">
                            <td class="uk-text-center uk-table-middle small_col"><input type="checkbox" data-md-icheck class="check_row"></td>
                            <td class="uk-text-center">{{$category->name}}</td>
                            <td class="uk-text-center">{{$category->details}}</td>
                        <td class="uk-text-center"><img class="img_thumb" style="height: 50px" src="{{asset($category->picture)}}"></td>
                            <td class="uk-text-center">
                            <a href="{{url('category/'.$category->id.'/edit')}}"><i class="md-icon material-icons">&#xE254;</i></a>
                            <a href="{{url('category/'.$category->id)}}" data-id="{{$category->id}}" class="delete"><i class="md-icon material-icons">delete</i></a>

                            </td>
                        </tr>
                        @empty @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded",function(){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $('#create_form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    html = `<tr>
                                <td class="uk-text-center uk-table-middle small_col"><input type="checkbox" data-md-icheck class="check_row"></td>
                                <td>`+res.name+`</td>
                            <td class="uk-text-center">`+res.details+`</td>
                                <td class="uk-text-center"><img src="{{asset('images')}}"`+res.picture+`
                                <td class="uk-text-center"><span class="uk-badge">
                                    </span></td>
                                <td class="uk-text-center">
                                    <a href="#"><i class="md-icon material-icons">&#xE254;</i></a>
                                    <a href="#"><i class="md-icon material-icons">&#xE88F;</i></a>
                                </td>
                            </tr>`
                    $('#table-body').prepend(html);
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $('.delete').on('click',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                $('#cat-'+id).hide(100);
            }
        }
    })
});
})

</script>
@endsection