<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use App\Restaurant;
use Illuminate\Support\Facades\Auth;

class CouponsController extends Controller
{

    private $user;
    public function __construct()
    {
        info('coupon controller');
        $this->middleware('auth', ['except' =>['isValid']]);
        // $this->user = Auth::user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function restCoupons(Restaurant $rest)
    {
        $coupons = Coupon::with('orders.customer')->where('restaurant_id', $rest->id)->get();
        info($coupons);
        $html = view('coupons.index')->with(['coupons' => $coupons, 'rest' => $rest])->render();
        return response()->json(['html' => $html]);
    }
    public function index()
    {
        //
    }

    public function isValid(Restaurant $restaurant, $code)
    {
        info($restaurant);
        info($code);
        $coupon = Coupon::where('code',$code)->where('restaurant_id',$restaurant->id)->first();
        info('is valid');
        if($coupon){
            info($coupon);
        $valid= $coupon->isValid();
        info($valid);
        if($valid)
         return $coupon;
        else
         return 3;
        }else{
            return 2;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        info('store method');
        info($request->all());
        if (Auth::user()->hasRole('admin'))
            $coupon = Coupon::create($request->all());
        else {
            $request['restaurant_id'] = $this->user->restaurant_id;
            $coupon = Coupon::create($request->all);
        }
        $rest = Restaurant::find($request['restaurant_id']);
        $coupons = Coupon::with('orders.customer')->where('restaurant_id', $rest->id)->get();
        $html = view('coupons.index')->with(['coupons' => $coupons, 'rest' => $rest])->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        info('show method');

        return $coupon;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        info('update method');
        if (Auth::user()->hasRole('admin'))
            $coupon = $coupon->update($request->all());
        else {
            $request['restaurant_id'] = $this->user->restaurant_id;
            $coupon = $coupon->update($request->all);
        }
        $rest = Restaurant::find($request['restaurant_id']);
        $coupons = Coupon::with('orders.customer')->where('restaurant_id', $rest->id)->get();
        $html = view('coupons.index')->with(['coupons' => $coupons, 'rest' => $rest])->render();

        return response()->json(['html' => $html]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        info('delete');
        if (Auth::user()->hasRole('admin')) {
            $coupon->delete();
        } else if (Auth::user()->resaurant_id == $coupon->restaurant_id) {
            $coupon->delete();
        }
        return response()->json(['success' => true]);
    }
}
