<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['picture', 'name', 'details'];

    public function foods()
    {
        return $this->hasMany(Food::class);
    }
}
