<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->double('minOrder',8,2)->default(0);
            $table->double('deliveryPrice',8,2)->default(0);
            $table->integer('deliveryTime')->default(0);
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->dropColumn('minOrder',8,2);
            $table->dropColumn('deliveryPrice',8,2);
            $table->dropColumn('deliveryTime');
        });
    }
}
