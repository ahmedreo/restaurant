<small>Order was dispatched at</small><br>
<i class="material-icons">access_time</i>&nbsp;<strong>{{$order->created_at->toDayDateTimeString()}}</strong>
@if($order->request_origin)<i style="float: right;
background-color: wheat;
padding: 3px;
border-radius: 5px;
border: 1px solid gray;box-shadow: 2px 3px 4px -1px black;">{{$order->request_origin}}</i>@endif

<hr>
<div class="uk-grid">
    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
        <h4 class="heading_c uk-margin-small-bottom">Customer Info</h4>
        <ul class="md-list md-list-addon">
            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon uk-icon-user"></i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->name}}</span>
                    <span class="uk-text-small uk-text-muted">Name</span>
                </div>
            </li>
            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon material-icons">&#xE158;</i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->email}}</span>
                    <span class="uk-text-small uk-text-muted">Email</span>
                </div>
            </li>
            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->phone}}</span>
                    <span class="uk-text-small uk-text-muted">Phone</span>
                </div>
            </li>

            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon uk-icon-adress-card"></i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->address}}</span>
                    <span class="uk-text-small uk-text-muted">Address</span>
                </div>
            </li>
        </ul>
    </div>
    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">

    </div>

</div>
<hr>
<div class="uk-grid">
    <div class="uk-width-large-2-3">
        <p><strong>ORDER ITEMS</strong></p>
        <hr>
        <table style="width:100%" class="uk-table uk-table-nowrap uk-table-hover">
            <thead>
                <th style="text-align: center;width:5%"></th>
                <th style="width:80%">Item</th>
                <th style="text-align: center;width:15% ">Price</th>
            </thead>
            <tbody>
                @forelse ($order->foods->groupBy('id') as $food)
                <tr class="table-child-row">
                    <td style="width:5%">{{$food[0]['pivot']['count']}} x</td>
                    <td style="width:80%"> {{$food[0]['name']}}</td>
                    <td style="width:15%">{{$food[0]['price']*$food[0]['pivot']['count']}}</td>
                </tr>
                @empty @endforelse
                <tr>
                    <td style="background-color: aliceblue">EXTRAS</td>
                </tr>
                @forelse ($order->selectedOtions->groupBy('option') as $option)
                <tr>
                    <td>{{count($option)}}</td>
                    <td>{{$option[0]['option']}}</td>
                    <td>{{$option[0]['price']*count($option)}}</td>
                </tr>

                @empty @endforelse
                <tr style="background-color: burlywood">
                    <td colspan="2">Total</td>
                    <td>{{$order->total}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-large-1-3">
        <p><strong>STATUS TIMELINE</strong></p>
        <hr>
        <ul class="md-list" style="text-align: left">
            @forelse ($order->statuses as $status)

            <li>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$status->status}}</span>
                    <span class="uk-text-small uk-text-muted"> {{$status->created_at->toTimeString()}}</span>
                </div>
            </li>
            @empty @endforelse
        </ul>
    </div>

</div>
</div>