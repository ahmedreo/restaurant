<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Http\Resources\OrdersCollection;
use App\Restaurant;
use App\Food;
use App\Customer;
use DB;
use App\Jobs\SendEmail;
use App\Mail\OrderAccepted;
use App\Events\NewOrder;
use App\Events\AllNewOrders;
use App\Events\OrderStatus;
use App\Size;
use App\OptionItem;
use App\Coupon;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['store','getOrderStatus']);
        // $this->middleware('role:admin|owner')->only('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('foods', 'statuses', 'customer', 'restaurant')->paginate(10);
        return new OrdersCollection($orders);
    }
    public function restaurantOrders(Restaurant $restaurant)
    {
        $orders = Order::with('foods', 'statuses', 'customer', 'restaurant')
            ->where('restaurant_id', $restaurant->id)->paginate(10);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        info($request->all());
        // info($request);
        // dd('');
        $customer['name']=$request['name'];
        $customer['company']=$request['company'];
        $customer['phone']=$request['phone'];
        $customer['email']=$request['email'];
        $customer['address']=$request['address'];
        $customer['postalCode'] = (int)$request['postalCode'];
        $customer['city'] = $request['city'];
        $customer['address_components'] = json_encode($request['address_components']);
        $customer = Customer::create($customer);
        $orderReq['restaurant_id'] = $request['restaurant_id'];
        $orderReq['pre_order'] = $request['pre_order'];
        $orderReq['details'] = $request['details'];
        $orderReq['customer_id'] = $customer->id;
        $orderReq['code']=$orderReq['restaurant_id']. now()->timestamp;
        $orderReq['pre_order']=$orderReq['pre_order'];
        $origin = request()->header();
        info($origin['origin']['0']);
        $orderReq['request_origin']=$origin['origin']['0'];
        $order = Order::create($orderReq);
        $order->save();

        $foods = $request['foods'];
        $total = 0;
        $c = 0;
        foreach ($foods as $food) {
            $c++;
            $count = $food['count'];
            
                $total += $this->storeFood($food, $order);
        }
        // info('food: ' . $c);
        // info($total);
        $coupon = Coupon::find($request['coupon']);
        if($coupon && $coupon->restaurant_id == $request['restaurant_id'] && $coupon->isValid()){
           if($coupon->descount_type == 1)
            $total = $total -  ($coupon->descount /100 *$total);
            elseif($coupon->descount_type == 0)
            $total = $total - $coupon->descount;

            $order->coupon_id = $coupon->id;
        }
        $rest = Restaurant::find($request['restaurant_id']);
        if($rest->deliveryPrice > 0)
            $total += $rest->deliveryPrice;
        $order->total = $total;
        $order->status = 0;
        $order->save();
        $order->statuses()->create([
            'status' => 'created'
        ]);
        $order->load('customer', 'restaurant');

        event(new NewOrder($order));
        event(new AllNewOrders($order));
        SendEmail::dispatch($order)->delay(now()->addSeconds(3));

        $order->load('customer', 'restaurant', 'foods', 'selectedOptions','coupon');
        return response()->json($order);
    }
    public function storeFood($food, $order)
    {
        $price = 0;
        $selectedOptions = $food['items'];
        unset($food['items']);
       
        $restFood= Food::find($food['food_id']);
        $size = Size::find($food['size_id']);
        $price = $size->price;
        if($restFood->discount > 0)
            $price -= $price*$restFood->discount/100;
        $foodOrder = DB::table('food_order')->insertGetId([
            'food_id' => $food['food_id'],
            'count' => $food['count'],
            'order_id' => $order->id,
            'size'=>$size->name,
            'discount'=>$price,
            'price'=>$size->price,
            'comment'=>$food['comment']
        ]);
       
        info('food_count:' . $price);
        $c = 0;
        foreach ($selectedOptions[0] as $option) {
            $c++;
            info($option);
            $item = OptionItem::find($option);
            info('iteeeeeeeem');
            info($item);
            info($item[0]['name']);
            $price += $this->storeOption($item, $foodOrder);
            info('price after option' . $c . ': ' . $price);
        }
        

        return $price*$food['count'];
    }
    public function storeOption($item, $foodOrder)
    {
        DB::table('selected_options')->insert([
            'food_order_id' => $foodOrder,
            'option' => $item->name,
            'price' => $item->price,
        ]);
        return $item->price;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrfail($id);
        return $order;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function getOrderStatus($code)
    {
        $order = Order::with('restaurant' ,'statuses','foods','customer','selectedOptions','coupon')->where('code',$code)->first();
        info($order);
        if($order)
            return response()->json($order);
        else
            return response('Not Found',404);
    }
    public function changeStatus(Request $request,Order $order, $status)
    {
       
        if (!auth()->user()->hasRole('admin') && auth()->check() && auth()->user()->restaurant &&!auth()->user()->restaurant->id==$order->restaurant_id  ) {
            return view('errors.403');
        }
        switch ($status) {
           
            case 1:
                $order->status=1;
                $order->statuses()->create([
                    'status' => 'accepted and will be delivered in 20 mins',
                    'code' =>1,
                    'time'=>20
                ]);
                break;
            case 2:
                $order->status=1;
                $order->statuses()->create([
                    'status' => 'accepted and will be delivered in 40 mins',
                    'code' =>2,
                    'time'=>40
                ]);
                break;
            case 3:
                $order->status=1;
                $order->statuses()->create([
                    'status' => 'accepted and will be delivered in 60 mins',
                    'code' =>3,
                    'time'=>60
                ]);
                break;
            case 4:
                $order->status=2;
                $order->statuses()->create([
                    'status' => 'with the delivery',
                    'code' =>4,
                    'time'=>null
                ]);
                break;
            case 5:
                $order->status=3;
                $order->statuses()->create([
                    'status' => 'Delivered',
                    'code' =>5,
                    'time'=>null
                ]);
                break;
            case 6:
                $order->status=4;
                $order->statuses()->create([
                    'status' => 'Rejected',
                    'code' =>6,
                    'time'=>null
                ]);
                break;
        }
        $order->save();
        // \Mail::to($order->customer->email)->send(new OrderAccepted($order));
        event(new OrderStatus($order->load('statuses','coupon')));
        SendEmail::dispatch($order)->delay(now()->addSeconds(3));
        $controls = 1;
        if($request['history']==true)
            $controls =0;
        else {
            $controls = 1;
        }
        info($controls);
        $html = view('dashboard.orderDetails')->with(['order'=>$order,'controls'=>$controls])->render();
        return response()->json(['html'=>$html,'status'=>$order->status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::findOrfail($id)->delete();
    }
}
