<tr id="order-row-{{$order->id}}" class="order-details hierarchical_slide" data-order="{{$order->id}}" data-uk-modal="{target:'#modal_overflow'}" id="hierarchical-slide" data-show-delay="280">
        <td class="uk-text-center">{{$order->code?$order->code:$order->id }}</td>
        <td class="uk-text-center">{{$order->customer->name}}</td>
        <td class="uk-text-center">{{$order->customer->address}}</td>
        <td class="uk-text-center">
            <p id="order{{$order->id}}">
                <script>
                    var timer{{$order->id}} = new Timer();
                
                timer{{$order->id}}.start({precision: 'seconds', startValues: {seconds: {{$order->created_at->diffInSeconds(Carbon\Carbon::now())}}}});
                timer{{$order->id}}.addEventListener('secondsUpdated', function (e) {
                    var el = document.getElementById('order'+{{$order->id}})
                    if(el)
                        el.innerHTML =timer{{$order->id}}.getTimeValues().toString();
                });
                </script>
            </p>
        </td>
        @role('admin')
        <td class="uk-text-center">{{$order->restaurant->name}}</td>
        @endrole
        <td class="uk-text-center">
        @if ($order->status==0)
        <span class="uk-badge uk-badge-notification">New</span>
        @elseif($order->status==1)
        <span class="uk-badge uk-badge-warning uk-badge-notification">Ongoing</span>
        @elseif($order->status==2)
        <span class="uk-badge  uk-badge-success uk-badge-notification">With Delivery</span>
        @endif
        </td>
    </tr>