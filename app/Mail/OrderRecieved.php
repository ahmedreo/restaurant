<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;

class OrderRecieved extends Mailable
{
    use Queueable, SerializesModels;

    public  $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {   
        info('mail construct');
        info($order);
        $this->order = $order;
        info($this->order);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        info('recieved');
        info($this->order);
        return $this->markdown('emails.OrderRecieved');
    }
}
