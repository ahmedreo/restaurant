<table class="uk-table uk-table-nowrap table_check">
    <thead>
        <tr>
            <th class="uk-width-3-10 uk-text-left uk-width-small-1-2">Name</th>
            <th class="uk-width-1-10 uk-text-center">Copy</th>
            <th class="uk-width-1-10 uk-text-center">Group</th>
            <th class="uk-width-1-10 uk-text-center">Category</th>
            <th class="uk-width-2-10 uk-text-center">Actions</th>
        </tr>
    </thead>
    <tbody id="table-body">
        @forelse ($foods as $food)
        <tr id="food-{{$food->id}}">
            <td class="uk-text-left">{{$food->name}}</td>
        <td class="uk-text-center"><a class="md-btn md-btn-primary copy" href="{{url('/food/copy/'.$food->id)}}">Copy</a></td>
            <td class="uk-text-center">{{($food->group)? $food->group->name : ''}}</td>
            <td class="uk-text-center">{{$food->category->name}}</td>
            <td class="uk-text-center">
                <a href="{{url('food/'.$food->id.'/edit')}}"><i class="md-icon material-icons">&#xE254;</i></a>
                <a href="{{url('food/'.$food->id)}}" data-id="{{$food->id}}" class="delete"><i class="md-icon material-icons">delete</i></a>

            </td>
        </tr>
        @empty @endforelse
        <tr>

        </tr>
    </tbody>

</table>
@if($foods->lastPage() > 1)
<ul class="uk-pagination uk-margin-medium-top" >
    <li @if($foods->onFirstPage())class="uk-disabled" @endif><a href="{{$foods->previousPageUrl()}}"><i class="uk-icon-angle-double-left"></i></a></li>
    <li><a>{{$foods->currentPage()}}</a> of </li>
    <li><a>{{$foods->lastPage()}}</a></li>
<li @if($foods->lastPage() == $foods->currentPage())class="uk-disabled" @endif><a href="{{$foods->nextPageUrl()}}"><i class="uk-icon-angle-double-right"></i></a></li>
</ul>
@endif
