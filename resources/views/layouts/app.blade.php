@include('layouts.header')
<body class="sidebar_main_swipe header_full" id="body">
    {{-- navbar --}}
    @if(Auth::check())
    @include('layouts.nav')
    {{-- /navbar --}}
    {{-- sidebar --}}
    @include('layouts.side')
    @endif

    {{-- /sidebar --}}
    <div id="page_content">
        @yield('content')
        
    </div>
    @include('layouts.js')
</body>
</html>
