    // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;
        
            var pusher = new Pusher('9fb6518537e6c09bd608', {
              cluster: 'eu',
              forceTLS: true
            });
        
            var channel = pusher.subscribe('order.{{auth()->user()->restaurants()->first()->id}}');
            channel.bind('App\\Events\\NewOrder', function(data) {
              console.log(data.order);
              order = data.order
              html = `<tr  id="order-row-${order.id}" class="order-details" data-order="${order.id}" data-uk-modal="{target:'#modal_overflow'}">
                        <td class="uk-text-center">${order.id}</td>
                        <td class="uk-text-center">${order.customer['name']}</td>
                        <td class="uk-text-center">${order.customer.address}</td>
                        <td class="uk-text-center">
                            <p id="order${order.id}">Just Now </td>
                        <td class="uk-text-center">${order.restaurant.name}</td>
                        <td class="uk-text-center">
                        <span class="uk-badge uk-badge-notification">New</span>
                        </td>
                    </tr>`;
            $('#new-orders').prepend(html);
            $('#order-row-'+order.id).click();

            count = $('#todayCount').text();
            console.log(count);
            count++;
            console.log(count);
            $('#todayCount').text(count);
            var audio = new Audio("{{asset('sound/plucky.mp3')}}");

            audio.play();
            $('#new-order-notify').data('message',`<a href='#' class='notify-action order-details' data-uk-modal="{target:'#modal_overflow'}" data-order='${order.id}'>View</a> You have a new order`)
            .click()
            setTimeout(function(){ $('#order-row-'+order.id).addClass('new') }, 1000);

            });
