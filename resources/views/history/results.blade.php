<div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4 uk-margin-large-bottom hierarchical_show"
    data-uk-grid="{gutter: 20}" data-show-delay="280">

    @forelse ($orders as $order)

    <div class="uk-width-medium-1-3">
        <div class="md-card">
            <div class="md-card-toolbar">
                <div class="md-card-toolbar-actions">
                    <i class="md-icon material-icons md-card-fullscreen-activate toolbar_fixed">&#xE5D0;</i>

                    <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                        <i class="md-icon material-icons">&#xE5D4;</i>
                        <div class="uk-dropdown">
                            <ul class="uk-nav">
                                <li><a href="#">Action 1</a></li>
                                <li><a href="#">Action 2</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="md-card-toolbar-heading-text">
                    {{$order->id}}
                </h3>
                <p id="order{{$order->id}}">

                </p>
            </div>
            <div class="md-card-content">
                <ul class="md-list" style="text-align: left">
                    @forelse ($order->statuses as $status)

                    <li>
                        <div class="md-list-content">
                            <span class="md-list-heading">{{$status->status}}</span>
                            <span class="uk-text-small uk-text-muted"> {{$status->created_at->diffForHumans()}}</span>
                        </div>
                    </li>
                    @empty @endforelse
                </ul>
                <div class="md-card-fullscreen-content">
                    <div class="uk-grid">
                        <div class="uk-width-1-2 uk-width-1-3">
                            <div class="uk-width-large-1-2" style="background-color: antiquewhite;padding-top: 17px;">
                                <h4 class="heading_c uk-margin-small-bottom">Customer Info</h4>
                                <ul class="md-list md-list-addon">
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon uk-icon-user"></i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$order->customer->name}}</span>
                                            <span class="uk-text-small uk-text-muted">Name</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$order->customer->email}}</span>
                                            <span class="uk-text-small uk-text-muted">Email</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$order->customer->phone}}</span>
                                            <span class="uk-text-small uk-text-muted">Phone</span>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon uk-icon-adress-card"></i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{$order->customer->address}}</span>
                                            <span class="uk-text-small uk-text-muted">Address</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="uk-width-1-2 uk-width-1-3" style="background-color: #72c1c3;padding-top: 17px;padding-left: 0%">
                            <table style="width:100%" class="uk-table uk-table-nowrap uk-table-hover">
                                <thead>
                                    <th style="text-align: center;width:25%">Count</th>
                                    <th style="text-align: center;width:50%">Item</th>
                                    <th style="text-align: center;width:25%">Price</th>
                                </thead>
                                <tbody>
                                    @forelse ($order->foods->groupBy('id') as $food)
                                    <tr class="table-child-row">
                                        <td>{{$food[0]['pivot']['count']}}</td>
                                        <td> {{$food[0]['name']}}</td>
                                        <td>{{$food[0]['price']*$food[0]['pivot']['count']}}</td>
                                    </tr>
                                    @empty @endforelse
                                    <tr>
                                        <td style="background-color: aliceblue">EXTRAS</td>
                                    </tr>
                                    @forelse ($order->selectedOtions->groupBy('option') as $option)
                                    <tr>
                                        <td>{{count($option)}}</td>
                                        <td>{{$option[0]['option']}}</td>
                                        <td>{{$option[0]['price']*count($option)}}</td>
                                    </tr>

                                    @empty @endforelse
                                    <tr style="background-color: burlywood">
                                        <td colspan="2">Total</td>
                                        <td>{{$order->total}}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="uk-width-1-6">

                        </div>

                        <div class="uk-width-1-2 uk-width-1-6" style="background-color: #369630;padding-top: 17px; padding-left: 0px;">
                            <div class="md-fab-wrapper md-fab-in-card md-fab-speed-dial-horizontal " data-fab-hover>
                                <div class="md-fab-wrapper-small">
                                    <a class="md-fab md-fab-small md-fab-active change-status" href="{{url('changestatus/'.$order->id.'/1')}}"><span style="display:block;padding-top:13px">20 M</span></a>
                                    <a class="md-fab md-fab-small md-fab-active change-status" href="{{url('changestatus/'.$order->id.'/2')}}"><span style="display:block;padding-top:13px">40 M</span></a>
                                    <a class="md-fab md-fab-small md-fab-active change-status" href="{{url('changestatus/'.$order->id.'/3')}}"><span style="display:block;padding-top:13px">60 M</span></a>
                                </div>
                                <a class="md-fab md-fab-primary" href="javascript:void(0)"><i class="material-icons">check</i></a>
                            </div>
                            <div class="uk-column-1-1">
                                <a class="md-btn md-btn-primary md-btn-block md-btn-wave-light change-status" href="{{url('changestatus/'.$order->id.'/4')}}">On The Way</a>
                                <a class="md-btn md-btn-success md-btn-block md-btn-wave-light change-status" href="{{url('changestatus/'.$order->id.'/5')}}">Delivered</a>
                                <a class="md-btn md-btn-danger md-btn-block md-btn-wave-light change-status" href="{{url('changestatus/'.$order->id.'/6')}}">Reject</a>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @empty
    <h3 style="text-align: center">No results found</h3>
    @endforelse
</div>