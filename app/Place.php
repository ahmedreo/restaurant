<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable=['name','restaurant_id','minOrder','deliveryTime','deliveryPrice'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
}
