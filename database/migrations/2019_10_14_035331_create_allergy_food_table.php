<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllergyFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allergy_food', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('food_id')->nullable();
            $table->foreign('food_id')->references('id')->on('foods')->onUpdate('cascade');
            $table->unsignedInteger('allergy_id')->nullable();
            $table->foreign('allergy_id')->references('id')->on('allergies')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allergy_food');
    }
}
