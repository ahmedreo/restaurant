<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelectedOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selected_options', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('food_order_id')->references('id')->on('food_order')->onDelete('cascade');
            $table->unsignedInteger('food_order_id');
            $table->string('option');
            $table->string('comment')->nullable();
            $table->double('price',8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selected_options');
    }
}
