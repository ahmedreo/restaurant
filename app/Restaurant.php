<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = [
        'name', 'logo', 'addressCord', 'addressName', 'open', 'close', 'offDays',
        'phone','group_id','new',
        'suggested','preorder','cover','commission','manager','active','order','details'
    ];
    // public function getOffDaysAttribute($value)
    // {
    //     return unserialize($value);
    // }
    public function foods()
    {
        return $this->hasMany(Food::class);
    }
    // public function group()
    // {
    //     return $this->belongsTo(Group::class);
    // }
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function cats()
    {
        // return $this->foods()->category;
        // // $id = $this->id;
        // //  $cats = Category::whereHas('foods',function($q)use($id){
        // //     $q->where('restaurant_id',$id);
        // // })->get();
        // // return $cats;
    }
    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
    public function places()
    {
        return $this->hasMany(Place::class);
    }
    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }
    
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function calendar()
    {
        return $this->hasOne(WeeklyCalendar::class);
    }

    public function reviews()
    {
        return $this->hasManyThrough(Order::class, Review::class);
    }
}

