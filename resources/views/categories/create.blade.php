@extends('layouts.app') 
@section('content')
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">

<div id="page_content_inner">
    <h1>Create New Category</h1>
    <div class="md-card">
        <form action="{{url('category')}}" method="POST" role="form" class="uk-form-stacked" enctype="multipart/form-data">

            <div class="md-card-content large-padding">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>

                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="name">Name<span class="req">*</span></label>
                            <input type="text" name="name" data-parsley-trigger="change" required class="md-input" />
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="parsley-row">
                            <label for="details">Details<span class="req">*</span></label>
                            <input type="text" name="details" required class="md-input" />
                        </div>
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_a uk-margin-small-bottom">
                                    Picture
                                </h3>
                                <input type="file" name="picture" id="input-file-a" class="dropify" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <button type="submit" class="md-btn md-btn-primary">Save</button>
                    </div>
                </div>

            </div>
        </form>


    </div>
@endsection