<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name','description', 'icon'];

    public function restaurants()
    {
        return $this->morphedByMany('App\Restaurant', 'taggable');
    }
    public function foods()
    {
        return $this->morphedByMany('App\Foods', 'taggable');
    }
}
