@if(auth()->check())
<script>
 Pusher.logToConsole = true;
  
        var pusher = new Pusher("{{ env('PUSHER_APP_KEY') }}", {
          cluster: 'eu',
          forceTLS: true,
        });
        pusher.connection.bind('disconnected', function() {
           console.log('disconnectedss')
            pusher.connect();
});
        @role('admin')
        var channel = pusher.subscribe('order');
        var event = "App\\Events\\AllNewOrders";
        @endrole
        @hasanyrole('owner|driver')
        var channel = pusher.subscribe('order.{{auth()->user()->restaurant->id}}');
        var event = "App\\Events\\NewOrder";
        @endrole
        channel.bind(event, function(data) {
          order = data.order

            if (! ('Notification' in window)) {
              alert('Web Notification is not supported');
              return;
            }

            Notification.requestPermission( permission => {
              let notification = new Notification('New Order alert!', {
                body: 'From: '+order.customer.name, // content for the alert
                icon: "{{asset('/')}}"+order.restaurant.logo // optional image url
              });

              // link to page on clicking the notification
              notification.onclick = () => {
                window.open("{{url('ongoingorders/#')}}"+order.id);
              };
        });
        });
</script>
@endif
<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">
            <div class="main_logo_top">
                <a href="{{asset('/')}}"><img src="{{asset('admin/assets/img/logo_main_white@2x.png')}}" alt="" width="90" data-dense-cap="2" class="dense-image dense-ready"></a>
            </div>
            <!-- main sidebar switch -->
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>

            <!-- secondary sidebar switch -->
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>

            <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                    <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
                    <div class="uk-dropdown uk-dropdown-width-3">
                        <div class="uk-grid uk-dropdown-grid">
                            <div class="uk-width-2-3">
                                <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-bottom uk-text-center">
                                    <a href="page_mailbox.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-light-green-600">&#xE158;</i>
                                                <span class="uk-text-muted uk-display-block">Mailbox</span>
                                            </a>
                                    <a href="page_invoices.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-purple-600">&#xE53E;</i>
                                                <span class="uk-text-muted uk-display-block">Invoices</span>
                                            </a>
                                    <a href="page_chat.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-cyan-600">&#xE0B9;</i>
                                                <span class="uk-text-muted uk-display-block">Chat</span>
                                            </a>
                                    <a href="page_scrum_board.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-red-600">&#xE85C;</i>
                                                <span class="uk-text-muted uk-display-block">Scrum Board</span>
                                            </a>
                                    <a href="page_snippets.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-blue-600">&#xE86F;</i>
                                                <span class="uk-text-muted uk-display-block">Snippets</span>
                                            </a>
                                    <a href="page_user_profile.html" class="uk-margin-top">
                                                <i class="material-icons md-36 md-color-orange-600">&#xE87C;</i>
                                                <span class="uk-text-muted uk-display-block">User profile</span>
                                            </a>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <ul class="uk-nav uk-nav-dropdown uk-panel">
                                    <li class="uk-nav-header">Components</li>
                                    <li><a href="components_accordion.html">Accordions</a></li>
                                    <li><a href="components_buttons.html">Buttons</a></li>
                                    <li><a href="components_notifications.html">Notifications</a></li>
                                    <li><a href="components_sortable.html">Sortable</a></li>
                                    <li><a href="components_tabs.html">Tabs</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">fullscreen</i></a></li>
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>
                        <div class="uk-dropdown uk-dropdown-xlarge">
                            <div class="md-card-content">
                                <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                    <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                </ul>
                                <ul id="header_alerts" class="uk-switcher uk-margin">
                                    <li>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <span class="md-user-letters md-bg-cyan">gk</span>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="page_mailbox.html">Ea omnis.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Et dolorum cum et excepturi enim aut aspernatur mollitia.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="admin/assets/img/avatars/avatar_07_tn.png" alt="" />
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="page_mailbox.html">Beatae iste.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Non sed iusto voluptatem vel ab inventore odio possimus dignissimos quo.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <span class="md-user-letters md-bg-light-green">cn</span>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="page_mailbox.html">Libero recusandae saepe.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Deleniti debitis voluptatem repellat inventore qui.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="admin/assets/img/avatars/avatar_02_tn.png" alt="" />
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="page_mailbox.html">Nostrum dolorem quia.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Possimus aut quaerat dolor soluta ab.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="admin/assets/img/avatars/avatar_09_tn.png" alt="" />
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="page_mailbox.html">Est est.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Eum eligendi non rerum dolore optio.</span>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                            <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                        </div>
                                    </li>
                                    <li>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">In molestiae qui.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Aut dolor consequatur eum aut quos nihil.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Possimus iure.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Dolorem voluptas ut officiis labore repellat.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Dignissimos est.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Qui harum quod ut et asperiores dolores.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Nam pariatur ipsam.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Quia officiis fugiat libero illum illum.</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_image">
                        @if(auth()->user()->restaurant_id && auth()->user()->restaurant->logo)
                        <img class="md-user-image" src="{{asset(auth()->user()->restaurant->logo)}}" alt=""/>
                        @else
                        <img class="md-user-image" src="{{asset('images/avatar.png')}}" alt=""/>
                        @endif
                        </a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav js-uk-prevent">
                            <li><a href="{{url('user/'.auth()->user()->id.'/edit')}}">My profile</a></li>
                                <li>
                                    <form method="POST" action="{{url('logout')}}">
                                        @csrf
                                        <button style="background:none; border:none; font:500 14px/25px Roboto,sans-serif;color: #212121;
                                                padding: 8px 20px;display: block;overflow: hidden;cursor:pointer">Log out</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</header>
<!-- main header end -->