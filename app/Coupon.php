<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Coupon extends Model
{
    protected $fillable=['code', 'count', 'descount_type', 'descount', 'restaurant_id', 'start',
    'end'];

    protected $dates = [
        'start','end'
    ];
    protected $casts = [
        'start' => 'datetime:Y-m-d',
        'end' => 'datetime:Y-m-d',
    ];
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function isValid()
    {
        info(count($this->orders));
        info($this->start <= Carbon::today());
        info($this->end >= Carbon::today());
        return count($this->orders) < $this->count && $this->start <= Carbon::today() && $this->end >= Carbon::today();
    }
}
