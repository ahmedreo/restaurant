<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    use RegistersUsers;
    protected function guard()
    {
        return Auth::guard('api');
    }

    function create(Request $request)
    {
        /**
         * Get a validator for an incoming registration request.
         *
         * @param  array  $request
         * @return \Illuminate\Contracts\Validation\Validator
         */
        info('create');
        $valid = validator($request->only('email', 'name', 'password', 'mobile'), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'mobile' => 'required',
        ]);

        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors()->all(), 400);
            return \Response::json(['success'=>false,'errors'=>$valid->errors()->all()]);
        }
        info($request->all());
        $data = request()->only('email', 'name', 'password', 'mobile');
        $hashed = Hash::make($data['password']);
        info($hashed);
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'mobile' => $data['mobile']
        ]);
        $customer = Customer::updateOrCreate(
            ['email' => $data['email']],
            ['user_id' => $user->id]
        );
        $role = Role::where('name','customer')->first();
        info($role);
        info('api');
        info(Auth::guard('api')->check());
        info('web');
        info(Auth::guard('web')->check());
        $user->assignRole($role);
        $accessToken = $user->createToken('token')->accessToken;
        return response(['user'=>$user,'token'=>$accessToken,'success'=>true]);
    }

    public function login(Request $request)
    {
        $valid = $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        
        info($valid);
       
        info(auth()->attempt($valid));
        if(!auth()->attempt($valid)){
            return response(['message'=>'invalid credentials','success'=>false]);
        }
        $user = auth()->user()->load('customer.orders');
       
        $accessToken = $user->createToken('token')->accessToken;
        return response(['user'=>$user,'token'=>$accessToken,'success'=>true]);
    }
 
}
