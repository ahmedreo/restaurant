<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Order;
use App\Report;
use App\Restaurant;
use PDF;
class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['name']= $request['restaurant_id']. now()->timestamp;
        $request['from'] = date("Y-m-d H:i:s", strtotime(str_replace('.', '-', $request['start'])));
        $request['to'] = date("Y-m-d H:i:s", strtotime(str_replace('.', '-', $request['end'])));
        $order = Order::query();
        $order->where('restaurant_id',$request['restaurant_id']);
        $request['count'] = $order->whereDate('created_at', '>=', $request['from'])->whereDate('created_at', '<=', $request['to'])->whereIn('status',[3])->count();
        $request['sum'] = $order->whereDate('created_at', '>=',$request['from'])->whereDate('created_at', '<=', $request['to'])->where('status',3)->sum('total');
        info($request->all());
        $report = Report::create($request->all());
        return response()->json($report->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::find($id);
        if(!auth()->user()->hasRole('admin') && !auth()->user()->restaurant->id == $report->restaurant->id )
            return view('errors.403');
        
        
        $orders = Order::whereDate('created_at', '>=', $report->from)->whereDate('created_at', '<=', $report->to)->whereIn('status',[3,4])->get();
        // update report count and sum
        $report->count = Order::whereDate('created_at', '>=', $report->from)->whereDate('created_at', '<=', $report->to)->whereIn('status',[3])->count();
        $report->sum = Order::whereDate('created_at', '>=',$report->from)->whereDate('created_at', '<=', $report->to)->where('status',3)->sum('total');
        $report->save();
        
        $data =['restaurant'=>$report->restaurant,'report'=>$report,'orders'=>$orders];
       $html = view('history.report')->with(['restaurant'=>$report->restaurant,'report'=>$report,'orders'=>$orders]);
       $pdf = PDF::loadView('history.report', $data);
       return $pdf->stream('document.pdf');

        return view('history.report')->with(['restaurant'=>$report->restaurant,'report'=>$report,'orders'=>$orders]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::findOrfail($id);
        $report->delete();
    }
}
