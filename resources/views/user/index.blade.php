@extends('layouts.app') 
@section('content')
<div id="page_content_inner">
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Create New User" data-uk-modal="{target:'#modal_create_user'}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
    <br>
    <div class="uk-modal" id="modal_create_user">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Create New User <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{url('user')}}" id="create_form" method="POST">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label>Name</label>
                        <input type="text" name="name" id="name" class="md-input" required/>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <label>Email</label>
                        <input type="text" name="email" id="email" class="md-input" required />
                    </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label>Password</label>
                        <input type="password" name="password" id="password" class="md-input" required />
                    </div>
                    <div class="uk-width-medium-1-2">
                        <label>Confirm Password</label>
                        <input type="password" name="password_confirmation" id="passowrd_confirm" class="md-input" required />
                    </div>
                </div>
                <br>
                <div class="uk-width-medium-3-5">
                    @forelse ($roles as $role)
                    <span class="icheck-inline">
                                    <input type="radio" name="role_id" id="{{$role->id}}" required value="{{$role->id}}" data-md-icheck />
                                        <label for="{{$role->id}}" class="inline-label">{{$role->name}}</label>
                                    </span> @empty @endforelse
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-nowrap table_check">
                    <thead>
                        <tr>
                            <th class="uk-width-2-10">User Name</th>
                            <th class="uk-width-2-10 uk-text-center">Email</th>
                            <th class="uk-width-1-10 uk-text-center">Role</th>
                            <th class="uk-width-1-10 uk-text-center">Restaurant</th>
                            <th class="uk-width-2-10 uk-text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        @forelse ($users as $user)
                        <tr id="user-{{$user->id}}">
                            <td>{{$user->name}}</td>
                            <td class="uk-text-center">{{$user->email}}</td>
                            <td class="uk-text-center">@forelse ($user->roles as $role) {{$role->name}} @empty @endforelse

                            </td>
                            <td class="uk-text-center">
                                <span class="uk-badge">{{$user->restaurant?$user->restaurant->name:null}}</span></td>
                            <td class="uk-text-center">
                                <a href="{{url('user/'.$user->id.'/edit')}}"><i class="md-icon material-icons">&#xE254;</i></a>
                                <a href="{{url('user/'.$user->id)}}" data-id="{{$user->id}}" class="delete"><i class="md-icon material-icons">delete</i></a>

                            </td>
                        </tr>
                        @empty @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<button class="md-btn" style="display:none;text-align:center;" id="danger" data-message="" data-status="danger" data-pos="bottom-center">Danger</button>
<button class="md-btn" style="display:none;text-align:center;" id="success" data-message="" data-status="success" data-pos="bottom-center">Success</button>

<script>
    document.addEventListener("DOMContentLoaded",function(){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    $('#create_form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    html = `<tr>
                                <td>`+res.name+`</td>
                            <td class="uk-text-center">`+res.email+`</td>
                                <td class="uk-text-center">`+res.roles[0].name+`
                                <td class="uk-text-center"><span class="uk-badge">
                                    </span></td>
                                <td class="uk-text-center">
                                    <a href="{{url('user/')}}/`+res.id+`/edit"><i class="md-icon material-icons">&#xE254;</i></a>
                                    <a href="{{url('user/')}}/`+res.id+`" data-id="`+res.id+`"  class="delete"><i class="md-icon material-icons">delete</i></a>
                                </td>
                            </tr>`
                    $('#table-body').prepend(html);
                    $('#create_form')[0].reset();

                    var modal = UIkit.modal("#modal_create_user");
                    modal.hide()
                    $('#success').data('message',"@lang('messages.UserAdded')").click();
                    
                // $('#modal_create_user').toggle();
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors);
                errors = error.responseJSON.errors
                for (var key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        console.log(key + " -> " + errors[key]);
                    $('#danger').data('message',errors[key]).click();
                        
                    }
                }
            }
        });
    });
});

</script>
@endsection
 
@section('pagejs')
<script>
    $('.delete').on('click',function(e){
    e.preventDefault()
    conf = confirm("Are You Sure?");
    if(!conf)
        return;
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                $('#user-'+id).hide(100);
            }
        }
    })
});
</script>
<script src="{{asset('admin/assets/js/pages/components_notifications.min.js')}}"></script>

@endsection