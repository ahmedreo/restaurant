@component('mail::message') 
# We Are Cooking Your Order 

@php
    $status = $order->statuses()->orderBy('created_at','DESC')->first();
@endphp

    Thank YOU For Ordering From {{$order->restaurant->name}}, Your Order We Be Delieverd In {{$status->status}}
 <br>
    Thanks,

<br> {{ config('app.name') }} @endcomponent