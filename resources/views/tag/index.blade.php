@extends('layouts.app')
@section('content')
<div id="page_content_inner">
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent md-fab-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Add Tag" id="create" data-uk-modal="{target:'#modal_create_Tag'}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
    <br>
    <div class="md-card uk-margin-medium-bottom">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-nowrap table_check">
                    <thead>
                        <tr>
                            <th class="uk-width-2-10 uk-text-center">Name</th>
                            <th class="uk-width-2-10 uk-text-center">Description</th>
                            <th class="uk-width-2-10 uk-text-center">Icon</th>
                            <th class="uk-width-2-10 uk-text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        @forelse ($allergies as $tag)
                        <tr id="tag-{{$tag->id}}">
                            <td class="uk-text-center name">{{$tag->name}}</td>
                            <td class="uk-text-center description">{{$tag->description}}</td>
                            <td class="uk-text-center icon">{{$tag->icon}}</td>
                            <td class="uk-text-center">
                                <a href="{{url('tag/'.$tag->id)}}" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Edit Tag" data-uk-modal="{target:'#modal_Edit_Tag'}" class="edit" data-info="{{$tag->toJson()}}"><i
                                        class="md-icon material-icons">&#xE254;</i></a>
                                <a href="{{url('tag/'.$tag->id)}}" data-id="{{$tag->id}}" class="delete"><i
                                        class="md-icon material-icons">delete</i></a>

                            </td>
                        </tr>
                        @empty @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="uk-modal" id="modal_create_Tag">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Create New Tag <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{url('tag')}}" id="create_form" method="POST">
                @csrf
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label>Name</label>
                        <input type="text" name="name" id="name" class="md-input" required/>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <label>Description</label>
                        <input type="text" name="description" id="description" class="md-input" />
                    </div>
                    <div class="uk-width-medium-1-2">
                        <label>Icon</label>
                        <input type="text" name="icon" id="icon" class="md-input" required/>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="uk-modal" id="modal_Edit_Tag">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Tag <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">person_add</i></h3>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="{{url('tag')}}" id="edit_form" method="POST">
            @csrf
            @method('PUT')
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <label>Name</label>
                    <input type="text" name="name" id="edit-name" class="md-input" required/>
                </div>
                <div class="uk-width-medium-1-2">
                    <label>Description</label>
                    <input type="text" name="description" id="edit-description" class="md-input" />
                </div>
                <div class="uk-width-medium-1-2">
                    <label>Icon</label>
                    <input type="text" name="icon" id="edit-icon" class="md-input" required/>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
            </div>
        </form>
      
<script>
    document.addEventListener("DOMContentLoaded",function(){
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        $(document).on('click','a.edit',function(e){
            e.preventDefault()
            console.log($(this).attr('href'));
            console.log($(this).data('info'));
            $('#edit_form').attr('action',$(this).attr('href'));
            console.log($('#edit_form'));
            info = $(this).data('info')
            $('#edit_form :input').each(function(){
                var key = $(this).attr('name');
                if(info[key])
                $(this).val(info[key]).text(info[key]).focus();
            });
        });
$('#edit_form').on('submit',function (e) {
e.preventDefault();
url = $(this).attr('action');
type = $(this).attr('method');
data = $(this).serializeArray();
console.log(data);
console.log(url);

$.ajax({
        url:url,
        type:type,
        data:data,
        success:function(res){
            console.log(res);
            $.each(res,function(k,v) {
                if($('#tag-'+res.id+' td.'+k))
                    $('#tag-'+res.id+' td.'+k).text(v);
            });
            UIkit.modal("#modal_Edit_Tag").hide();
           
        }
})
})
    $('#create_form').on('submit',function(e){
        e.preventDefault();
        url = $(this).attr('action');
        type = $(this).attr('method');
        fdata = $(this).serializeArray();
        console.log(fdata,type,url)
        $.ajax({
            url:url,
            type:type,
            data:fdata,
            success:function(res){
                console.log(res);
                if(res){
                    html = `<tr>
                        <td class="uk-text-center name">`+res.name+`</td>
                                <td class="uk-text-center description">`+res.description+`</td>
                                <td class="uk-text-center icon">`+res.icon+`</td>
                                
                               
                                <td class="uk-text-center">
                                    <a href="{{url('tag/')}}/${res.id}" class="edit" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Edit Tag" data-uk-modal="{target:'#modal_Edit_Tag'}" class="edit" data-info="${res}"><i class="md-icon material-icons">&#xE254;</i></a>
                                    <a href="{{url('tag/')}}/${res.id}" data-id="${res.id}" class="delete"><i
                                        class="md-icon material-icons">delete</i></a>
                                </td>
                            </tr>`
                    $('#table-body').prepend(html);
                    $('#create_form')[0].reset();

                    var modal = UIkit.modal("#modal_create_Tag");
                    modal.hide()
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $(document).on('click','.delete',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res){
                $('#tag-'+id).hide(100);
            }
        }
    })
});
})

</script>
@endsection