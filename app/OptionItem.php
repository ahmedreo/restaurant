<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionItem extends Model
{
    protected $fillable=['food_option_id', 'name', 'price'];

    public function foodOption()
    {
        return $this->belongsTo(FoodOption::class, 'food_option_id');
    }
}
