<?php

use App\Order;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route('ongoingorders');
});
Route::get('login',function(){
    
    if(auth()->check())
        return redirect()->route('ongoingorders');
});
Route::get('register',function(){
    if(auth()->check())
        return redirect()->route('ongoingorders');
    else {
        return redirect()->route('login');
    }
});

Auth::routes();
Route::middleware(['auth'])->group(function() {
// Route::get('/home', 'HomeController@index')->name('home');

// users 
Route::resource('user','UsersController');
Route::get('addUserRestaurant/{user}/{restId}','UsersController@addToRestaurant');
Route::get('changeRoles/{user}/{roleId}','UsersController@changeRoles')->middleware('role:admin');
// Category
Route::resource('category','CategoriesController')->middleware('role:admin');
//Group
Route::resource('groups','GroupsController')->middleware('role:admin');
Route::get('restfoods/{rest}','FoodsController@restFoods');
//Allergies
Route::resource('allergy','AllergiesController')->middleware('role:admin');
//Tags
Route::resource('tag','TagsController')->middleware('role:admin');

// restaurant
Route::resource('restaurant','RestaurantController')->middleware('role:admin');
Route::post('rest/{rest}/logo','RestaurantController@updateLogo')->middleware('role:admin');
// restaurant ordering
Route::post('rests/changeorder','RestaurantController@changeOrder')->middleware('role:admin');
// restaurant settings
Route::post('restaurant/{rest}/changesettings','RestaurantController@changeSettings')->middleware('role:admin');
// restaurant calendar 
Route::post('restaurant/{rest}/calendar','RestaurantController@setCalendar');
Route::get('restaurant/{rest}/calendar','RestaurantController@getCalendar');
//food
Route::resource('food','FoodsController');
//food copy
Route::get('food/copy/{food}','FoodsController@FoodCopy')->middleware('role:admin');
//food ordering
Route::post('food/{rest}/changeorder','FoodsController@changeOrder');
//change passsword
Route::post('changepassword/{user}','UsersController@changePassword');
//update restaurant's users

Route::post('restaurantusers/{rest}',"RestaurantController@updateUsers");
Route::get('restaurantfoods/{rest}','FoodsController@restaurantFoods');

//add food option

Route::post('addsize/{food}','FoodsController@createFoodSize');
Route::post('editsize/{size}','FoodsController@updateSize');
Route::delete('deletesize/{size}','FoodsController@deleteSize');
Route::post('addoption/{food}','FoodsController@createFoodOption');
Route::get('edit/option/{option}','FoodsController@editFoodOption');
Route::get('delete/option/{option}','FoodsController@deleteFoodOption');
Route::get('addoptionitem/{option}','FoodsController@createOptionList');
Route::get('deleteitem/{item}','FoodsController@deleteOptionList');
Route::post('copy-size/{size}','FoodsController@copySize');
// dashboard
Route::get('ongoingorders','DashboardController@index')->name('ongoingorders')->middleware('role:owner|admin');
Route::get('dashboard','DashboardController@Dashboard');
Route::get('orderdetails/{order}','DashboardController@orderDetails');
// order status

Route::get('changestatus/{order}/{status}','OrdersController@changeStatus');
Route::get('eee',function(){
    return view('errors.403');
});

// History
Route::get('history','DashboardController@history');
Route::get('historyfilter','DashboardController@historyfilter');
Route::get('report','ReportsController@store');
Route::get('report/{id}','ReportsController@show')->middleware('role:owner|admin');
Route::get('report-delete/{id}','ReportsController@destroy')->middleware('role:admin');
//Print order
Route::get('print/{order}',function(Order $order){
        $data=['order'=>$order];
        // return view('dashboard.print')->with('order',$order);
       $pdf = Pdf::loadView('dashboard.print', $data);
       return $pdf->stream('document.pdf');
});
Route::resource('coupon','CouponsController');
Route::get('rest-coupons/{rest}','CouponsController@restCoupons');

});