<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Restaurant;
use App\Category;
class GroupsController extends Controller
{
    public function __construct()
    {
        info(request()->getHttpHost());

        // $this->middleware('auth');
        // $this->middleware('role:admin|owner')->only(['index','orderDetails']);
        // $this->middleware('role:driver')->only(['index']);
    }

    public function index()
    {
        $groups = Group::all();
        $groups->load('groups','restaurants');
        return view('groups.index')->with('groups',$groups);
    }

    public function store(Request $request)
    {
        $group = Group::create(['name'=>$request['name']]);
        return response()->json(['group'=>$group]);
    }
    
    public function update(Request $request,Group $group)
    {
        $group->name = $request['name'];
        $group->save();
        info($request['restaurant_id']);
        $rests = $group->restaurants;
        $groups = $group->groups;

        foreach ($groups as $g) {
            info($g);
            $g->group_id = null;
            $g->save();
        }
        if($request['group_id']){
            foreach ($request['group_id'] as $group_id) {
               $rest = group::find($group_id);
               $rest->update(['group_id'=>$group->id]);
            }}
        foreach ($rests as $rest) {
            info($rest);
            $rest->group_id = null;
            $rest->save();
        }
        if($request['restaurant_id']){
        foreach ($request['restaurant_id'] as $rest_id) {
           $rest = Restaurant::find($rest_id);
           $rest->update(['group_id'=>$group->id]);
        }}

        $group->load('groups','restaurants');
        return response()->json(['success'=>true,'group'=>$group,
        'rest'=>$group->restaurants->pluck('id'),
        'resnames'=>$group->restaurants->pluck('name'),
        'groups'=>$group->groups->pluck('id'),
        'groupnames'=>$group->groups->pluck('name')]
    );
    }
    public function getRestByGroup(Group $group,$place)
    {
        info('api request');
        info(request()->getHttpHost());
        $origin = request()->header();
        // info($origin['origin']['0']);
        info('end');
        $restaurants = Restaurant::whereHas('group.foods')->where('group_id',$group->id)->whereHas('places',function($q)use($place){
            $q->where('name',$place);
        })->get();
        $res['cat'] = $group->load('foods.category');
        $restaurants->load('places','group.foods.category');
        foreach ($restaurants as $rest) {
            $rest['offDays'] = $this->offDaysCast(unserialize($rest->offDays));
            $cat = Category::select('id', 'name', 'picture', 'details')->whereHas('foods', function ($q) use ($rest) {
                $q->where('group_id', $rest->group->id);
            })->get();
            $rest['cats'] = $cat;
        }
        $subgs = $group->groups;
        foreach ($subgs as $subg ) {
            $subrestaurants = Restaurant::whereHas('group.foods')->where('group_id',$subg->id)->whereHas('places',function($q)use($place){
                $q->where('name',$place);
            })->get();
            info($subrestaurants);
        $subrestaurants->load('places','group.foods.category');
        foreach ($subrestaurants as $rest) {
            $rest['offDays'] = $this->offDaysCast(unserialize($rest->offDays));
            $cat = Category::select('id', 'name', 'picture', 'details')->whereHas('foods', function ($q) use ($rest) {
                $q->where('group_id', $rest->group->id);
            })->get();
            $rest['cats'] = $cat;
        }
            $restaurants->push($subrestaurants);
        }
        

        return response()->json([$restaurants]);
    }
    public function offDaysCast($values)
    {
        info('cast');
        $result = [];
        $l = count($values);
        // info($l);
        $l--;
        foreach ($values as $key => $value) {

                array_push($result,$value);

        }
        // $result .= '"';
        info($result);
        return $result;
    }
    public function getFoodByRes(Restaurant $rest)
    {
        $rest->load('group.foods.sizes.options.items','group.foods.category');
        $rest['offDays'] = $this->offDaysCast(unserialize($rest->offDays));

        return response()->json($rest);
    }
    public function destroy(Group $group)
    {
        $group->delete();
        return response()->json(['success'=>true]);        
    }
}
