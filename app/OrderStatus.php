<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $fillable = ['order_id', 'user_id', 'status','code'];
    protected $table = 'order_status';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
