<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WeeklyCalendar extends Model
{
    protected $table = 'weekly_calendar';
    protected $fillable = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'restaurant_id'];

    protected $casts = [
        'monday' => 'array',
        'tuesday' => 'array',
        'wednesday' => 'array',
        'thursday' => 'array',
        'friday' => 'array',
        'saturday' => 'array',
        'sunday' => 'array',
    ];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function working()
    {
        $today = strtolower(Carbon::today()->englishDayOfWeek);

        // info([$this->$today['openAt'],$this->$tomorrow,$this->$afterTomorrow]);
        $isOpen = $this->isOpenNow($today);
        $days = $this->getDays();
        $working = ['isOpen' => $isOpen, 'days' => $days];
        info($working);
        return $working;
    }

    public function isOpenNow($today)
    {
        if ((isset($this->$today['closed']) && $this->$today['closed']==true)||(in_array($this->$today['openAt'], [null, '00:00']) && in_array($this->$today['closeAt'], [null, '00:00']))) {
            return false;
        } else {
            $thisHour = Carbon::now()->hour . ":" . Carbon::now()->minute;

            if (strtotime($thisHour) < strtotime($this->$today['openAt']) || strtotime($thisHour) >= strtotime($this->$today['closeAt'])) {
                return false;
            }
            return true;
        }
    }
    public function isOpenThisDay($day)
    {
        if ($day) {
            if ((isset($this->$day['closed']) && $this->$day['closed']==true)||(in_array($this->$day['openAt'], [null, '00:00']) && in_array($this->$day['closeAt'], [null, '00:00']))) {
                return false;
            } else {
                return true;
            }
        }else{
            return false;
        }
    }

    public function getDays()
    {
        $days = [];
        $i = 1;
        info('getDays');
        $d = strtolower(Carbon::today()->englishDayOfWeek);
        $days[0]['today'] =  $this->$d;
        $x = 1;
        info(count($days));
        for ($i = 1; count($days) < 3 && $i <= 7; $i++) {
            if ($this->isOpenThisDay(strtolower(Carbon::today()->addDays($i)->englishDayOfWeek))) {
                $d = strtolower(Carbon::today()->addDays($i)->englishDayOfWeek);
                $days[$x][$d] = $this->$d;
                $x++;
            }
        }
        // array_shift($days,['today'=>strtolower(Carbon::today()->englishDayOfWeek)]);
        return $days;
    }
}
