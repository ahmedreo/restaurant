<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=['restaurant_id','customer_id','total'
    ,'details','code','pre_order','request_origin'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function foods()
    {
        return $this->belongsToMany(Food::class)->using(FoodOrder::class)->withPivot('count','id','size','discount','price','comment');
    }

    public function selectedOptions()
    {
        return $this->hasManyThrough(
            SelectedFoodOption::class,
            FoodOrder::class,
            'order_id',
            'food_order_id',
            'id',
            'id'
        );
    }
    
    public function statuses()
    {
        return $this->hasMany(OrderStatus::class);
    }
    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

}
