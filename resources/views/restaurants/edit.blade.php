@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{asset('admin/assets/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" href="{{asset('admin/bower_components/select2/dist/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/assets/choices/choices.min.css?version=4.1.3')}}">
<style>
    .dropify-wrapper .dropify-preview .dropify-render img {
        top: 30%;
    }

    .user_heading .bg_overlay:before {
        display: block;
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0, 0, 0, .3);
    }
</style>
<div id="page_content_inner">
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-large-2-3">
            <div class="md-card">
                <div class="user_heading " style="background-image: url('{{url($rest->logo)}}');background-repeat: no-repeat;
                            background-position: center center;">
                    <div class="bg_overlay">
                        <div class="user_heading_menu hidden-print">
                            <div class="uk-display-inline-block" data-uk-dropdown="{pos:'left-top'}">
                                <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="#">Action 1</a></li>
                                        <li><a href="#">Action 2</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="uk-display-inline-block"><i class="md-icon md-icon-light material-icons"
                                    id="page_print">&#xe251;</i></div>
                        </div>
                        <div class="user_heading_avatar fileinput fileinput-new" style="z-index: 10"
                            data-provides="fileinput">
                            <div class="fileinput-new thumbnail xxx">
                                <img src="{{url($rest->logo)}}" alt="{{$rest->name}}" id="logo" />
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                            <div class="user_avatar_controls">
                                <span class="btn-file">
                                    <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                    <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                    <input type="file" name="logo" id="logo-input">
                                </span>
                                <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i
                                        class="material-icons">&#xE5CD;</i></a>
                                <a href="#" class="btn-file fileinput-upload"
                                    style="top:65px;right: 23px;display: none; "><i class="material-icons"
                                        style="color: forestgreen;">&#xe2c6;</i></a>
                            </div>
                        </div>
                        <div class="user_heading_content" style="position: relative">
                            <h2 class="heading_b uk-margin-bottom"><span
                                    class="uk-text-truncate">{{$rest->name}}</span><span
                                    class="sub-heading">{{$rest->details}}</span></h2>
                            <ul class="user_stats">
                                <li>
                                    <h4 class="heading_a">{{$rest->orders->count()}} <span
                                            class="sub-heading">Orders</span></h4>
                                </li>
                                <li>
                                    <h4 class="heading_a">{{count($categories)}} <span
                                            class="sub-heading">Categories</span></h4>
                                </li>
                                <li>
                                    <h4 class="heading_a">{{$rest->foods->count()}} <span
                                            class="sub-heading">Foods</span></h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="uk-width-large-1-3 uk-width-medium-1-2">
            <div class="md-card">
                <div class="md-card-content">
                    <ul class="md-list">
                        <li>
                            <div class="md-list-content">
                                <div class="uk-float-right">
                                    <input type="checkbox" data-switchery value="1" class="settings" @if($rest->active)
                                    checked @endif id="active" name="active" />
                                </div>
                                <span class="md-list-heading">Active <i class="uk-icon-lightbulb-o uk-icon-hover"></i></span>
                                <span class="uk-text-muted uk-text-small">Check if this restaurant is Active</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <div class="uk-float-right">
                                    <input type="checkbox" data-switchery value="1" class="settings"
                                        @if($rest->preorder) checked @endif id="preorder" name="preorder" />
                                </div>
                                <span class="md-list-heading">PreOrder <i class="material-icons" title="PreOrder">
                                        alarm_add
                                    </i></span>
                                <span class="uk-text-muted uk-text-small">Can user pre order?</span>
                            </div>
                        </li>
                        <li>
                           
                            <div class="md-list-content">
                                <div class="uk-float-right">
                                    <input type="checkbox" data-switchery value="1" class="settings"
                                        @if($rest->suggested) checked @endif id="suggested" name="suggested" />
                                </div>
                                <span class="md-list-heading">Suggested <i class="material-icons" data-uk-tooltip="Suggested">
                                        thumb_up
                                    </i></span>
                                <span class="uk-text-muted uk-text-small">Mark as suggested</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-content">
                                <div class="uk-float-right">
                                    <input type="checkbox" data-switchery value="1" class="settings" @if($rest->new)
                                    checked @endif id="new" name="new" />
                                </div>
                                <span class="md-list-heading">New<i class="material-icons" title="New Restaurant">
                                        fiber_new
                                    </i></span>
                                <span class="uk-text-muted uk-text-small">Mark as new</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="uk-width-medium-1-3">

            <div class="md-card">
                <form action="{{url('restaurant/'.$rest->id)}}" name="restaurant" method="POST" role="form" class="uk-form-stacked"
                    enctype="multipart/form-data">

                    <div class="md-card-content large-padding">
                        @csrf @method('put')
                        {{-- <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="md-card">
                                    <div class="md-card-content">
                                        <h3 class="heading_a uk-margin-small-bottom">
                                            Logo
                                        </h3>
                                        <input type="file" name="logo" data-default-file="{{asset($rest->logo)}}"
                        id="input-file-a" class="dropify" />
                    </div>
            </div>
        </div>
    </div> --}}
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
            <div class="parsley-row  uk-form-row">
                <label for="tags" class="uk-form-label">Tags<span class="req">*</span></label>
                <select id="tags" name="tags[]" style="width:100%" multiple data-md-select2>
                    @forelse ($tags as $tag)
                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>
    </div>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="name">Name<span class="req">*</span></label>
                <input type="text" value="{{$rest->name}}" name="name" data-parsley-trigger="change" required
                    class="md-input" />
            </div>
        </div>
        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="addressCord">addressCord<span class="req">*</span></label>
                <input type="text" value="{{$rest->addressCord}}" name="addressCord" required class="md-input" />
            </div>
        </div>

    </div>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="addressName">addressName<span class="req">*</span></label>
                <input type="text" value="{{$rest->addressName}}" name="addressName" required class="md-input" />
            </div>
        </div>
        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="manager">Manager Name<span class="req">*</span></label>
                <input type="text" value="{{$rest->manager}}" name="manager" required class="md-input" />
            </div>
        </div>

    </div>
  
    <div class="uk-grid" data-uk-grid-margin>
      
        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="details">Details<span class="req">*</span></label>
                <textarea class="md-input no_autosize" name="details" required>{{$rest->details}}</textarea>
            </div>
        </div>
    </div>

    <div class="uk-grid" data-uk-grid-margin>

        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="phone">phone<span class="req">*</span></label>
                <input type="text" value="{{$rest->phone}}" name="phone" required class="md-input" />
            </div>
        </div><br>
        <div class="uk-width-medium-1-2">
            <div class="parsley-row">
                <label for="commission">Commission<span class="req">*</span></label>
                <input type="text" value="{{$rest->commission}}" name="commission" required class="md-input" />
            </div>
        </div><br>


    </div><br>
    <legend>Places</legend>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1 uk-margin-large-bottom">
            <table>
                <thead>
                    <th>Postal</th>
                    <th>Min Order</th>
                    <th>Delivery Price</th>
                    <th>Delivery Time</th>
                    <th><a class="add-place" style="text-align: center" title="add new place"><i class="md-icon material-icons " style="color:green">&#xE146;</i></a>
                    </th>
                </thead>
                <tbody id="places-body">
                    @forelse ($rest->places as $place)
                    <tr id="place-{{$place->id}}">
                        <td>
                            <input type="text" value="{{$place->name}}" name="place[name][]" required class="md-input" />
                        </td>
                        <td>
                            <input type="number" step=".01" value="{{$place->minOrder}}" name="place[minOrder][]" required
                                class="md-input" />
                        </td>
                        <td>
                            <input type="number" step=".01" value="{{$place->deliveryPrice}}" name="place[deliveryPrice][]" required
                                class="md-input" />
                        </td>
                        <td>
                            <input type="number" step="1" value="{{$place->deliveryTime}}" name="place[deliveryTime][]" required
                                class="md-input" />
                        </td>
                        <td>
                            <a class="remove-place" style="text-align: center" title="remove place" data-id="{{$place->id}}"><i class="md-icon material-icons "
                                    style="color:red;">delete</i></a>
                        </td>
                    </tr>
                    @empty

                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
   

    <div class="uk-grid">
        <div class="uk-width-1-1">
            <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">Update</button>
        </div>
    </div>
    <hr>
    <div class="uk-grid">
        <div class="uk-width-1-1">
            <button id="workingCalendarbtn" type="button" class="md-btn md-btn-primary md-btn-block md-btn-wave-light" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" title="Working Calendar" data-uk-modal="{target:'#modal_working_calendar'}">Working Calendar
                </button>
        </div>
    </div>
    <hr>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
            <div class="parsley-row">
                <label for="users">Users<span class="req">*</span></label>
                <select id="users" name="users[]" class="uk-width-1-1" multiple data-md-select2>
                    <option value="">Select Users</option>
                    @forelse ($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @empty

                    @endforelse
                </select>
            </div>
        </div>

    </div>
    <div class="uk-grid">
        <div class="uk-width-1-1">
            <button id="editUsers" type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">Update
                Users</button>
        </div>
    </div>
    
</div>

</div>
</form>
<div class="md-card">
    <div class="md-card-toolbar">
        <h3 class="md-card-toolbar-heading-text">Restaurant Coupons</h3>
        <button class="md-btn md-bg-green-300 get-coupons md-btn-wave-light"
            style="float:right; margin:5px">Show</button>
    </div>
    <div class="md-card-content large-padding coupon-content">

    </div>
</div>
@if(count($rest->reports)>0)
<div class="md-card">
    <div class="md-card-toolbar">
        <h3 class="md-card-toolbar-heading-text">Restaurant Reports</h3>
    </div>
    <div class="md-card-content large-padding">
        <div class="md-list-outside-inner">
            <ul class="md-list md-list-outside invoices_list" id="invoices_list">

                @forelse ($rest->reports as $report)
                <li id="report-{{$report->id}}">
                    @role('admin')
                    <a href="{{url('report-delete/'.$report->id)}}" data-id="{{$report->id}}"
                        class="material-icons delete-report" style="float: left;
                padding: 33px;
                margin: -13px;
                font-size: x-large;
                color: red;
                background-color: white;">delete</a>
                    @endrole
                    <a href="{{url('report/'.$report->id)}}" target="_blank" class="md-list-content "
                        style="width:70%;    padding-left: 40px;">
                        <span class="md-list-heading uk-text-truncate">{{$report->name}} <span
                                class="uk-text-small uk-text-muted">({{$report->created_at->toFormattedDateString()}})</span></span>
                        <span class="uk-text-small uk-text-muted">{{$report->from}} - {{$report->to}}</span>
                    </a>
                </li>
                @empty

                @endforelse
            </ul>
        </div>
    </div>
</div>
@endif
<button class="md-btn" style="display:none" id="danger" data-message="Password Confirmation didn't match"
    data-status="danger" data-pos="bottom-center">Danger</button>
<button class="md-btn" style="display:none" id="success"
    data-message="<a href='#' class='notify-action'>Clear</a> Success Message" data-status="success"
    data-pos="bottom-center">Success</button>
</div>
@include('restaurants.foods')
</div>
</div>
@include('restaurants.workingCalendar')

<table style="display:none;">
        <tr id="place">
            <td>
                <input type="text" value="" name="place[name][]" required class="md-input" />
            </td>
            <td>
                <input type="number" step=".01" value="" name="place[minOrder][]" required
                    class="md-input" />
            </td>
            <td>
                <input type="number" step=".01" value="" name="place[deliveryPrice][]" required
                    class="md-input" />
            </td>
            <td>
                <input type="number" step="1" value="" name="place[deliveryTime][]" required
                    class="md-input" />
            </td>
            <td>
                <a class="remove-place" style="text-align: center" title="remove place" data-id=""><i class="md-icon material-icons "
                        style="color:red;">remove_circle</i></a>
            </td>
        </tr>
    </table>
@endsection

@section('pagejs2')


<script>
    $('.add-place').on('click',function(e){
        e.preventDefault();
        $('#places-body').append($('#place').clone(true));
    });
    $('.remove-place').on('click', function () {
            $(this).parent().parent().remove();
        });
    $('#logo-input').on('change',(e)=>{
        console.log(e);
        if(e.target.files.length > 0)
            $('.fileinput-upload').show();
        else
        $('.fileinput-upload').hide();

    });
    $('.fileinput-upload').on('click',()=>{
        var image = $('#logo-input')[0].files[0];
        console.log(image);
        var fdata= new FormData();
        fdata.append('logo',image);
        console.log(fdata);
        $(this).attr("disabled","disabled")
        $.ajax({
           url:"{{url('rest/'.$rest->id.'/logo')}}",
           type:"POST",
           data:fdata,
           contentType: false,
            processData: false,
           success:function(res){
             $(this).attr('disabled','false');

               var reader = new FileReader();
                    reader.onload = function (e) {
                    $('#logo').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(image);
               $('a.fileinput-exists').click();

            }
        })
    })
    
    $('.settings').on('change',(e)=>{
        console.log(e.target.checked);
        console.log(e.target.name);
        $name = e.target.name;
        val = (e.target.checked)?1:0;
        data={};
        data[$name]=val;
        $.ajax({
            url:"{{url('restaurant/'.$rest->id.'/changesettings')}}",
            type:'post',
            data:data,
            success:function(res){
                $('#success').data('message',"Settings have been updated").click();
            }
        })
        
    })
   
    /////////////////////////
    $(document).on('ready',function(){

tags =[];
@forelse ($rest->tags as $tag)
      tags.push("{{$tag->id}}");
@empty
@endforelse
$("#tags").select2()
$('#tags').val(tags).trigger('change');
restUSers = [];
@forelse ($rest->users as $user)
restUSers.push("{{$user->id}}");
@empty
@endforelse
    console.log('tags');
$("#users").select2()
$('#users').val(restUSers).trigger('change');

})
    $('.delete-report').on('click',function(e){
    e.preventDefault();
    c = confirm('Are You Sure You Want to delete this report?');
    url = $(this).attr('href');
    id = $(this).data('id');
    if(c){

        $.ajax({
           url:url,
           type:"get",
           success:function(res){
               $('#report-'+id).slideUp();
               $('#report-'+id).remove();
            }
        })
    }
})
$('#editUsers').on('click',function(e){
    console.log('edit user');
    
    e.preventDefault();
    values = $('#users').select2('val');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
        url = "{{url('restaurantusers/'.$rest->id)}}"
        type = "POST";
        $.ajax({
            url:url,
            type:type,
            data:{users:values},
            success:function(res){
                if(res){
                    $('#success').data('message',"@lang('messages.UsersUpdated')").click();

                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    });
    $(document).on('submit','.create-coupon-form',function(e){
    e.preventDefault();
    url = $(this).attr('action');
    data = $(this).serializeArray();
    type = $(this).attr('method');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }})
    $.ajax({
            url:url,
            type:type,
            data:data,
            success:function(res){
                altair_helpers.content_preloader_hide();

                if(res.html == null){
                    $('#success').data('message',"@lang('messages.NoCoupons')").click();
                   
                }else{

                    $('.coupon-content').html(res.html);
                    $('.get-coupons').hide();
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
        var modal = UIkit.modal("#modal_create");
                    modal.hide()
    
})
$(document).on('submit','#edit-coupon-form',function(e){
    e.preventDefault();
    data = $(this).serializeArray();
    url = $(this).attr('action');
    type = $(this).attr('method');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }})
    $.ajax({
            url:url,
            type:type,
            data:data,
            success:function(res){
                altair_helpers.content_preloader_hide();

                if(res.html == null){
                    $('#success').data('message',"@lang('messages.NoCoupons')").click();
                   
                }else{

                    $('.coupon-content').html(res.html);
                    // $('.get-coupons').hide();
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
        var modal = UIkit.modal("#modal_edit");
                    modal.hide()
})
    $(document).on('click','.edit-coupon',function(){
    data = $(this).data('info');
    $('#edit-coupon-form').attr('action',"{{url('coupon')}}"+'/'+data['id'])
    $('#edit-coupon-form input').each(function(){
   if(data[this.id])
    this.value=data[this.id]
});

    $('#edit-coupon-form select').each(function(){
   if(data[this.id])
    this.value=data[this.id]
});
    
})

    $(document).on('click','.delete-coupon',function(e){
    e.preventDefault()
    conf = confirm('Are Your Aure?');
    if(!conf)
        return
    url=$(this).attr('href');
    id=$(this).data('id');
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }})
    $.ajax({
        url:url,
        type:'POST',
        data: {_method: 'delete'},
        success:function(res){
            if(res.success){
                $('#coupon-'+id).hide(100);
            }
        },
        error:function(e){
            console.log(e);
        }
    })
});
    $('.get-coupons').on('click',function(){
        altair_helpers.content_preloader_show('md');
        url = "{{url('rest-coupons/'.$rest->id)}}"
        type = "GET";
        $.ajax({
            url:url,
            type:type,
            success:function(res){
                altair_helpers.content_preloader_hide();

                if(res.html == null){
                    $('#success').data('message',"@lang('messages.NoCoupons')").click();
                   
                }else{
                    $('.coupon-content').append(res.html);
                    $('.get-coupons').hide();
                }
            },
            error: function( error){
                console.log(error.responseJSON.errors)
            }
        });
    })

</script>
@endsection