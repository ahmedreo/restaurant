<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable=['name','enough','details','info','picture','discount','category_id','restaurant_id','available','order'];
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
    public function options()
    {
        return $this->hasMany(FoodOption::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function sizes()
    {
        return $this->hasMany(Size::class);
    }
    public function allergies()
    {
        return $this->belongsToMany(Allergy::class);
    }
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
