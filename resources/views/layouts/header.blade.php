<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html  lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <link rel="icon" type="image/png" href="{{asset('admin/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{asset("admin/assets/img/favicon-32x32.png")}}" sizes="32x32">



    <!-- uikit -->
    <link rel="stylesheet" href="{{asset("admin/bower_components/uikit/css/uikit.almost-flat.min.css")}}" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{asset("admin/assets/icons/flags/flags.min.css")}}" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="{{asset("admin/assets/css/style_switcher.min.css")}}" media="all">
    
    <!-- altair admin -->
    <link rel="stylesheet" href="{{asset("admin/assets/css/main.min.css")}}" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="{{asset("admin/assets/css/themes/themes_combined.min.css")}}" media="all">
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
        <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
       
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->
<script>

</script>

</head>