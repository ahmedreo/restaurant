<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->boolean('new');
            $table->boolean('suggested');
            $table->boolean('preorder');
            $table->boolean('active');
            $table->string('manager');
            $table->string('cover');
            $table->double('commission',8,2);
            $table->integer('order');
            $table->text('details')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurants', function (Blueprint $table) {
            $table->dropColumn('new');
            $table->dropColumn('suggested');
            $table->dropColumn('preorder')->default(1);
            $table->dropColumn('active')->default(1);
            $table->string('manager')->nullable();
            $table->string('cover')->nullable();
            $table->double('commission',8,2)->nullable();
            $table->integer('order')->nullable();
            $table->text('details')->nullable();

        });
    }
}
