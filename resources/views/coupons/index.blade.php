<div class="uk-width-medium-1-6" style="padding: 5px;">
    {{-- <a class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light create-coupon" href="{{url('category/create')}}">Create
    Coupon</a> --}}
    <button class="md-btn" data-uk-modal="{target:'#modal_create'}">Create Coupon</button>

</div>
<div class="md-card uk-margin-medium-bottom">
    <div class="md-card-content">
        <div class="uk-overflow-container">
            <table class="uk-table uk-table-nowrap table_check">
                <thead>
                    <tr>
                        <th class="uk-width-2-10 uk-text-center">Code</th>
                        <th class="uk-width-2-10 uk-text-center">Count</th>
                        <th class="uk-width-1-10 uk-text-center">Used</th>
                        <th class="uk-width-2-10 uk-text-center">Descount</th>
                        <th class="uk-width-2-10 uk-text-center">Start</th>
                        <th class="uk-width-2-10 uk-text-center">End</th>
                        <th class="uk-width-2-10 uk-text-center">Action</th>
                    </tr>
                </thead>
                <tbody id="table-body">
                    @forelse ($coupons as $coupon)
                    <tr id="coupon-{{$coupon->id}}">
                        <td class="uk-text-center">{{$coupon->code}}</td>
                        <td class="uk-text-center">{{$coupon->count}}</td>
                        <td class="uk-text-center">@if(count($coupon->orders)>0){{count($coupon->orders)}} @else 0
                            @endif</td>
                        <td class="uk-text-center">{{$coupon->descount }}{{($coupon->descount_type==0)?' e':' %'}}</td>
                        <td class="uk-text-center">{{$coupon->start->toFormattedDateString()}}</td>
                        <td class="uk-text-center">{{$coupon->end->toFormattedDateString()}}</td>

                        <td class="uk-text-center">
                            <a href="{{url('coupon/'.$coupon->id.'/edit')}}" class="edit-coupon" data-uk-modal="{target:'#modal_edit'}" data-info="{{$coupon->toJson()}}"><i
                                    class="md-icon material-icons">&#xE254;</i></a>
                                    

                            <a href="{{url('coupon/'.$coupon->id)}}" data-id="{{$coupon->id}}" class="delete-coupon"><i
                                    class="md-icon material-icons">delete</i></a>

                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6" style="text-align: center">@lang('messages.NoCoupons')</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

    </div>

</div>
<div class="uk-modal" id="modal_create">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Create Coupon</h3>
        </div>
        <form action="{{url('coupon')}}" method="post" class="create-coupon-form">
            <input type="hidden" name="restaurant_id" value="{{$rest->id}}">
            @include('coupons.form')
            <div class="uk-modal-footer uk-text-right">
                <button type="submit" class="md-btn md-btn-flat md-btn-primary create-coupon">Create</button>
                <button class="md-btn md-btn-flat uk-modal-close">Close</button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="modal_edit">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Coupon</h3>
        </div>
        <form action="{{url('coupon/')}}" method="post" id="edit-coupon-form">
            @include('coupons.form')
            <input type="hidden" name="restaurant_id" value="{{$rest->id}}">
            {{ method_field('PUT') }}   
            <input type="hidden" name="id" id="id">
            <div class="uk-modal-footer uk-text-right">
                <button  type="submit" class="md-btn md-btn-flat md-btn-primary ">Edit</button>
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            </div>
        </form>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded",function(){
           $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    



  })
</script>