<div id="option-card-{{$option->id}}">

    <div class="md-card">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
                <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
                <i class="md-icon material-icons edit-option" data-uk-modal="{target:'#edit_option'}" data-option="{{json_encode($option)}}">edit</i>
                <i class="md-icon material-icons uk-text-danger delete-option" id="{{$option->id}}" title="Delete">&#xE14C;</i>
            </div>
            <h3 class="md-card-toolbar-heading-text">
                {{$option->name}}
            </h3>
        </div>
        <div class="md-card-content">
            MIN:{{$option->min}}<br> MAX:{{$option->max}}
            <br> Required:{{$option->req?'YES':'NO'}}
            <br>
            <hr>
            <legend style="text-align: center;font-weight: bold">Items</legend>
            <form action="{{url('addoptionitem/'.$option->id)}}" class="add-item" data-id="{{$option->id}}" method="get">
                <div class="uk-grid" data-uk-grid-margin>
                    <table style="width:85%">
                        <td>
                            <label for="name">name<span class="req">*</span></label>
                            <input type="text" name="name" data-parsley-trigger="change" required class="md-input">
                        </td><td>
                            <label for="price">price<span class="req">*</span></label>
                            <input type="number" step=".01" name="price" required class="md-input" />
                        </td><td>
                            <button type="submit" class=" " style="background:none; border:none; font:500 14px/25px Roboto,sans-serif;color: #212121;
                           ;cursor:pointer"><i class="material-icons uk-text-primary">&#xE145;</i></button>
                    </td> 
                </table>
                </div>
            </form>
            <hr>
            <table id="list-{{$option->id}}" style="width:100%">

                @forelse ($option->items as $item)
                <tr id="item-{{$item->id}}">
                    <td>{{$item->name}}</td>
                    <td>{{$item->price==0?'free':$item->price}}</td>
                    <td style="cursor:pointer; text-align: center" id="{{$item->id}}"><i class="material-icons delete-item uk-text-danger" id="{{$item->id}}">&#xE872;</i></td>
                </tr>
                @empty @endforelse
            </table>
    </div>
</div>
</div>