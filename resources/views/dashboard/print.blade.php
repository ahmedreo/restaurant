<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Invoice {{$order->code}}</title>

    <style>
        table {
            width: 100%;
            border-spacing: 0;

        }

        tr {
            width: 100%;
        }

        .borderd-td-left {
            border-left: 1px solid black;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: left;
            padding: 10px;

        }

        .borderd-td-right {
            border-right: 1px solid black;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: right;
            padding: 10px;

        }

        .borderd-td-middle {
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: center;
        }
        .remove-top-border > *{
            border-top:none;
        }
        .items > *{
            border-bottom: 1px solid black;
        }
        .page-break {
    page-break-after: always;
}
        @page {
	header: page-header;
	footer: page-footer;
}
    </style>

</head>

<body>
        <htmlpageheader name="page-header">
                Your Header Content
            </htmlpageheader>
            <htmlpagefooter name="page-footer">
                    Your Footer Content
                </htmlpagefooter>
                <div style="margin:10px">
                    <table>
                        <tr>
                            <td width='60%' style="text-align: center">
                                {{-- <img src="{{asset('images/alertLogo.png')}}" width="70px"> --}}
                                <p>Delivery Hero Germany GmbH - Oranienburger Straße 70 - 10117 Berlin
                                </p>
                            </td>
                            <td  style="border-right: 2px solid black; "></td>
                            <td  width='35%' style="vertical-align: baseline;padding-left:10px">
                                <div style="line-height: 1">
                                    <div style="">
                                    <p><strong>{{$order->restaurant->name}}</strong></p>
                                    <p style="">{{$order->restaurant->addressName}}</p>
                                </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <td width="30%">
                                <p>{{$order->customer->address}}</p>
                            </td>
                            <td style="text-align: center">
                                {{-- <img width="70px" src="{{asset($restaurant->logo)}}" alt="{{$restaurant->name}}" /> --}}
            
                            </td>
            
                        </tr>
                    </table>
                    <br>
                    <table>
                            <tr>
                                <td class="borderd-td-middle" width="33%">
                                  <div style="padding-right: 10%;
                                  padding-top: 2px;">
                                   <span> Client Name.:</span>  <span style="float:right">  {{$order->customer->name}}</span>
                                </div>
                                <div style="padding-right: 10%;
                                padding-top: 2px;">
                                   <span style="padding-right:90px"> Order Code.:</span> <span  style="float:right">{{$order->code}}</span>
                                </div>
                                </td>
                                <td class="borderd-td-middle" width="33%">
                                        (Bitte bei Zahlung unbedingt angeben!)
                                </td>
                                <td class="borderd-td-middle" width="33%">
                                        <div style="padding-right: 10%;
                                        padding-top: 2px;">
                                          Dispatched at:  <span style="float:right">  {{$order->created_at->toFormattedDateString()}}</span>
                                      </div>
                                </td>
                            </tr>
                        </table>
                <div class="uk-grid">
                    <div class="uk-width-large-2-3">
                        <div class="uk-grid">
                            <p><strong>ORDER ITEMS</strong></p>
                        </div>
                        <hr>
                        <table id="invoice-{{$order->id}}" style="width:100%" class="uk-table uk-table-nowrap uk-table-hover">
                            <thead>
                                <th style="width:5%">Count</th>
                                <th style="width:80%">Item</th>
                                <th style="text-align: left;width:15% ">Price</th>
                            </thead>
                            <tbody>
                
                                @forelse ($order->foods as $food)
                                <tr class="table-child-row">
                                    <td style="width:5%">{{$food->pivot->count}}x</td>
                                    <td style="width:80%"> {{$food['name']}}-{{$food->pivot->size}}
                                        @if($food->pivot->comment)<br><span style="color:red;">{{$food->pivot->comment}}</span>@endif
                                    </td>
                                    <td style="width:15%;text-align: right">
                                            @if($food->pivot->discount >0)
                                        <span style="color:red;  text-decoration: line-through;">{{$food->pivot->price}}-</span>{{$food->pivot->discount}}
                                        @else
                                        {{$food->pivot->price}} @endif
                                    </td >
                                </tr>
                                @forelse ($food->pivot->selectedOptions as $option)
                                <tr>
                                    <td style="width:5%"></td>
                                    <td style="padding-left:12%">{{$option['option']}}</td>
                                    <td style="text-align: right">{{$option['price']}}</td>
                                </tr>
                                @empty @endforelse
                                @empty @endforelse              
                              @if($order->restaurant->deliveryPrice >0)
                                <tr >
                                    <td colspan="2">Delivery</td>
                                    <td style="text-align: right">{{$order->restaurant->deliveryPrice}}</td>
                                </tr>
                                @endif
                                <tr style="background-color: burlywood">
                                    <td colspan="2">Total</td>
                                    <td style="text-align: right">{{$order->total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="uk-width-large-1-3">
                        <p><strong>STATUS TIMELINE</strong></p>
                        <hr>
                        <ul class="md-list" style="text-align: left">
                            @forelse ($order->statuses as $status)
                
                            <li>
                                <div class="md-list-content">
                                    <span class="md-list-heading">{{$status->status}}</span>
                                    <span class="uk-text-small uk-text-muted"> {{$status->created_at->toTimeString()}}</span>
                                </div>
                            </li>
                            @empty @endforelse
                        </ul>
                    </div> --}}
                
                </div>
                </div>
    <script>
        win = window;
    	win.setTimeout( function () {
			
				win.print(); // blocking - so close will not
				// win.close(); // execute until this is done
		}, 1000 );
    </script>
</body>

</html>