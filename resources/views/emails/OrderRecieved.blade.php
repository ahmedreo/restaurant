@component('mail::message') 
# Your Order has been placed successfully and sent to <strong>{{$order->restaurant->name}}</strong>
Your Order Number is : {{$order->code}}
@component('mail::table')

<table id="invoice-{{$order->id}}" style="width:100%" class="uk-table uk-table-nowrap uk-table-hover">
    <thead>
        <th style="text-align: center;width:10%">Count</th>
        <th style="width:75%">Item</th>
        <th style="text-align: center;width:15% ">Price</th>
    </thead>
    <tbody>
            @forelse ($order->foods as $food)
            <tr class="table-child-row">
                <td style="width:10%">{{$food->pivot->count}}x</td>
                <td style="width:75%"> {{$food['name']}}-{{$food->pivot->size}}</td>
                <td style="width:15%"><span @if($food->pivot->discount >0)style="color:red" @endif>{{$food->pivot->price}}</span>
                    @if($food->pivot->discount >0)-{{$food->pivot->discount}} @endif
                </td>
            </tr>
            @forelse ($food->pivot->selectedOptions as $option)
            <tr>
                <td style="width:5%"></td>
                <td style="padding-left:12%">{{$option['option']}}</td>
                <td>{{$option['price']}}</td>
            </tr>
            @empty @endforelse
            @empty @endforelse
           
          

          @if($order->restaurant->deliveryPrice >0)
            <tr >
                <td colspan="2">Delivery</td>
                <td>{{$order->restaurant->deliveryPrice}}</td>
            </tr>
            @endif
            <tr style="background-color: burlywood">
                <td colspan="2">Total</td>
                <td>{{$order->total}}</td>
            </tr>
    </tbody>
</table>
@endcomponent 


 Thanks,

<br> {{ config('app.name') }} @endcomponent