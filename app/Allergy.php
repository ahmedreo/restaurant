<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allergy extends Model
{
    protected $fillable=['code','description'];

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }
}
