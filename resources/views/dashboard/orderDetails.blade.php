<small>Order was dispatched at</small><br>
<i class="material-icons">access_time</i>&nbsp;<strong>{{$order->created_at->toDayDateTimeString()}}</strong>
@if($order->request_origin)<i style="float: right;
background-color: wheat;
padding: 3px;
border-radius: 5px;
border: 1px solid gray;box-shadow: 2px 3px 4px -1px black;">{{$order->request_origin}}</i>@endif
<hr>
<div class="uk-grid">
    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1" id="customer-{{$order->id}}">
        <h4 class="heading_c uk-margin-small-bottom">Customer Info</h4>
        <ul class="md-list md-list-addon">
            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon uk-icon-user"></i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->name}}</span>
                    <span class="uk-text-small uk-text-muted">Name</span>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->company}}</span>
                    <span class="uk-text-small uk-text-muted">Company</span>
                </div>
            </li>
            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon material-icons">&#xE158;</i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->email}}</span>
                    <span class="uk-text-small uk-text-muted">Email</span>
                </div>
            </li>
            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->phone}}</span>
                    <span class="uk-text-small uk-text-muted">Phone</span>
                </div>
            </li>

            <li>
                <div class="md-list-addon-element">
                    <i class="md-list-addon-icon uk-icon-adress-card"></i>
                </div>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$order->customer->address}}</span>
                    <span class="uk-text-small uk-text-muted">Address</span>
                </div>
            </li>
        </ul>
    </div>
    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
        @if($controls) {{-- just created --}} @if ($order->status == 0)
        <a class="md-btn md-btn-danger md-btn-block md-btn-wave-light change-status" data-id="{{$order->id}}" href="{{url('changestatus/'.$order->id.'/6')}}"><i class="material-icons">cancel</i>&nbsp;Reject</a>
        <div style="background-color: springgreen;margin-top: 20px;padding-bottom: 10px;
        padding: 10px;">
            <p style="text-align: center"><strong>Accept</strong></p>
            <a class="md-btn md-btn-block md-btn-wave-ligh md-btn-block change-status" data-id="{{$order->id}}" href="{{url('changestatus/'.$order->id.'/1')}}"><i class="material-icons">timer</i>&nbsp;20 M</a>
            <a class="md-btn md-btn-block md-btn-wave-ligh md-btn-block change-status" data-id="{{$order->id}}" href="{{url('changestatus/'.$order->id.'/2')}}"><i class="material-icons">timer</i>&nbsp;40 M</a>
            <a class="md-btn md-btn-block md-btn-wave-ligh md-btn-block change-status" data-id="{{$order->id}}" href="{{url('changestatus/'.$order->id.'/3')}}"><i class="material-icons">timer</i>&nbsp;60 M</a>
        </div>
        {{-- // accepted --}} @elseif($order->status == 1)
        <a class="md-btn md-btn-primary md-btn-block md-btn-wave-light change-status" data-id="{{$order->id}}" href="{{url('changestatus/'.$order->id.'/4')}}"><i class="material-icons">directions_run</i>&nbsp; On The Way</a>        {{-- // with the delivery --}} @elseif($order->status == 2)
        <a class="md-btn md-btn-success md-btn-block md-btn-wave-light change-status" data-id="{{$order->id}}" href="{{url('changestatus/'.$order->id.'/5')}}"><i class="material-icons">check_circle</i>&nbsp; Delivered</a>        {{-- // rejected --}} @elseif($order->status == 4) This Order Has Been Rejected @endif @endif
    </div>

</div>
<hr>
<div class="uk-grid">
        <div class="uk-width-large-2-3">
                <p><strong>ORDER DETAILS</strong></p>
            <p style="background-color: #FCB813;margin: 1%;padding: 3%;">
        {{$order->details}}</p>
        <p style="color:red">{{$order->pre_order}}</p>
        </div>
</div>
<div class="uk-grid">
    <div class="uk-width-large-2-3">
        <div class="uk-grid">
            <p><strong>ORDER ITEMS</strong></p>
        <div style="padding-left:46%; "> <a  href="{{url('print/'.$order->id)}}" target="_blank" style="background-color:gray" class=" md-btn  md-btn-mini "><i class="material-icons">print</i>print</a></div>
        </div>
        <hr>
        <table id="invoice-{{$order->id}}" style="width:100%" class="uk-table uk-table-nowrap uk-table-hover">
            <thead>
                <th style="width:5%">Count</th>
                <th style="width:80%">Item</th>
                <th style="text-align: center;width:15% ">Price</th>
            </thead>
            <tbody>

                @forelse ($order->foods as $food)
                <tr class="table-child-row">
                    <td style="width:5%">{{$food->pivot->count}}x</td>
                    <td style="width:80%"> {{$food['name']}}-{{$food->pivot->size}}
                        @if($food->pivot->comment)<br><span style="color:red;">{{$food->pivot->comment}}</span>@endif
                    </td>
                    <td style="width:15%"><span @if($food->pivot->discount >0)style="color:red" @endif>{{$food->pivot->price*$food->pivot->count}}</span>
                        @if($food->pivot->discount >0)-{{$food->pivot->discount*$food->pivot->count}} @endif
                    </td>
                </tr>
                @forelse ($food->pivot->selectedOptions as $option)
                <tr>
                    <td style="width:5%"></td>
                    <td style="padding-left:12%">{{$option['option']}}</td>
                    <td>{{$option['price']*$food->pivot->count}}</td>
                </tr>
                @empty @endforelse
                @empty @endforelse   
              @if($order->coupon)
                <tr>
                    <td colspan="2">Coupon</td>
                <td>{{$order->coupon->descount}}{{($order->coupon->descount_type == 1)?'%':'e'}}</td>
                </tr>
                @endif
              @if($order->restaurant->deliveryPrice >0)
                <tr >
                    <td colspan="2">Delivery</td>
                    <td>{{$order->restaurant->deliveryPrice}}</td>
                </tr>
                @endif
                <tr style="background-color: burlywood">
                    <td colspan="2">Total</td>
                    <td>{{$order->total}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="uk-width-large-1-3">
        <p><strong>STATUS TIMELINE</strong></p>
        <hr>
        <ul class="md-list" style="text-align: left">
            @forelse ($order->statuses as $status)

            <li>
                <div class="md-list-content">
                    <span class="md-list-heading">{{$status->status}}</span>
                    <span class="uk-text-small uk-text-muted"> {{$status->created_at->toTimeString()}}</span>
                </div>
            </li>
            @empty @endforelse
        </ul>
    </div>

</div>
<div id="printable">
    <div class="uk-grid">

    </div>

</div>
