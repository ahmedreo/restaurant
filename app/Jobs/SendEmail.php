<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\OrderAccepted;
use App\Order;
use App\Mail\WithDelivery;
use App\Mail\OrderRecieved;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        info('dispatched');
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Order $order)
    {
        
        info('dispat');
        info($this->order);
        if ($this->order->status == 0) {
            info('status0');
            \Mail::to($this->order->customer->email)->send(new OrderRecieved($this->order));
        } elseif ($this->order->status == 1) {
            info('status1');
            \Mail::to($this->order->customer->email)->send(new OrderAccepted($this->order));
        } elseif ($this->order->status == 2) {
            info('status2');
            \Mail::to($this->order->customer->email)->send(new WithDelivery($this->order));
        }
        info($this->order);
    }
}
