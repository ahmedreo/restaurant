<table>
        <tr class="items">
            <td>Pos</td>
            <td>Bestellnr</td>
            <td>Bestellwert</td>
            <td>Datum</td>
            <td>Online Geb.satz</td>
            <td>Online Gebühr</td>
        </tr>
        @forelse ($orders as $i=>$order)
            <tr>
            <td>{{$i+1}}</td>
            <td>{{$order->code}}</td>
            <td>{{$order->total}}</td>
            <td>{{$order->created_at}}</td>
            <td>2.50%</td>
            <td>{{ round($order->total*2.5/100,2)}}</td>
            </tr>
        @empty
            No Orders
        @endforelse
</table>