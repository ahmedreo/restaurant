<?php

namespace App\Http\Controllers;

use App\Allergy;
use Illuminate\Http\Request;
use App\Restaurant;
use App\Http\Resources\RestaurantCollection;
use App\Http\Requests\CreateRestaurant;
use App\User;
use App\Category;
use App\Food;
use App\Place;
use App\Tag;
use App\WeeklyCalendar;
use Illuminate\Support\Facades\Input;
use Image;

class RestaurantController extends Controller
{
    public function __construct()
    {
        info(request()->getHttpHost());

        $this->middleware('auth')->except(['findOneRestaurant', 'findRestaurant', 'findByPostal', 'getRestauantFoods', 'findByPostalForMap']);
        // $this->middleware(['role:admin|owner'])->only('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rests = Restaurant::orderBy('order', 'asc')->paginate(10);
        return view('restaurants.index')->with('restaurants', $rests);
    }
    public function findRestaurant($key)
    {
        $rests = Restaurant::whereHas('places', function ($q) use ($key) {
            $q->where('name', 'like', '%' . $key . '%');
        })->get();
        if (!$rests)
            response('Not Found', 404);
        $rests->load('foods.options.items', 'foods.category');
        foreach ($rests as $rest) {
            $rest['offDays'] = $this->offDaysCast(unserialize($rest->offDays));
            $cat = Category::select('id', 'name', 'picture', 'details')->whereHas('foods', function ($q) use ($rest) {
                $q->where('restaurant_id', $rest->id);
            })->get();
            $rest['cats'] = $cat;
        }

        return response()->json($rests);
    }
    public function findOneRestaurant($key)
    {
        info(request()->getHttpHost());
        $rest = Restaurant::whereHas('places', function ($q) use ($key) {
            $q->where('name', 'like', '%' . $key . '%');
        })->first();
        // info($rest);
        if (!$rest)
            response('Not Found', 404);
        $rest['offDays'] = $this->offDaysCast(unserialize($rest->offDays));
        info('de');
        $rest->load('foods.options.items', 'foods.category');
        $cat = Category::select('id', 'name', 'picture', 'details')->whereHas('foods', function ($q) use ($rest) {
            $q->where('restaurant_id', $rest->id);
        })->get();
        $rest['cats'] = $cat;
        return response()->json($rest);
    }

    public function offDaysCast($values)
    {
        info('cast');
        $result = "";
        $l = count($values);
        // info($l);
        $l--;
        foreach ($values as $key => $value) {
            $result .= $value;
            if ($key < $l)
                $result .= ",";
        }
        // $result .= '"';
        info($result);
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restaurants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRestaurant $request)
    {
        info($request->all());
        if ($request->hasFile('logo')) {
            $path = public_path('/images');
            $time = time();
            $file_name = 'images/' . $time . "_" . Input::file('logo')->getClientOriginalName();
            Input::file('logo')->move($path, $file_name);
            $thumb_name = 'thumb-images\\' . $time . "_" . Input::file('logo')->getClientOriginalName();
            $img = Image::make(public_path('/' . $file_name))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path($thumb_name));
        } else {
            $file_name = null;
        }

        $restaurant = Restaurant::create([
            'name' => $request['name'],
            'logo' => $file_name,
            'addressName' => $request['addressName'],
            'addressCord' => $request['addressCord'],
            'phone' => $request['phone'],
            'restaurant_id' => $request['restaurant_id'],
        ]);
        info($restaurant);
       
        return redirect()->route('restaurant.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (auth()->user()->hasRole('owner'))
            $rest = auth()->user()->restaurant;
        elseif (auth()->user()->hasRole('admin'))
            $rest = Restaurant::find($id);
        $rest->load('foods');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasRole('owner') && auth()->user()->restaurant->id != $id)
            return view('errors.403');
        $rest = Restaurant::findOrfail($id);
        $rest->load('users', 'tags', 'calendar');
        $users = User::all();
        $tags = Tag::all();
        // dd($rest);
        $cat = Food::orderBy('order', 'ASC')->where('restaurant_id', $rest->id)->with('category')->get();
        $cat = $cat->groupBy('category.name');
        return view('restaurants.edit')->with(['rest' => $rest, 'users' => $users, 'categories' => $cat, 'tags' => $tags]);
    }

    public function updateUsers(Request $request, Restaurant $rest)
    {
        if (auth()->user()->hasRole('owner') && auth()->user()->restaurant->id != $rest->id)
            return view('errors.403');
        info($request->all());
        info($rest);
        $users = $rest->users;
        foreach ($users as $user) {
            $user->restaurant_id = null;
            $user->save();
        }
        if ($request['users']) {
            foreach ($request['users'] as $user_id) {
                $user = User::find($user_id);
                $user->update(['restaurant_id' => $rest->id]);
            }
        }
        return response()->json(['success' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->user()->hasRole('owner') && auth()->user()->restaurant->id != $id)
            return view('errors.403');
        $restaurant = Restaurant::find($id);

        // dd($request->all());
       
        info($request->all());
        $path = null;
        $file_name = null;
        if ($request->hasFile('logo')) {
            $path = public_path('/images');
            $time = time();
            $file_name = 'images/' . $time . "_" . Input::file('logo')->getClientOriginalName();
            Input::file('logo')->move($path, $file_name);
            $thumb_name = 'thumb-images\\' . $time . "_" . Input::file('picture')->getClientOriginalName();
            $img = Image::make(public_path('/' . $file_name))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path($thumb_name));
        }
        $request['logo'] = $path;
        $restaurant->tags()->sync($request['tags']);
        unset($request['tags']);
        $restaurant->update([
            'name' => $request['name'],
            'logo' => $file_name ? $file_name : $restaurant->logo,
            'addressCord' => $request['addressCord'],
            'addressName' => $request['addressName'],
            'phone' => $request['phone'],
            'restaurant_id' => $request['restaurant_id'],
            'details' => $request['details'],
            'manager' => $request['manager'],
            'commission' => $request['commission']

        ]);
        // dd($restaurant);

        $restaurant->places()->delete();
        foreach ($request['place']['name'] as $key => $value) {
            Place::updateOrCreate(
                ['restaurant_id' => $restaurant->id, 'name' => $value],
                [
                    'minOrder' => $request['place']['minOrder'][$key],
                    'deliveryTime' => $request['place']['deliveryTime'][$key],
                    'deliveryPrice' => $request['place']['deliveryPrice'][$key],
                ]
            );
        }
        return redirect()->back();
    }
    public function updateLogo(Request $request, Restaurant $rest)
    {
        info($request);
        if ($request->hasFile('logo')) {
            $path = public_path('/images');
            $time = time();
            $file_name = 'images/' . $time . "_" . Input::file('logo')->getClientOriginalName();
            Input::file('logo')->move($path, $file_name);
            $thumb_name = 'thumb-images\\' . $time . "_" . Input::file('logo')->getClientOriginalName();
            $img = Image::make(public_path('/' . $file_name))->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path($thumb_name));
        }
        $request['logo'] = $path;
        $res = $rest->update([
            'logo' => $file_name ? $file_name : $rest->logo,
        ]);
        if ($res) {
            return response()->json(['success', true], 200);
        }
        return response()->json(['success', false]);
    }

    public function changeOrder(Request $request)
    {

        foreach ($request['ids'] as $key => $id) {
            info($id);
            Restaurant::find($id)->update(['order' => $key + 1]);
        }
        info($request->all());
    }

    public function changeSettings(Request $request, Restaurant $rest)
    {
        $rest->update($request->all());
        info($request->all());
    }

    public function setCalendar(Request $request, Restaurant $rest)
    {
        info($request->all());

        WeeklyCalendar::updateOrCreate(
            ['restaurant_id' => $rest->id],
            $request->all()
        );
        $rest->load('calendar');
        return response()->json(['res' => $rest]);
    }
    public function getCalendar(Restaurant $rest)
    {
        $rest->calendar->working();
    }

    public function findByPostal($postal)
    {
        $restaurants = Restaurant::with('foods.category', 'tags', 'places')->whereHas('places', function ($q) use ($postal) {
            $q->where('name', $postal);
        })->orderBy('order', 'asc')->get();
        foreach ($restaurants as $restaurant) {
            $restaurant['working'] = ($restaurant->calendar) ? $restaurant->calendar->working() : null;
        }
        return $restaurants;
    }
    public function findByPostalForMap($postal)
    {
        $restaurants = Restaurant::whereHas('places', function ($q) use ($postal) {
            $q->where('name', $postal);
        })->get();
        foreach ($restaurants as $restaurant) {
            $restaurant['working'] = ($restaurant->calendar) ? $restaurant->calendar->working() : null;
        }
        return (count($restaurants) > 0) ? $restaurants : response('', 404);
    }
    public function getRestauantFoods(Restaurant $rest)
    {
        info($rest);
        $rest->load('foods.sizes.options.items', 'foods.category', 'foods.allergies', 'foods.tags', 'places');
        $rest['working'] = ($rest->calendar) ? $rest->calendar->working() : null;

        return response()->json($rest);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Restaurant::find($id)->delete();
        return redirect()->route('restaurant.index');
    }
}
